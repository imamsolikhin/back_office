"use strict";
window.onload = function() {
  $(document).ready(function() {
    $("#datatable_wrapper").removeClass("dataTables_wrapper form-inline dt-bootstrap no-footer");
    $("#datatable_wrapper").addClass("dataTables_wrapper dt-bootstrap4 no-footer");

    $("#datatable-minor_wrapper").removeClass("dataTables_wrapper form-inline dt-bootstrap no-footer");
    $("#datatable-minor_wrapper").addClass("dataTables_wrapper dt-bootstrap4 no-footer");
  });
};

function bg_readURL(input, id_field) {
  var file = input.files[0];
  var reader = new FileReader();
  reader.onloadend = function() {
    $('#' + id_field).css('background-image', 'url("' + reader.result + '")');
  }
  if (file) {
    reader.readAsDataURL(file);
  }
}

function img_readURL(input, id_field) {
  if (input.files && input.files[0]) {
    var reader = new FileReader();

    reader.onload = function(e) {
      $('#' + id_field).attr('src', e.target.result);
    }

    reader.readAsDataURL(input.files[0]); // convert to base64 string
  }
}

function formatdate(input, to = false) {
  if(!input){
    var today = new Date();
    var dd = today.getDate();

    var mm = today.getMonth()+1;
    var yyyy = today.getFullYear();
    if(dd<10)
    {
        dd='0'+dd;
    }

    if(mm<10)
    {
        mm='0'+mm;
    }
    today = yyyy+'-'+mm+'-'+dd;

    if (to){
      return today+" 23:59"
    }else{
      return today+" 00:00"
    }
  }
  var date_arr = input.split(" ");
  var time = date_arr[1];
  var date_id_arr = date_arr[0].split("-");
  var date_fix = date_id_arr[2] + "-" + date_id_arr[1] + "-" + date_id_arr[0];
  var date_transaction = date_fix;
  if (time) {
    if(date_arr[1].split(":")[0] != "00"){
      date_transaction = date_fix + " " + date_arr[1].split(":")[0]+":"+date_arr[1].split(":")[1];
    }
  }
  return date_transaction;
}

function addCommas(nStr,sprtor = null) {
    nStr += '';
    if(!sprtor){
      sprtor = ',';
    }
    var x = nStr.split('.');
    var x1 = x[0];
    var x2 = x.length > 1 ? '.' + x[1] : '';
    var rgx = /(\d+)(\d{3})/;
    while (rgx.test(x1)) {
            x1 = x1.replace(rgx, '$1' + sprtor + '$2');
    }
    return x1 + x2;
}


var showDialog = showDialog || (function($) {

  'use strict';
  var $dialog = $(
    '<div id="show-modal" class="modal fade" data-backdrop="static" data-keyboard="false" tabindex="-1" role="dialog" aria-hidden="true" style="padding-top:15%; overflow-y:visible;">' +
    '<div class="modal-dialog modal-md">' +
    '<div class="modal-content">' +
    '<div class="modal-header bg-danger pt-3 pb-3">' +
    '<h5 class="modal-title text-white bold" id="modal-title">Show Dialog</h5>' +
    '<button type="button" class="close" data-dismiss="modal" aria-label="Close">' +
    '<i aria-hidden="true" class="text-white bold ki ki-close"></i>' +
    '</button>' +
    '</div>' +
    '<div class="modal-body">' +
    '<div class="container row">' +
    '<div id="content-header" class="col-lg-4 mr-2 ml-2 float-left">' +
    '<div class="col-12 symbol symbol-150 mr-5 align-self-start align-self-xxl-center">' +
    '<div id="content-dialog-img" class="symbol-label" style="background-image:url()"></div>' +
    '</div>' +
    '</div>' +
    '<div id="content-dialog" style="overflow-wrap: anywhere;" class="col-lg-12 col-lg-7 mr-2 ml-5 float-right">' +
    '<h1>Loading Content....</h1>' +
    '</div>' +
    '</div>' +
    '</div>' +
    '</div>' +
    '</div>' +
    '</div>');

  return {

    show: function(message, options) {
      // Assigning defaults
      if (typeof options === 'undefined') {
        options = {};
      }
      if (typeof message === 'undefined') {
        message = 'Loading';
      }

      var settings = $.extend({
        title: null,
        form_url: null,
        form_csrf: null,
        form_method: 'POST',
        imgUrl: null,
        dialogSize: 'm',
        progressType: '',
        onHide: null,
        btnCopy: false,
      }, options);

      // Configuring dialog
      $dialog.find('#show-modal>.modal-dialog').attr('class', 'modal-dialog').addClass('modal-' + settings.dialogSize);
      $dialog.find('show-modal>.progress-bar').attr('class', 'progress-bar');
      if (settings.progressType) {
        $dialog.find('#show-modal>.progress-bar').addClass('progress-bar-' + settings.progressType);
      }

      if (typeof settings.onHide === 'function') {
        $dialog.off('hidden.bs.modal').on('hidden.bs.modal', function(e) {
          settings.onHide.call($dialog);
        });
      }

      if (settings.title != null) {
        $dialog.find('#modal-title').html('<h2 style="font-weight:800;">' + settings.title + '</h2>');
      }

      if (settings.imgUrl == null) {
        $dialog.find('#content-header').css("display", "none");
      } else {
        $dialog.find('#content-dialog-img').css("background-image", "url('" + settings.imgUrl + "')");
      }

      if (settings.btnCopy) {
        var textarea = "'" + message + "'";
        var button = '<br><button type="button" class="btn btn-sm btn-info" onclick="copyText(' + textarea + ')" >Copy Laporan</button>';
        $dialog.find('#content-dialog').html(message + button);
      } else {
        var button = "";
        if (settings.form_url != null) {
          var button = '<hr><form class="form" id="form-input" action="'+settings.form_url+'" method="POST"><input type="hidden" class="form-control" id="method" name="_method" placeholder="Enter method" value="'+settings.form_method+'"/>'+settings.form_csrf+'<button type="submit" class="btn btn-sm btn-warning" >Process</button></form>';
          console.log(button);
        }
        $dialog.find('#content-dialog').html(message.replaceAll(' ', '&nbsp;').replaceAll('*', '')+button);
      }
      $dialog.modal();
    },
    hide: function() {
      $dialog.modal('hide');
    }
  };

})(jQuery);

function copyToClipboard(text) {
  var textArea = document.createElement("textarea");
  textArea.style.position = 'fixed';
  textArea.style.top = 0;
  textArea.style.left = 0;
  textArea.style.width = '2em';
  textArea.style.height = '2em';
  textArea.style.padding = 0;
  textArea.style.border = 'none';
  textArea.style.outline = 'none';
  textArea.style.boxShadow = 'none';
  textArea.style.background = 'transparent';
  textArea.value = text.replaceAll('<br>', '\n');

  document.body.appendChild(textArea);
  textArea.focus();
  textArea.select();

  try {
    var successful = document.execCommand('copy');
    var msg = successful ? 'successful' : 'unsuccessful';
    console.log('Copying text command was ' + msg);
  } catch (err) {
    console.log('Oops, unable to copy');
  }
  document.body.removeChild(textArea);
}


const Loading = function(element, options) {
  this.element = element;
  this.settings = $.extend({}, Loading.defaults, options);
  this.settings.fullPage = this.element.is("body");

  this.init();

  if (this.settings.start) {
    this.start();
  }
};

Loading.defaults = {
  /**
   * jQuery element to be used as overlay
   * If not defined, a default overlay will be created
   */
  overlay: undefined,

  /**
   * z-index to be used by the default overlay
   * If not defined, a z-index will be calculated based on the
   * target's z-index
   * Has no effect if a custom overlay is defined
   */
  zIndex: undefined,

  /**
   * Message to be rendered on the overlay content
   * Has no effect if a custom overlay is defined
   */
  message: "Loading...",

  /**
   * Theme to be applied on the loading element
   *
   * Some default themes are implemented on `jquery.loading.css`, but you can
   *  define your own. Just add a `.loading-theme-my_awesome_theme` selector
   *  somewhere with your custom styles and change this option
   *  to 'my_awesome_theme'. The class is applied to the parent overlay div
   *
   * Has no effect if a custom overlay is defined
   */
  theme: "light",

  /**
   * Class(es) to be applied to the overlay element when the loading state is started
   */
  shownClass: "loading-shown",

  /**
   * Class(es) to be applied to the overlay element when the loading state is stopped
   */
  hiddenClass: "loading-hidden",

  /**
   * Set to true to stop the loading state if the overlay is clicked
   * This options does NOT override the onClick event
   */
  stoppable: false,

  /**
   * Set to false to not start the loading state when initialized
   */
  start: true,

  /**
   * Function to be executed when the loading state is started
   * Receives the loading object as parameter
   *
   * The function is attached to the `loading.start` event
   */
  onStart: function(loading) {
    loading.overlay.fadeIn(150);
  },

  /**
   * Function to be executed when the loading state is stopped
   * Receives the loading object as parameter
   *
   * The function is attached to the `loading.stop` event
   */
  onStop: function(loading) {
    loading.overlay.fadeOut(150);
  },

  /**
   * Function to be executed when the overlay is clicked
   * Receives the loading object as parameter
   *
   * The function is attached to the `loading.click` event
   */
  onClick: function() {}
};

/**
 * Extend the Loading plugin default settings with the user options
 * Use it as `$.Loading.setDefaults({ ... })`
 *
 * @param {Object} options Custom options to override the plugin defaults
 */
Loading.setDefaults = function(options) {
  Loading.defaults = $.extend({}, Loading.defaults, options);
};

$.extend(Loading.prototype, {
  /**
   * Initializes the overlay and attach handlers to the appropriate events
   */
  init: function() {
    this.isActive = false;
    this.overlay = this.settings.overlay || this.createOverlay();
    this.resize();
    this.attachMethodsToExternalEvents();
    this.attachOptionsHandlers();
  },

  /**
   * Return a new default overlay
   *
   * @return {jQuery} A new overlay already appended to the page's body
   */
  createOverlay: function() {
    var overlay = $(
        '<div class="loading-overlay loading-theme-' +
        this.settings.theme +
        '"><div class="loading-overlay-content">' +
        this.settings.message +
        "</div></div>"
      )
      .addClass(this.settings.hiddenClass)
      .hide()
      .appendTo("body");

    var elementID = this.element.attr("id");
    if (elementID) {
      overlay.attr("id", elementID + "_loading-overlay");
    }

    return overlay;
  },

  /**
   * Attach some internal methods to external events
   * e.g. overlay click, window resize etc
   */
  attachMethodsToExternalEvents: function() {
    var self = this;

    // Add `shownClass` and remove `hiddenClass` from overlay when loading state
    // is activated
    self.element.on("loading.start", function() {
      self.overlay
        .removeClass(self.settings.hiddenClass)
        .addClass(self.settings.shownClass);
    });

    // Add `hiddenClass` and remove `shownClass` from overlay when loading state
    // is stopped
    self.element.on("loading.stop", function() {
      self.overlay
        .removeClass(self.settings.shownClass)
        .addClass(self.settings.hiddenClass);
    });

    // Attach the 'stop loading on click' behaviour if the `stoppable` option is set
    if (self.settings.stoppable) {
      self.overlay.on("click", function() {
        self.stop();
      });
    }

    // Trigger the `loading.click` event if the overlay is clicked
    self.overlay.on("click", function() {
      self.element.trigger("loading.click", self);
    });

    // Bind the `resize` method to `window.resize`
    $(window).on("resize", function() {
      self.resize();
    });

    // Bind the `resize` method to `document.ready` to guarantee right
    // positioning and dimensions after the page is loaded
    $(function() {
      self.resize();
    });
  },

  /**
   * Attach the handlers defined on `options` for the respective events
   */
  attachOptionsHandlers: function() {
    var self = this;

    self.element.on("loading.start", function(event, loading) {
      self.settings.onStart(loading);
    });

    self.element.on("loading.stop", function(event, loading) {
      self.settings.onStop(loading);
    });

    self.element.on("loading.click", function(event, loading) {
      self.settings.onClick(loading);
    });
  },

  /**
   * Calculate the z-index for the default overlay element
   * Return the z-index passed as setting to the plugin or calculate it
   * based on the target's z-index
   */
  calcZIndex: function() {
    if (this.settings.zIndex !== undefined) {
      return this.settings.zIndex;
    } else {
      return (
        (parseInt(this.element.css("z-index")) || 0) +
        1 +
        this.settings.fullPage
      );
    }
  },

  /**
   * Reposition the overlay on the top of the target element
   * This method needs to be called if the target element changes position
   *  or dimension
   */
  resize: function() {
    var self = this;

    var element = self.element,
      totalWidth = element.outerWidth(),
      totalHeight = element.outerHeight();

    if (this.settings.fullPage) {
      totalHeight = "100%";
      totalWidth = "100%";
    }

    this.overlay.css({
      position: self.settings.fullPage ? "fixed" : "absolute",
      zIndex: self.calcZIndex(),
      top: element.offset().top,
      left: element.offset().left,
      width: totalWidth,
      height: totalHeight
    });
  },

  /**
   * Trigger the `loading.start` event and turn on the loading state
   */
  start: function() {
    this.isActive = true;
    this.resize();
    this.element.trigger("loading.start", this);
  },

  /**
   * Trigger the `loading.stop` event and turn off the loading state
   */
  stop: function() {
    this.isActive = false;
    this.element.trigger("loading.stop", this);
  },

  /**
   * Check whether the loading state is active or not
   *
   * @return {Boolean}
   */
  active: function() {
    return this.isActive;
  },

  /**
   * Toggle the state of the loading overlay
   */
  toggle: function() {
    if (this.active()) {
      this.stop();
    } else {
      this.start();
    }
  },

  /**
   * Destroy plugin instance.
   */
  destroy: function() {
    this.overlay.remove();
  }
});

/**
 * Name of the data attribute where the plugin object will be stored
 */
var dataAttr = "jquery-loading";

/**
 * Initializes the plugin and return a chainable jQuery object
 *
 * @param {Object} [options] Initialization options. Extends `Loading.defaults`
 * @return {jQuery}
 */
$.fn.loading = function(options) {
  return this.each(function() {
    // (Try to) retrieve an existing plugin object associated with element
    var loading = $.data(this, dataAttr);

    if (!loading) {
      // First call. Initialize and save plugin object
      if (
        options === undefined ||
        typeof options === "object" ||
        options === "start" ||
        options === "toggle"
      ) {
        // Initialize it just if argument is undefined, a config object
        // or a direct call to 'start' or 'toggle' methods
        $.data(this, dataAttr, new Loading($(this), options));
      }
    } else {
      // Already initialized
      if (options === undefined) {
        // $(...).loading() call. Call the 'start' by default
        loading.start();
      } else if (typeof options === "string") {
        // $(...).loading('method') call. Execute 'method'
        loading[options].apply(loading);
      } else {
        // $(...).loading({...}) call. New configurations. Reinitialize
        // plugin object with new config options and start the plugin
        // Also, destroy the old overlay instance
        loading.destroy();
        $.data(this, dataAttr, new Loading($(this), options));
      }
    }
  });
};

$.fn.Loading = function(options) {
  var loading = $(this).data(dataAttr);

  if (!loading || options !== undefined) {
    $(this).data(dataAttr, (loading = new Loading($(this), options)));
  }

  return loading;
};

$.expr[":"].loading = function(element) {
  var loadingObj = $.data(element, dataAttr);

  if (!loadingObj) {
    return false;
  }

  return loadingObj.active();
};

$.Loading = Loading;
