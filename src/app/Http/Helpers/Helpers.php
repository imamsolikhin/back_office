<?php
use Illuminate\Support\Facades\Config;

function makeResponse($statusCode, $status, $message, $data = null, $headers = [])
{
    $result = [
        'status_id' => $statusCode,
        'status' => $status == 'pagination' ? 'success' : $status,
        'message' => $message,
        'data' => $data,
    ];

    if ($status == 'pagination') {
        $result = array_merge($result, ['paginator' => [
            'total_records' => (int) $data->total(),
            'total_pages' => (int) $data->lastPage(),
            'current_page' => (int) $data->currentPage(),
            'per_page' => (int) $data->perPage(),
        ]]);
    }

    return response()->json($result, $statusCode, $headers);
}

function prefix($str, $length)
{
    return str_pad($str, $length, '0', STR_PAD_LEFT);
}

function currDate()
{
    date_default_timezone_set('Asia/Jakarta');
    return date("Y-m-d H:i", time());
}

function sess_company($prm = null)
{
    if ($prm) {
        if (Session::get('com') != null) {
            return Session::get('com')[$prm];
        }
    }
    return null;
}

function sess_user($prm = null)
{
    if ($prm) {
        if (Session::get('user') != null) {
            return Session::get('user')[$prm];
        }
    }
    return null;
}

function sess_shift($prm = null)
{
    if ($prm) {
        if (Session::get('shift') != null) {
            return Session::get('shift')[$prm];
        }
        return 0;
    }
    return null;
}

function getUserIP()
{
    $client = @$_SERVER['HTTP_CLIENT_IP'];
    $forward = @$_SERVER['HTTP_X_FORWARDED_FOR'];
    $remote = $_SERVER['REMOTE_ADDR'];

    if (filter_var($client, FILTER_VALIDATE_IP)) {
        $ip = $client;
    } elseif (filter_var($forward, FILTER_VALIDATE_IP)) {
        $ip = $forward;
    } else {
        $ip = $remote;
    }

    return $ip;
}

function swalError($message = 'Validation Error!')
{
    while (DB::transactionLevel() > 0) {
        DB::rollBack();
    }

    return redirect()->back()->withInput(request()->except('_token'))->with('swal_error', $message);
}

function user()
{
    $user = Auth::user();
    return $user;
}

function list_confirm_status(){
  return [
    [#0
      0 => 'Waiting',
      1 => 'Waiting'
    ],
    [#1
      0 => 'Packing',
      1 => 'Packing'
    ],
    [#2
      0 => 'Ready Delived',
      1 => 'Ready Deliverd'
    ],
    [#3
      0 => 'Delivery Courier',
      1 => 'Delivery Courier'
    ],
    [#4
      0 => 'Return',
      1 => 'Return'
    ],
    [#5
      0 => 'Success',
      1 => 'Sukses'
    ],
    [#6
      0 => 'Cancel',
      1 => 'Cancel'
    ]
  ];
}

function list_model($path, $module, $where = null, $order = null, $column=null, $start=null, $end=null)
{
    $data = DB::table(getTableName($path, $module));
    $data->select(getTableName($path, $module).'.*');
    if (strtolower($module) == "company") {
      $data->join('company_auth', function($join)use($path,$module){
          $join->on('company_auth.company_child', '=', getTableName($path, $module).'.id');
          $join->where('company_auth.status',1);
      });
      $data->where('company_auth.company_id',sess_user('company_id'));
      $data->where(getTableName($path, $module).'.status', 1);
    }else{
      $data->where(DB::raw(getTableName($path, $module).'.company_id'),sess_user('company_id'));
      // dd(getSql($data));
    }
    if ($where) {
        $data->whereRaw(DB::raw(getTableName($path, $module).'.'.$where));
    }
    if ($column) {
        $data->whereBetween($column, [$start,$end]);
    }
    if ($order) {
        $data->orderByRaw(DB::raw($order));
    }

    return $data->get();
}

function list_province()
{
  $result = \DB::table('mst_destination')->select('*')
            ->groupBy('province')
            ->get();
    return $result;
}

function getSql($model)
{
    $replace = function ($sql, $bindings) {
        $needle = '?';
        foreach ($bindings as $replace) {
            $pos = strpos($sql, $needle);
            if ($pos !== false) {
                if (gettype($replace) === "string") {
                    $replace = ' "' . addslashes($replace) . '" ';
                }
                $sql = substr_replace($sql, $replace, $pos, strlen($needle));
            }
        }
        return $sql;
    };
    $sql = $replace($model->toSql(), $model->getBindings());

    return $sql;
}

function getRoutes($path, $module)
{
    return route(strtolower($path).'.index',strtolower($module));
}

function getTableName($path, $module)
{
    $table =  'App\Http\Resources\\' . $path .'\\' . getKeyName($module);
    $table = new $table();
    return $table->getTable();
}


function getKeyName($module)
{
    return str_replace(' ', '', ucwords(str_replace('-', ' ', $module)));
}

function getModelName($folder, $module)
{
    return 'App\Http\Models\\' . $folder .'\\' . getKeyName($module);
}

function getResourceName($folder, $module)
{
    return 'App\Http\Resources\\' . $folder .'\\' . getKeyName($module);
}

function getControllerName($folder, $module)
{
    return 'App\Http\Controllers\\'. $folder .'\\' . getKeyName($module) . 'Controller';
}

function generadeCode($folder, $table, $branch=null, $prfix=null, $numb=5)
{
    $branch = ($branch)? $branch."-":"";
    $prfix = ($prfix)? $branch.$prfix:$branch;
    $last_count = getResourceName($folder, $table)::where('company_id', sess_company('id'))->count();
    $code = "";
    for ($i=1; $i <= 1000; $i++) {
      $code = $prfix.str_pad(($last_count+$i), $numb, '0', STR_PAD_LEFT);
      $ch = getResourceName($folder, $table)::where('id', $code)->count();
      if($ch==0){
        break;
      }
    }
    return $code;
}

function to_bool($val = null)
{
    if ($val == "on" || $val != null) {
        return 1;
    } else {
        return 0;
    }
}

function listUser($auth = null){
  if($auth){
    return \DB::table('users')
              ->orderBy('users.company_id','ASC')
              ->orderBy('users.name','ASC')
              ->get();
  }
  return \DB::table('users')
            ->where('users.company_id',sess_user('company_id'))
            ->get();
}
function listMenu()
{
    $menus = config_menu(null);
    $list['items'] = [];
    $dashboard = [
          'title' => 'Dashboard',
          'root' => true,
          'icon' => 'media/svg/icons/Home/Flower3.svg',
          'page' => sess_user('dashboard_url'),
          'new-tab' => false,
        ];
    array_push($list,$dashboard);
    if(sess_user('role_id') == 'DEV' AND sess_user('role_id') == 'MNG'){
      array_push($list,['section' => 'MENU SYSTEM']);
    }
    foreach ($menus as $id => $rs) {
      $head_mn = $rs[2];
      $sub_mn = config_menu($rs[0]);
      $head_mn["submenu"] = $sub_mn;
      if($sub_mn){
        if(sess_user('role_id') != 'DEV' AND sess_user('role_id') != 'MNG'){
          array_push($list,['section' => $rs[1]]);
          array_push($list,$sub_mn);
        }else{
          array_push($list,$head_mn);
        }
      }
    }
    $dashboard = [
          'title' => 'Logout',
          'root' => true,
          'icon' => 'media/svg/icons/Home/Flower3.svg',
          'page' => 'logout',
          'new-tab' => false,
        ];
    array_push($list,$dashboard);
    return $list;
}

function config_menu($parent)
{
    $table = new App\Models\RoleAuth();
    $menus = \DB::table($table->getTable())
              ->select(
                 "role_menu_auth.*"
                ,"menus.name as menu_name"
                ,"menus.url as menu_url"
                ,"menu_icons.icon_url as menu_icon"
              )
              ->join('roles', 'roles.id', '=', 'role_menu_auth.role_id')
              ->join('menus', 'menus.id', '=', 'role_menu_auth.menu_id')
              ->join('menu_icons', 'menu_icons.id', '=', 'menus.icon')
              ->where('role_menu_auth.view',1)
              ->where(function ($query) use ($parent) {
                  $query->where('menus.parent_id',$parent);
                  if($parent){
                      $query->where('role_menu_auth.role_id',sess_user('role_id'));
                  }
              })
              ->groupBy('menus.id')
              ->get();
    $list = [];
    foreach ($menus as $rs) {
      $mn = [
            'title' => $rs->menu_name,
            'root' => true,
            'icon' => $rs->menu_icon,
            'page' => $rs->menu_url,
            'new-tab' => false
          ];

      if($parent){
        array_push($list,$mn);
      }else {
        array_push($list,[$rs->id,$rs->menu_name,$mn]);
      }

    }
    return $list;
}

function getAuthMenu($module, $key)
{
    $flag = App\Models\RoleAuth::select("role_menu_auth.".$key." AS key")
                ->join('menus', 'menus.id', '=', 'role_menu_auth.menu_id')
                ->where('role_menu_auth.role_id', sess_user('role_id'))
                ->where('menus.url', \URL::route($module, [], false))
                ->first();
    if ($flag) {
        return $flag->key;
    }
    return false;
}

function getAttrLogin()
{
    $data[] = 'clinic';
    $data[] = 'marketing-clinic';
    $data[] = 'marketing-product';

    foreach ($data as $val) {
        if ($val == ENV('APP_BRANCH')) {
            return true;
        }
    }
    return false;
}

function hari_ini($date = null)
{
    $hari = ($date) ? date("D", $date):date("D");
    switch ($hari) {
      case 'Sun':
          $hari_ini = "Minggu";
      break;

      case 'Mon':
          $hari_ini = "Senin";
      break;

      case 'Tue':
          $hari_ini = "Selasa";
      break;

      case 'Wed':
          $hari_ini = "Rabu";
      break;

      case 'Thu':
          $hari_ini = "Kamis";
      break;

      case 'Fri':
          $hari_ini = "Jumat";
      break;

      case 'Sat':
          $hari_ini = "Sabtu";
      break;

      default:
          $hari_ini = "Tidak di ketahui";
      break;
  }

    return $hari_ini;
}
