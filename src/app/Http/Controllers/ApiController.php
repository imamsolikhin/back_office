<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class ApiController extends Controller{

    public static function destination(Request $request){
      $groupby = 'area';
      if($request->province){
        $groupby = 'city';
      }
      if($request->city AND $request->province){
        $groupby = 'area';
      }
      $result = \DB::table('mst_destination')->select('*')
                ->where(function ($query)use($request) {
                    if($request->province){
                      $query->where('province',$request->province);
                    }
                    if($request->city){
                      $query->where('city',$request->city);
                    }
                })
                ->groupBy($groupby)
                ->orderBy($groupby,'ASC')
                ->get();

      // dd($result);
      return makeResponse(200, 'success', null, $result);
    }
}
