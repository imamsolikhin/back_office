<?php

namespace App\Http\Controllers\Warehouse;

use Session;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use PHPJasper\PHPJasper;

class PackingDashboardController extends Controller {
    private static $module;
    private static $dashboard_date;
    private static $dashboard_startdate;
    private static $dashboard_enddate;
    public static function init(){
        static::$module = 'Dashboard Product';
        static::$dashboard_date = date('d-m-Y 00:00').' <=> '.date('d-m-Y 24:00');
        static::$dashboard_startdate = date('Y-m-d 00:00');
        static::$dashboard_enddate = date('Y-m-d 23:59');
    }
    public static function index($request){
        static::init();
        $start_date = $request->dashboard_startdate ? $request->dashboard_startdate:static::$dashboard_startdate;
        $end_date = $request->dashboard_enddate ? $request->dashboard_enddate:static::$dashboard_enddate;
        $dash = \DB::table('sls_product_visitor')
                    ->select('sls_product_visitor.*'
                      ,'mst_customer.full_name as full_name'
                      ,'mst_customer.phone as phone'
                      ,'mst_gender.id as gender_id'
                      ,'mst_gender.name as gender_name'
                      ,'mst_advertise.name as advertise_name'
                    )
                    ->join('mst_customer','mst_customer.id','=', 'sls_product_visitor.customer_id')
                    ->join('mst_gender','mst_gender.id','=', 'mst_customer.gender_id')
                    ->join('mst_advertise','mst_advertise.id','=', 'mst_customer.advertise_id')
                    ->where(function ($query)use($request) {
                      if($request->company_id){
                          $query->where('sls_product_visitor.company_id',$request->company_id);
                      }else{
                        $query->where(function ($query)use($request) {
                          if($request->company_id){
                              $query->where('sls_product_visitor.company_id',$request->company_id);
                          }else{
                            $query->where('sls_product_visitor.company_id',sess_user('company_id'));
                          }
                        });
                      }
                    })
                    ->where('sls_product_visitor.created_by',sess_user('id'))
                    ->where('sls_product_visitor.created_at','>=' , $start_date)
                    ->where('sls_product_visitor.created_at','<=' , $end_date)
                    ->orderBy('sls_product_visitor.created_at','DESC')
                    ->get();
        $dash_track = \DB::table('sls_product_visitor')
                    ->select('sls_product_visitor.*'
                      ,'mst_customer.full_name as full_name'
                      ,'mst_customer.phone as phone'
                      ,'mst_gender.id as gender_id'
                      ,'mst_gender.name as gender_name'
                      ,'mst_advertise.name as advertise_name'
                    )
                    ->join('mst_customer','mst_customer.id','=', 'sls_product_visitor.customer_id')
                    ->join('mst_gender','mst_gender.id','=', 'mst_customer.gender_id')
                    ->join('mst_advertise','mst_advertise.id','=', 'mst_customer.advertise_id')
                    ->where(function ($query)use($request) {
                      if($request->company_id){
                          $query->where('sls_product_visitor.company_id',$request->company_id);
                      }else{
                        $query->where('sls_product_visitor.company_id',sess_user('company_id'));
                      }
                    })
                    ->where('sls_product_visitor.created_by',sess_user('id'))
                    ->where('sls_product_visitor.confirm_status','!=',null)
                    ->where('sls_product_visitor.transaction_date','>=' , $start_date)
                    ->where('sls_product_visitor.transaction_date','<=' , $end_date)
                    ->orderBy('sls_product_visitor.transaction_date','DESC')
                    ->get();
        $dash1 = \DB::table('sls_product_visitor')
                    ->select(\DB::raw('count(sls_product_visitor.id) as jml'))
                    ->join('mst_customer','mst_customer.id','=', 'sls_product_visitor.customer_id')
                    ->join('mst_gender','mst_gender.id','=', 'mst_customer.gender_id')
                    ->join('mst_advertise','mst_advertise.id','=', 'mst_customer.advertise_id')
                    ->where(function ($query)use($request) {
                      if($request->company_id){
                          $query->where('sls_product_visitor.company_id',$request->company_id);
                      }else{
                        $query->where('sls_product_visitor.company_id',sess_user('company_id'));
                      }
                    })
                    ->where('sls_product_visitor.created_by',sess_user('id'))
                    ->where('sls_product_visitor.followup_status',0)
                    ->where('sls_product_visitor.created_at','>=' , $start_date)
                    ->where('sls_product_visitor.created_at','<=' , $end_date)
                    ->first();
        $dash2 = \DB::table('sls_product_visitor')
                    ->select(\DB::raw('count(sls_product_visitor.id) as jml'))
                    ->join('mst_customer','mst_customer.id','=', 'sls_product_visitor.customer_id')
                    ->join('mst_gender','mst_gender.id','=', 'mst_customer.gender_id')
                    ->join('mst_advertise','mst_advertise.id','=', 'mst_customer.advertise_id')
                    ->where(function ($query)use($request) {
                      if($request->company_id){
                          $query->where('sls_product_visitor.company_id',$request->company_id);
                      }else{
                        $query->where('sls_product_visitor.company_id',sess_user('company_id'));
                      }
                    })
                    ->where('sls_product_visitor.created_by',sess_user('id'))
                    ->where('sls_product_visitor.followup_status',1)
                    ->where('sls_product_visitor.created_at','>=' , $start_date)
                    ->where('sls_product_visitor.created_at','<=' , $end_date)
                    ->first();
        $dash3 = \DB::table('sls_product_visitor')
                    ->select(\DB::raw('count(sls_product_visitor.id) as jml'))
                    ->join('mst_customer','mst_customer.id','=', 'sls_product_visitor.customer_id')
                    ->join('mst_gender','mst_gender.id','=', 'mst_customer.gender_id')
                    ->join('mst_advertise','mst_advertise.id','=', 'mst_customer.advertise_id')
                    ->where(function ($query)use($request) {
                      if($request->company_id){
                          $query->where('sls_product_visitor.company_id',$request->company_id);
                      }else{
                        $query->where('sls_product_visitor.company_id',sess_user('company_id'));
                      }
                    })
                    ->where('sls_product_visitor.created_by',sess_user('id'))
                    ->where('sls_product_visitor.followup_status',0)
                    ->where('sls_product_visitor.confirm_status',list_confirm_status()[5][0])
                    ->where('sls_product_visitor.created_at','>=' , $start_date)
                    ->where('sls_product_visitor.created_at','<=' , $end_date)
                    ->first();
        $dash4 = \DB::table('sls_product_visitor')
                    ->select(\DB::raw('count(sls_product_visitor.id) as jml'))
                    ->join('mst_customer','mst_customer.id','=', 'sls_product_visitor.customer_id')
                    ->join('mst_gender','mst_gender.id','=', 'mst_customer.gender_id')
                    ->join('mst_advertise','mst_advertise.id','=', 'mst_customer.advertise_id')
                    ->where(function ($query)use($request) {
                      if($request->company_id){
                          $query->where('sls_product_visitor.company_id',$request->company_id);
                      }else{
                        $query->where('sls_product_visitor.company_id',sess_user('company_id'));
                      }
                    })
                    ->where('sls_product_visitor.created_by',sess_user('id'))
                    ->where('sls_product_visitor.followup_status',1)
                    ->where('sls_product_visitor.confirm_status',list_confirm_status()[5][0])
                    ->where('sls_product_visitor.created_at','>=' , $start_date)
                    ->where('sls_product_visitor.created_at','<=' , $end_date)
                    ->first();

        $data["module"] = static::$module;
        $data["dash"] = $dash ? $dash:null;
        $data["dash_track"] = $dash_track ? $dash_track:null;
        $data["dash1"] = $dash1 ? ["jml"=>$dash1->jml,"name"=>"Interaksi Lead"]:["jml"=>0,"name"=>"Interaksi Lead"];
        $data["dash2"] = $dash2 ? ["jml"=>$dash2->jml,"name"=>"Interaksi Followup"]:["jml"=>0,"name"=>"Interaksi Followup"];
        $data["dash3"] = $dash3 ? ["jml"=>$dash3->jml,"name"=>"Closing Lead"]:["jml"=>0,"name"=>"Closing Lead"];
        $data["dash4"] = $dash4 ? ["jml"=>$dash4->jml,"name"=>"Closing Followup"]:["jml"=>0,"name"=>"Closing Followup"];
        $data["dashboard_date"] = $request->dashboard_date ? $request->dashboard_date:static::$dashboard_date;
        $data["dashboard_startdate"] = $request->dashboard_startdate ? $request->dashboard_startdate:static::$dashboard_startdate;
        $data["dashboard_enddate"] = $request->dashboard_enddate ? $request->dashboard_enddate:static::$dashboard_enddate;
        $data["dashboard_datetime"] = date('m-d-Y H:i',strtotime($data["dashboard_startdate"]))." <=> ".date('m-d-Y H:i',strtotime($data["dashboard_enddate"]));
        return view('product.dashboard',$data);
    }

}
