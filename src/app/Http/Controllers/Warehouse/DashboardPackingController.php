<?php

namespace App\Http\Controllers\Warehouse;

use Session;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use PHPJasper\PHPJasper;

class DashboardPackingController extends Controller {
    private static $module;
    private static $dashboard_date;
    private static $dashboard_startdate;
    private static $dashboard_enddate;
    public static function init(){
        static::$module = 'Dashboard Packing Product';
        static::$dashboard_date = date('d-m-Y 00:00').' <=> '.date('d-m-Y 24:00');
        static::$dashboard_startdate = date('Y-m-d 00:00');
        static::$dashboard_enddate = date('Y-m-d 23:59');
    }
    public static function index($request){
        static::init();
        $start_date = $request->dashboard_startdate ? $request->dashboard_startdate:static::$dashboard_startdate;
        $end_date = $request->dashboard_enddate ? $request->dashboard_enddate:static::$dashboard_enddate;
        $dash = \DB::table('sls_product_visitor')
                    ->select('sls_product_visitor.*'
                      ,'mst_customer.full_name as full_name'
                      ,'mst_customer.phone as phone'
                      ,'mst_gender.id as gender_id'
                      ,'mst_gender.name as gender_name'
                      ,'mst_advertise.name as advertise_name'
                    )
                    ->join('mst_customer','mst_customer.id','=', 'sls_product_visitor.customer_id')
                    ->join('mst_gender','mst_gender.id','=', 'mst_customer.gender_id')
                    ->join('mst_advertise','mst_advertise.id','=', 'mst_customer.advertise_id')
                    ->where(function ($query)use($request) {
                      if($request->company_id){
                          $query->where('sls_product_visitor.company_id',$request->company_id);
                      }else{
                        $query->where('sls_product_visitor.company_id',sess_user('company_id'));
                      }
                    })
                    ->where('sls_product_visitor.packing_id',sess_user('id'))
                    ->where('sls_product_visitor.packing_date','>=' , $start_date)
                    ->where('sls_product_visitor.packing_date','<=' , $end_date)
                    ->orderBy('sls_product_visitor.packing_date','DESC')
                    ->get();
        $dash_track = \DB::table('sls_product_visitor')
                    ->select('sls_product_visitor.*'
                      ,'mst_customer.full_name as full_name'
                      ,'mst_customer.phone as phone'
                      ,'mst_gender.id as gender_id'
                      ,'mst_gender.name as gender_name'
                      ,'mst_advertise.name as advertise_name'
                    )
                    ->join('mst_customer','mst_customer.id','=', 'sls_product_visitor.customer_id')
                    ->join('mst_gender','mst_gender.id','=', 'mst_customer.gender_id')
                    ->join('mst_advertise','mst_advertise.id','=', 'mst_customer.advertise_id')
                    ->where(function ($query)use($request) {
                      if($request->company_id){
                          $query->where('sls_product_visitor.company_id',$request->company_id);
                      }else{
                        $query->where('sls_product_visitor.company_id',sess_user('company_id'));
                      }
                    })
                    ->where('sls_product_visitor.packing_id',sess_user('id'))
                    ->where('sls_product_visitor.confirm_status','!=',null)
                    ->where('sls_product_visitor.updated_at','>=' , $start_date)
                    ->where('sls_product_visitor.updated_at','<=' , $end_date)
                    ->orderBy('sls_product_visitor.updated_at','DESC')
                    ->get();
        $dash1 = \DB::table('sls_sales_order')
                    ->select(
                    \DB::raw('count(sls_sales_order.id) as jml'),
                    \DB::raw('sls_sales_order.confirm_status as status')
                    )
                    ->where(function ($query)use($request) {
                      if($request->company_id){
                          $query->where('sls_sales_order.company_id',$request->company_id);
                      }else{
                        $query->where('sls_sales_order.company_id',sess_user('company_id'));
                      }
                    })
                    ->where('sls_sales_order.packing_id',sess_user('id'))
                    ->where('sls_sales_order.confirm_status','!=',null)
                    ->where('sls_sales_order.transaction_date','>=' , $start_date)
                    ->where('sls_sales_order.transaction_date','<=' , $end_date)
                    ->groupBy('sls_sales_order.confirm_status')
                    ->orderBy('sls_sales_order.transaction_date','DESC')
                    ->get();
        $dash2 = \DB::table('sls_sales_order')
                    ->select(
                    \DB::raw('IFNULL(COUNT(sls_sales_order.id),0) as jml')
                    ,\DB::raw('1000 as commission_price')
                    )
                    ->where(function ($query)use($request) {
                      if($request->company_id){
                          $query->where('sls_sales_order.company_id',$request->company_id);
                      }else{
                        $query->where('sls_sales_order.company_id',sess_user('company_id'));
                      }
                    })
                    ->where('sls_sales_order.packing_id',sess_user('id'))
                    ->where('sls_sales_order.confirm_status',list_confirm_status()[5][0])
                    ->where('sls_sales_order.packing_date','>=' , $start_date)
                    ->where('sls_sales_order.packing_date','<=' , $end_date)
                    ->first();
        $dash3 = \DB::table('sls_sales_order')
                    ->select(
                    \DB::raw('IFNULL(COUNT(sls_sales_order.id),0) as jml')
                    ,\DB::raw('1000 as commission_price')
                    )
                    ->where(function ($query)use($request) {
                      if($request->company_id){
                          $query->where('sls_sales_order.company_id',$request->company_id);
                      }else{
                        $query->where('sls_sales_order.company_id',sess_user('company_id'));
                      }
                    })
                    ->where('sls_sales_order.packing_id',sess_user('id'))
                    ->where('sls_sales_order.confirm_status',list_confirm_status()[3][0])
                    ->where('sls_sales_order.packing_date','>=' , $start_date)
                    ->where('sls_sales_order.packing_date','<=' , $end_date)
                    ->first();

        $dash4 = \DB::table('sls_sales_order')
                    ->select(
                    \DB::raw('count(sls_sales_order.id) as jml'),
                    \DB::raw('sls_sales_order.confirm_status as status')
                    )
                    ->join('mst_customer','mst_customer.id','=', 'sls_sales_order.customer_id')
                    ->join('mst_gender','mst_gender.id','=', 'mst_customer.gender_id')
                    ->join('mst_advertise','mst_advertise.id','=', 'mst_customer.advertise_id')
                    ->where(function ($query)use($request) {
                      if($request->company_id){
                          $query->where('sls_sales_order.company_id',$request->company_id);
                      }else{
                        $query->where('sls_sales_order.company_id',sess_user('company_id'));
                      }
                    })
                    ->where('sls_sales_order.confirm_status','!=',null)
                    ->where('sls_sales_order.packing_id',sess_user('id'))
                    ->where('sls_sales_order.created_at','>=' , $start_date)
                    ->where('sls_sales_order.created_at','<=' , $end_date)
                    ->groupBy('sls_sales_order.confirm_status')
                    ->get();

        $data["module"] = static::$module;
        $data["dash"] = $dash ? $dash:null;
        $data["dash_track"] = $dash_track ? $dash_track:null;
        $data["dash1"] = $dash1;
        $data["dash2"] = $dash2;
        $data["dash3"] = $dash3;
        $data["dash4"] = $dash4;
        $data["dashboard_date"] = $request->dashboard_date ? $request->dashboard_date:static::$dashboard_date;
        $data["dashboard_startdate"] = $request->dashboard_startdate ? $request->dashboard_startdate:static::$dashboard_startdate;
        $data["dashboard_enddate"] = $request->dashboard_enddate ? $request->dashboard_enddate:static::$dashboard_enddate;
        $data["dashboard_datetime"] = date('m-d-Y H:i',strtotime($data["dashboard_startdate"]))." <=> ".date('m-d-Y H:i',strtotime($data["dashboard_enddate"]));
        return view('warehouse.dashboard-packing',$data);
    }

}
