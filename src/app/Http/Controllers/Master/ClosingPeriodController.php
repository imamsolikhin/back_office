<?php

namespace App\Http\Controllers\Master;

use DataTables;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Carbon\Carbon;

class ClosingPeriodController extends Controller {

    private static $module;
    private static $module_alias;
    private static $auth;
    private static $path;
    private static $data;
    private static $delete;
    private static $controller;
    private static $resource;
    private static $table;
    private static $dashboard_date;
    private static $dashboard_startdate;
    private static $dashboard_enddate;

    public static function init()
    {
        static::$module = 'Closing Period';
        static::$module_alias = 'Closing Period';
        static::$auth = 'closing-period';
        static::$path = route('master.index','closing-period');
        static::$data = route('master.list','closing-period');
        static::$delete = route('master.delete',['closing-period','']);
        static::$controller = getControllerName("Master", "closing-period");
        static::$resource = getResourceName("Master", "closing-period");
        static::$table = new static::$resource();
        static::$dashboard_date = date('d-m-Y 00:00').' <=> '.date('d-m-Y 24:00');
        static::$dashboard_startdate = date('Y-m-d 00:00');
        static::$dashboard_enddate = date('Y-m-d 23:59');
    }

    public static function index($request) {
      self::init();
      $data["dashboard_date"] = $request->dashboard_date ? $request->dashboard_date:static::$dashboard_date;
      $data["dashboard_startdate"] = $request->dashboard_startdate ? $request->dashboard_startdate:static::$dashboard_startdate;
      $data["dashboard_enddate"] = $request->dashboard_enddate ? $request->dashboard_enddate:static::$dashboard_enddate;
      $data["dashboard_datetime"] = date('m-d-Y H:i',strtotime($data["dashboard_startdate"]))." <=> ".date('m-d-Y H:i',strtotime($data["dashboard_enddate"]));
      $data['module'] = static::$module;
      $data['auth'] = static::$auth;
      $data['path'] = static::$path;
      $data['data'] = static::$data;
      return view('master.closing-period',$data);
    }

    public static function data($id) {
        self::init();
        $module = static::$resource::withoutGlobalScopes(['active'])->findOrFail($id);
        return makeResponse(200, 'success', null, $module);
    }

    public static function save($request) {
        self::init();
        $module = static::$controller::execute($request);
        return redirect()->route('master.index',static::$auth)->with('notif_success', 'New '.static::$module_alias.' '. $request->name .' has been added successfully!');
    }

    public static function update($id, $request) {
        self::init();
        $data = static::$resource::find(str_replace('%20', ' ', $id));
        if (!$data) return redirect()->route('master.index',static::$auth)->with('notif_danger', 'Data '. $id .' not found!');
        $so = \DB::table('sls_sales_order')
              ->join('sls_sales_order_closing_detail','sls_sales_order_closing_detail.sales_order_id','=', 'sls_sales_order.id')
              ->join('sls_sales_order_closing','sls_sales_order_closing.id','=', 'sls_sales_order_closing_detail.sales_order_closing_id')
              ->where('sls_sales_order.company_id',$data->company_id)
              ->where(\DB::raw('IFNULL(sls_sales_order.closing_status,0)'),0)
              ->whereDate('sls_sales_order.transaction_date',">=",date("Y-m-d",strtotime($data->start_date)))
              ->whereDate('sls_sales_order.transaction_date',"<=",date("Y-m-d",strtotime($data->end_date)))
              ->whereDate('sls_sales_order_closing.closing_date',">=",date("Y-m-d",strtotime($data->period_start_date)))
              ->whereDate('sls_sales_order_closing.closing_date',"<=",date("Y-m-d",strtotime($data->period_end_date)))
              ->update(array(
                'sls_sales_order.closing_status' => 1,
                'sls_sales_order.closing_by' => $id,
                'sls_sales_order.confirm_status' => list_confirm_status()[5][0],
                'sls_sales_order.closing_date' => date("Y-m-d H:i")
              ));
        $so = \DB::table('sls_sales_order')
              ->join('ivt_delivery_return_detail','ivt_delivery_return_detail.sales_order_id','=', 'sls_sales_order.id')
              ->join('ivt_delivery_return','ivt_delivery_return.id','=', 'ivt_delivery_return_detail.delivery_return_id')
              ->where('sls_sales_order.company_id',$data->company_id)
              ->where(\DB::raw('IFNULL(sls_sales_order.closing_status,0)'),0)
              ->whereDate('sls_sales_order.transaction_date',">=",date("Y-m-d",strtotime($data->start_date)))
              ->whereDate('sls_sales_order.transaction_date',"<=",date("Y-m-d",strtotime($data->end_date)))
              ->whereDate('ivt_delivery_return.delivery_date',">=",date("Y-m-d",strtotime($data->period_start_date)))
              ->whereDate('ivt_delivery_return.delivery_date',"<=",date("Y-m-d",strtotime($data->period_end_date)))
              ->update(array(
                'sls_sales_order.closing_status' => 1,
                'sls_sales_order.closing_by' => $id,
                'sls_sales_order.confirm_status' => list_confirm_status()[4][0],
                'sls_sales_order.closing_date' => date("Y-m-d H:i")
              ));

        $vs = \DB::table('sls_product_visitor')
              ->join('sls_sales_order_closing_detail','sls_sales_order_closing_detail.sales_order_id','=', 'sls_product_visitor.id')
              ->join('sls_sales_order_closing','sls_sales_order_closing.id','=', 'sls_sales_order_closing_detail.sales_order_closing_id')
              ->where('sls_product_visitor.company_id',$data->company_id)
              ->where(\DB::raw('IFNULL(sls_product_visitor.closing_status,0)'),0)
              ->whereDate('sls_product_visitor.transaction_date',">=",date("Y-m-d",strtotime($data->start_date)))
              ->whereDate('sls_product_visitor.transaction_date',"<=",date("Y-m-d",strtotime($data->end_date)))
              ->whereDate('sls_sales_order_closing.closing_date',">=",date("Y-m-d",strtotime($data->period_start_date)))
              ->whereDate('sls_sales_order_closing.closing_date',"<=",date("Y-m-d",strtotime($data->period_end_date)))
              ->update(array(
                'sls_product_visitor.closing_status' => 1,
                'sls_product_visitor.closing_by' => $id,
                'sls_product_visitor.confirm_status' => list_confirm_status()[5][0],
                'sls_product_visitor.closing_date' => date("Y-m-d H:i")
              ));
        $vs = \DB::table('sls_product_visitor')
              ->join('ivt_delivery_return_detail','ivt_delivery_return_detail.sales_order_id','=', 'sls_product_visitor.id')
              ->join('ivt_delivery_return','ivt_delivery_return.id','=', 'ivt_delivery_return_detail.delivery_return_id')
              ->where('sls_product_visitor.company_id',$data->company_id)
              ->where(\DB::raw('IFNULL(sls_product_visitor.closing_status,0)'),0)
              ->whereDate('sls_product_visitor.transaction_date',">=",date("Y-m-d",strtotime($data->start_date)))
              ->whereDate('sls_product_visitor.transaction_date',"<=",date("Y-m-d",strtotime($data->end_date)))
              ->whereDate('ivt_delivery_return.delivery_date',">=",date("Y-m-d",strtotime($data->period_start_date)))
              ->whereDate('ivt_delivery_return.delivery_date',"<=",date("Y-m-d",strtotime($data->period_end_date)))
              ->update(array(
                'sls_product_visitor.closing_status' => 1,
                'sls_product_visitor.closing_by' => $id,
                'sls_product_visitor.confirm_status' => list_confirm_status()[4][0],
                'sls_product_visitor.closing_date' => date("Y-m-d H:i")
              ));

        $data->status = 1;
        $data->save();
        return redirect()->route('master.index',static::$auth)->with('notif_success', static::$module_alias.' '. $data->name .' has been update successfully!');
    }

    public static function delete($id) {
        self::init();
        $data = static::$resource::find(str_replace('%20', ' ', $id));
        if (!$data) return redirect()->route('master.index',static::$auth)->with('notif_danger', 'Data '. $id .' not found!');

        $vs = \DB::table('sls_sales_order')
              ->where('sls_sales_order.closing_by',$data->id)
              ->update(array(
                'closing_status' => 0,
                'closing_by' => null,
                'closing_date' => null
              ));
        $so = \DB::table('sls_product_visitor')
              ->where('sls_product_visitor.closing_by',$data->id)
              ->update(array(
                'closing_status' => 0,
                'closing_by' => null,
                'closing_date' => null
              ));

        $module = $data->delete();
        return redirect()->back()->with('notif_success', static::$module_alias.' '. $data->name .' has been deleted!');
    }

    public static function list($request) {
        self::init();
        if($request->detail){
          $id = $request->id;
          $data = static::$resource::find(str_replace('%20', ' ', $id));

          $result['closing'] = \DB::table('sls_sales_order_closing')
                      ->select('sls_sales_order.*'
                        ,'mst_customer.full_name as full_name'
                        ,'mst_customer.phone as phone'
                        ,'mst_gender.id as gender_id'
                        ,'mst_advertise.name as advertise_name'
                        ,'mst_company.name as company_name'
                        ,\DB::raw('IFNULL(mst_gender.name,"") as gender_name')
                        ,\DB::raw('IFNULL(mst_bank.name,"") as bank_name')
                        ,\DB::raw('IFNULL(mst_market.name,"") as market_name')
                        ,\DB::raw('IFNULL(mst_courier.name,"") as courier_name')
                        ,\DB::raw('IFNULL(mst_payment_type.name,"") as payment_type_name')
                        ,\DB::raw('IFNULL(SUM(sls_sales_order_detail.quantity),0) as quantity')
                        ,\DB::raw('IFNULL(SUM(sls_sales_order_detail.price),0) as price')
                        ,\DB::raw('IFNULL(SUM(sls_sales_order_detail.voucer),0) as voucer')
                        ,\DB::raw('IFNULL(SUM(sls_sales_order_detail.total_transaction),0) as total_transaction')
                        ,\DB::raw('IFNULL(mst_customer_address.address,"") as address')
                        ,\DB::raw('IFNULL(mst_customer_address.address_no,"") as address_no')
                        ,\DB::raw('IFNULL(mst_customer_address.rt,"") as rt')
                        ,\DB::raw('IFNULL(mst_customer_address.rw,"") as rw')
                        ,\DB::raw('IFNULL(mst_customer_address.village,"") as village')
                        ,\DB::raw('IFNULL(mst_customer_address.sub_district,"") as sub_district')
                        ,\DB::raw('IFNULL(mst_customer_address.benchmark,"") as benchmark')
                        ,\DB::raw('IFNULL(mst_customer_address.city_id,"") as city_id')
                        ,\DB::raw('IFNULL(mst_customer_address.district,"") as district')
                        ,\DB::raw('IFNULL(mst_customer_address.province_id,"") as province_id')
                        ,\DB::raw('IFNULL(mst_customer_address.postal_code,"") as postal_code')
                        ,\DB::raw('SUM(sls_sales_order_closing_detail.balance_price) as balance_price')
                      )
                      ->join('sls_sales_order_closing_detail','sls_sales_order_closing_detail.sales_order_closing_id','=','sls_sales_order_closing.id')
                      ->join('sls_sales_order','sls_sales_order.id','=', 'sls_sales_order_closing_detail.sales_order_id')
                      ->join('sls_sales_order_detail','sls_sales_order_detail.sales_order_id','=','sls_sales_order.id')
                      ->leftjoin('mst_bank','mst_bank.id','=', 'sls_sales_order.bank_id')
                      ->leftjoin('mst_market','mst_market.id','=', 'sls_sales_order.market_id')
                      ->leftjoin('mst_courier','mst_courier.id','=', 'sls_sales_order.courier_id')
                      ->leftjoin('mst_payment_type','mst_payment_type.id','=', 'sls_sales_order.payment_type_id')
                      ->leftjoin('mst_customer','mst_customer.id','=', 'sls_sales_order.customer_id')
                      ->leftjoin('mst_company','mst_company.id','=', 'sls_sales_order.company_id')
                      ->leftjoin('mst_customer_address','mst_customer_address.id','=', 'sls_sales_order.customer_address_id')
                      ->leftjoin('mst_advertise','mst_advertise.id','=', 'sls_sales_order.advertise_id')
                      ->leftjoin('mst_gender','mst_gender.id','=', 'mst_customer.gender_id')
                      ->where('sls_sales_order.company_id',$data->company_id)
                      ->where(function ($query)use($data) {
                        if($data->status){
                            $query->where('sls_sales_order.closing_by',$data->id);
                        }else{
                          $query->orWhereNull('sls_sales_order.closing_by');
                          $query->orWhere('sls_sales_order.closing_by','');
                        }
                      })
                      ->whereDate('sls_sales_order.transaction_date',">=",date("Y-m-d",strtotime($data->start_date)))
                      ->whereDate('sls_sales_order.transaction_date',"<=",date("Y-m-d",strtotime($data->end_date)))
                      ->whereDate('sls_sales_order_closing.closing_date',">=",date("Y-m-d",strtotime($data->period_start_date)))
                      ->whereDate('sls_sales_order_closing.closing_date',"<=",date("Y-m-d",strtotime($data->period_end_date)))
                      ->groupBy('sls_sales_order.id')
                      ->orderBy('sls_sales_order.transaction_date','ASC')
                      ->orderBy('sls_sales_order.courier_id','ASC')
                      ->orderBy('sls_sales_order.market_id','ASC')
                      ->get();
                      // ;dd(getSql($result['closing']));
          $result['return'] = \DB::table('ivt_delivery_return')
                      ->select('sls_sales_order.*'
                        ,'mst_customer.full_name as full_name'
                        ,'mst_customer.phone as phone'
                        ,'mst_gender.id as gender_id'
                        ,'mst_advertise.name as advertise_name'
                        ,'mst_company.name as company_name'
                        ,\DB::raw('IFNULL(mst_gender.name,"") as gender_name')
                        ,\DB::raw('IFNULL(mst_bank.name,"") as bank_name')
                        ,\DB::raw('IFNULL(mst_market.name,"") as market_name')
                        ,\DB::raw('IFNULL(mst_courier.name,"") as courier_name')
                        ,\DB::raw('IFNULL(mst_payment_type.name,"") as payment_type_name')
                        ,\DB::raw('IFNULL(SUM(sls_sales_order_detail.quantity),0) as quantity')
                        ,\DB::raw('IFNULL(SUM(sls_sales_order_detail.price),0) as price')
                        ,\DB::raw('IFNULL(SUM(sls_sales_order_detail.voucer),0) as voucer')
                        ,\DB::raw('IFNULL(SUM(sls_sales_order_detail.total_transaction),0) as total_transaction')
                        ,\DB::raw('IFNULL(mst_customer_address.address,"") as address')
                        ,\DB::raw('IFNULL(mst_customer_address.address_no,"") as address_no')
                        ,\DB::raw('IFNULL(mst_customer_address.rt,"") as rt')
                        ,\DB::raw('IFNULL(mst_customer_address.rw,"") as rw')
                        ,\DB::raw('IFNULL(mst_customer_address.village,"") as village')
                        ,\DB::raw('IFNULL(mst_customer_address.sub_district,"") as sub_district')
                        ,\DB::raw('IFNULL(mst_customer_address.benchmark,"") as benchmark')
                        ,\DB::raw('IFNULL(mst_customer_address.city_id,"") as city_id')
                        ,\DB::raw('IFNULL(mst_customer_address.district,"") as district')
                        ,\DB::raw('IFNULL(mst_customer_address.province_id,"") as province_id')
                        ,\DB::raw('IFNULL(mst_customer_address.postal_code,"") as postal_code')
                        ,\DB::raw('SUM(sls_sales_order_detail.price) as total_balance_price')
                      )
                      ->join('ivt_delivery_return_detail','ivt_delivery_return_detail.delivery_return_id','=','ivt_delivery_return.id')
                      ->join('sls_sales_order','sls_sales_order.id','=', 'ivt_delivery_return_detail.sales_order_id')
                      ->join('sls_sales_order_detail','sls_sales_order_detail.sales_order_id','=','sls_sales_order.id')
                      ->leftjoin('mst_bank','mst_bank.id','=', 'sls_sales_order.bank_id')
                      ->leftjoin('mst_market','mst_market.id','=', 'sls_sales_order.market_id')
                      ->leftjoin('mst_courier','mst_courier.id','=', 'sls_sales_order.courier_id')
                      ->leftjoin('mst_payment_type','mst_payment_type.id','=', 'sls_sales_order.payment_type_id')
                      ->leftjoin('mst_customer','mst_customer.id','=', 'sls_sales_order.customer_id')
                      ->leftjoin('mst_company','mst_company.id','=', 'sls_sales_order.company_id')
                      ->leftjoin('mst_customer_address','mst_customer_address.id','=', 'sls_sales_order.customer_address_id')
                      ->leftjoin('mst_advertise','mst_advertise.id','=', 'sls_sales_order.advertise_id')
                      ->leftjoin('mst_gender','mst_gender.id','=', 'mst_customer.gender_id')
                      ->where('sls_sales_order.company_id',$data->company_id)
                      ->where(function ($query)use($data) {
                        if($data->status){
                            $query->where('sls_sales_order.closing_by',$data->id);
                        }else{
                          $query->orWhereNull('sls_sales_order.closing_by');
                          $query->orWhere('sls_sales_order.closing_by','');
                        }
                      })
                      ->whereDate('sls_sales_order.transaction_date',">=",date("Y-m-d",strtotime($data->start_date)))
                      ->whereDate('sls_sales_order.transaction_date',"<=",date("Y-m-d",strtotime($data->end_date)))
                      ->whereDate('ivt_delivery_return.delivery_date',">=",date("Y-m-d",strtotime($data->period_start_date)))
                      ->whereDate('ivt_delivery_return.delivery_date',"<=",date("Y-m-d",strtotime($data->period_end_date)))
                      ->groupBy('sls_sales_order.id')
                      ->orderBy('sls_sales_order.transaction_date','ASC')
                      ->orderBy('sls_sales_order.courier_id','ASC')
                      ->orderBy('sls_sales_order.market_id','ASC')
                      ->get();
          return makeResponse(200, 'success', null, $result);
        }
        $result = static::$resource::withoutGlobalScopes()
                  ->select('mst_closing_period.*',\DB::raw('mst_company.name as company_name'))
                  ->join('mst_company','mst_company.id','mst_closing_period.company_id')
                  ->where(function ($query)use($request) {
                    if($request->company_id){
                        $query->where('mst_closing_period.company_id',$request->company_id);
                    }else{
                      $query->where('mst_closing_period.company_id',sess_user('company_id'));
                    }
                  });
        if($request->from_date != '' && $request->to_date != ''){
          $result->where('mst_closing_period.created_at' ,'>=' , $request->from_date);
          $result->where('mst_closing_period.created_at' ,'<=' , $request->to_date);
        }
        $result->orderBy('mst_closing_period.start_date','DESC');

        return DataTables::of($result)
          ->addIndexColumn()
          ->addColumn('period_start_date', function($module) {
              return date('d-m-Y',strtotime($module->period_start_date));
          })
          ->addColumn('period_end_date', function($module) {
              return date('d-m-Y',strtotime($module->period_end_date));
          })
          ->addColumn('start_date', function($module) {
              return date('d-m-Y',strtotime($module->start_date));
          })
          ->addColumn('end_date', function($module) {
              return date('d-m-Y',strtotime($module->end_date));
          })
          ->addColumn('active', function($module) {
            return '<div class="align-items-center bg-dark">
                          <a data-href="' . static::$delete.'/'.$module->id . '"  class="btn btn-transparent-warning font-weight-bold mr-2" title="Closing Period" data-toggle="modal" data-target="#confirm-modal">Process</a>
                    </div>';
          })
          ->addColumn('action', function($module) {
              $delete = '<a data-href="' . static::$delete.'/'.$module->id . '" class="btn btn-icon btn-light btn-hover-danger btn-sm" "data-toggle="tooltip" data-placement="top" title="Delete" data-toggle="modal" data-target="#confirm-delete-modal">
          							    <span class="svg-icon svg-icon-md svg-icon-danger">
          							        <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
          							            <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
          							                <rect x="0" y="0" width="24" height="24"/>
          							                <path d="M6,8 L6,20.5 C6,21.3284271 6.67157288,22 7.5,22 L16.5,22 C17.3284271,22 18,21.3284271 18,20.5 L18,8 L6,8 Z" fill="#000000" fill-rule="nonzero"/>
          							                <path d="M14,4.5 L14,4 C14,3.44771525 13.5522847,3 13,3 L11,3 C10.4477153,3 10,3.44771525 10,4 L10,4.5 L5.5,4.5 C5.22385763,4.5 5,4.72385763 5,5 L5,5.5 C5,5.77614237 5.22385763,6 5.5,6 L18.5,6 C18.7761424,6 19,5.77614237 19,5.5 L19,5 C19,4.72385763 18.7761424,4.5 18.5,4.5 L14,4.5 Z" fill="#000000" opacity="0.3"/>
          							            </g>
          							        </svg>
          							    </span>
          							</a>';
              return $delete;
          })
          ->rawColumns(['active', 'action'])
          ->make(true);
    }

     public static function execute($request, $data = null) {
        self::init();
        if (is_null($data)) {
            $data = static::$table;
            $data->author = sess_user('name');
            $data->created_by = sess_user('id');
            $data->created_at = currDate();
        }else{
            $data->updated_by = sess_user('id');
            $data->updated_at = currDate();

        }
        if ($request->company_id){
          $data->company_id = $request->company_id;
        }else{
          $data->company_id = sess_user('company_id');
        }
        if ($request->id) {
            $data->id = strtoupper($request->id);
        }else{
            $data->id = generadeCode("Master","closing-period",$data->company_id,"CMSRT", $numb=5);
        }
        if ($request->name){
          $data->name = $request->name;
        }
        if ($request->period_start_date){
          if(!preg_match("/^[0-9]{4}-(0[1-9]|1[0-2])-(0[1-9]|[1-2][0-9]|3[0-1])$/",$request->period_start_date)){
            $data->period_start_date = Carbon::createFromFormat('d-m-Y', $request->period_start_date)->format('Y-m-d');
          }else{
            $data->period_start_date = $request->start_date;
          }
        }
        if ($request->period_end_date){
          if(!preg_match("/^[0-9]{4}-(0[1-9]|1[0-2])-(0[1-9]|[1-2][0-9]|3[0-1])$/",$request->period_end_date)){
            $data->period_end_date = Carbon::createFromFormat('d-m-Y', $request->period_end_date)->format('Y-m-d');
          }else{
            $data->period_end_date = $request->end_date;
          }
        }
        if ($request->start_date){
          if(!preg_match("/^[0-9]{4}-(0[1-9]|1[0-2])-(0[1-9]|[1-2][0-9]|3[0-1])$/",$request->start_date)){
            $data->start_date = Carbon::createFromFormat('d-m-Y', $request->start_date)->format('Y-m-d');
          }else{
            $data->start_date = $request->start_date;
          }
        }
        if ($request->end_date){
          if(!preg_match("/^[0-9]{4}-(0[1-9]|1[0-2])-(0[1-9]|[1-2][0-9]|3[0-1])$/",$request->end_date)){
            $data->end_date = Carbon::createFromFormat('d-m-Y', $request->end_date)->format('Y-m-d');
          }else{
            $data->end_date = $request->end_date;
          }
        }
        if ($request->except('status')) {
            $data->status = to_bool($request->status);
        }
        $data->save();

        return $data;
    }

}
