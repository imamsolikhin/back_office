<?php

namespace App\Http\Controllers\Sales;

use DataTables;
use Illuminate\Http\Request;
use App\Http\Resources\Product\Visitor;
use App\Http\Resources\Product\Followup;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Carbon\Carbon;

class SalesCommissionController extends Controller {
    private static $module;
    private static $module_alias;
    private static $auth;
    private static $path;
    private static $data;
    private static $delete;
    private static $controller;
    private static $resource;
    private static $resource_detail;
    private static $table;
    private static $dashboard_date;
    private static $dashboard_startdate;
    private static $dashboard_enddate;

    public static function init()
    {
        static::$module = 'sales-commission';
        static::$module_alias = 'Sales Commission';
        static::$auth = 'sales-commission';
        static::$path = route('sales.index','sales-commission');
        static::$data = route('sales.list','sales-commission');
        static::$delete = route('sales.delete',['sales-commission','']);
        static::$controller = getControllerName("Sales", "sales-commission");
        static::$resource = getResourceName("Sales", "sales-order");
        static::$resource_detail = getResourceName("Sales", "sales-order-detail");
        static::$table = new static::$resource();
        static::$dashboard_date = date('d-m-Y 00:00').' <=> '.date('d-m-Y 24:00');
        static::$dashboard_startdate = date('Y-m-d 00:00');
        static::$dashboard_enddate = date('Y-m-d 23:59');
    }

    public static function index($request) {
      static::init();
      $data["dashboard_date"] = $request->dashboard_date ? $request->dashboard_date:static::$dashboard_date;
      $data["dashboard_startdate"] = $request->dashboard_startdate ? $request->dashboard_startdate:static::$dashboard_startdate;
      $data["dashboard_enddate"] = $request->dashboard_enddate ? $request->dashboard_enddate:static::$dashboard_enddate;
      $data["dashboard_datetime"] = date('m-d-Y H:i',strtotime($data["dashboard_startdate"]))." <=> ".date('m-d-Y H:i',strtotime($data["dashboard_enddate"]));
      $data['module'] = static::$module;
      $data['module_alias'] = static::$module_alias;
      $data['auth'] = static::$auth;
      $data['path_import'] = route('reports.import',['product-sales','xls']);
      $data['path_export'] = route('reports.download',['product-sales','xls']);
      $data['path'] = static::$path;
      $data['data'] = static::$data;
      return view('sales.sales-commission',$data);
    }

    public static function data($id) {
        static::init();
        $module = static::$resource::select(static::$table->getTable().'.*')->findOrFail($id);
        return makeResponse(200, 'success', null, $module);
    }

    public static function save($request) {
        static::init();
        $validator = static::$controller::validation($request);
        if ($validator->fails()) return redirect()->route('sales.index',static::$auth)->with('notif_danger', 'New '.static::$module_alias.' '. $request->full_name .' can not be save!');

        $module = static::$controller::execute($request);

        $m_customer = $request;
        $m_customer->id = $module->customer_id;
        getResourceName("Master", "Customer")::destroy($module->customer_id);
        getControllerName("Master", "Customer")::execute($m_customer);

        $m_customer_address = $request;
        $m_customer_address->id = $module->customer_address_id;
        getResourceName("Master", "CustomerAddress")::destroy($module->customer_address_id);
        getControllerName("Master", "CustomerAddress")::execute($m_customer_address);
        Followup::destroy($module->id);
        $followup = new Followup($module->getOriginal());
        $followup->save();
        if($module->status){
          Followup::destroy($module->id);
          Customer::destroy($module->id);
          $patient = new Customer($module->getOriginal());
          $patient->save();
        }
        return redirect()->route('sales.index',static::$auth)->with('notif_success', 'New '.static::$module_alias.' '. $request->full_name .' has been added successfully!');
    }

    public static function update($id, $request) {
        static::init();
        $validator = static::$controller::validation($request,'update');
        if ($validator->fails()) return redirect()->route('sales.index',static::$auth)->with('notif_danger', 'New '.static::$module_alias.' '. $request->full_name .' can not be udate!');

        $data = Visitor::find(str_replace('%20', ' ', $id));
        if (!$data) return redirect()->route('sales.index',static::$auth)->with('notif_danger', 'Data '. $id .' not found!');

        $module = static::$controller::execute($request,$data);

        $m_customer = $request;
        $m_customer->id = $module->customer_id;
        getResourceName("Master", "Customer")::destroy($module->customer_id);
        getControllerName("Master", "Customer")::execute($m_customer);

        $m_customer_address = $request;
        $m_customer_address->id = $module->customer_address_id;
        getResourceName("Master", "CustomerAddress")::destroy($module->customer_address_id);
        getControllerName("Master", "CustomerAddress")::execute($m_customer_address);
        Followup::destroy($module->id);
        $followup = new Followup($module->getOriginal());
        $followup->save();
        if($module->status){
          Followup::destroy($module->id);
          Customer::destroy($module->id);
          $patient = new Customer($module->getOriginal());
          $patient->save();
        }

        return redirect()->route('sales.index',static::$auth)->with('notif_success', ''.static::$module_alias.' '. $data->full_name .' has been update successfully!');
    }

    public static function delete($id) {
        static::init();
        $data = static::$resource::find(str_replace('%20', ' ', $id));
        if (!$data) return redirect()->route('sales.index',static::$auth)->with('notif_danger', 'Data '. $id .' not found!');

        $module = $data->delete();

        return redirect()->back()->with('notif_success', ''.static::$module_alias.' '. $data->full_name .' has been deleted!');
    }

    public static function validation($request, $type = null) {
         $rules = [
             'full_name' => 'required|max:250',
         ];
         return Validator::make($request->all(), $rules);
   }

    public static function list($request) {
        static::init();

        if($request->detail){
          $id = $request->id;
          $result = \DB::table('sls_sales_order')
                      ->select('sls_sales_order.*'
                        ,'mst_customer.full_name as full_name'
                        ,'mst_customer.phone as phone'
                        ,'mst_gender.id as gender_id'
                        ,'mst_advertise.name as advertise_name'
                        ,'mst_company.name as company_name'
                        ,\DB::raw('IFNULL(mst_gender.name,"") as gender_name')
                        ,\DB::raw('IFNULL(mst_bank.name,"") as bank_name')
                        ,\DB::raw('IFNULL(mst_market.name,"") as market_name')
                        ,\DB::raw('IFNULL(mst_courier.name,"") as courier_name')
                        ,\DB::raw('IFNULL(mst_payment_type.name,"") as payment_type_name')
                        ,\DB::raw('IFNULL(SUM(sls_sales_order_detail.quantity),0) as quantity')
                        ,\DB::raw('IFNULL(SUM(sls_sales_order_detail.price),0) as price')
                        ,\DB::raw('IFNULL(SUM(sls_sales_order_detail.voucer),0) as voucer')
                        ,\DB::raw('IFNULL(SUM(sls_sales_order_detail.total_transaction),0) as total_transaction')
                        ,\DB::raw('IFNULL(mst_customer_address.address,"") as address')
                        ,\DB::raw('IFNULL(mst_customer_address.address_no,"") as address_no')
                        ,\DB::raw('IFNULL(mst_customer_address.rt,"") as rt')
                        ,\DB::raw('IFNULL(mst_customer_address.rw,"") as rw')
                        ,\DB::raw('IFNULL(mst_customer_address.village,"") as village')
                        ,\DB::raw('IFNULL(mst_customer_address.sub_district,"") as sub_district')
                        ,\DB::raw('IFNULL(mst_customer_address.benchmark,"") as benchmark')
                        ,\DB::raw('IFNULL(mst_customer_address.city_id,"") as city_id')
                        ,\DB::raw('IFNULL(mst_customer_address.district,"") as district')
                        ,\DB::raw('IFNULL(mst_customer_address.province_id,"") as province_id')
                        ,\DB::raw('IFNULL(mst_customer_address.postal_code,"") as postal_code')
                        ,\DB::raw('SUM(sls_sales_order_detail.price) as balance_price')
                        ,\DB::raw('SUM(IF(IFNULL(sls_sales_order_detail.status,0) = 1 ,mst_company.commission_price,IFNULL(mst_commission_rate.commission_price,mst_company.commission_price))*sls_sales_order_detail.quantity) as sum_commission')
                      )
                      ->join('sls_sales_order_detail','sls_sales_order_detail.sales_order_id','=','sls_sales_order.id')
                      ->leftjoin('mst_commission_rate', function ($join) {
                          $join->on('mst_commission_rate.sales_id', '=', 'sls_sales_order.sales_id');
                          $join->on('sls_sales_order.transaction_date', '>=', 'mst_commission_rate.start_date');
                          $join->on('sls_sales_order.transaction_date', '<=', 'mst_commission_rate.end_date');
                      })
                      ->leftjoin('mst_bank','mst_bank.id','=', 'sls_sales_order.bank_id')
                      ->leftjoin('mst_market','mst_market.id','=', 'sls_sales_order.market_id')
                      ->leftjoin('mst_courier','mst_courier.id','=', 'sls_sales_order.courier_id')
                      ->leftjoin('mst_payment_type','mst_payment_type.id','=', 'sls_sales_order.payment_type_id')
                      ->leftjoin('mst_customer','mst_customer.id','=', 'sls_sales_order.customer_id')
                      ->leftjoin('mst_company','mst_company.id','=', 'sls_sales_order.company_id')
                      ->leftjoin('mst_customer_address','mst_customer_address.id','=', 'sls_sales_order.customer_address_id')
                      ->leftjoin('mst_advertise','mst_advertise.id','=', 'sls_sales_order.advertise_id')
                      ->leftjoin('mst_gender','mst_gender.id','=', 'mst_customer.gender_id')
                      ->where('sls_sales_order.company_id',$request->company_id)
                      ->where('sls_sales_order.sales_id',$request->sales_id)
                      ->where(function ($query)use($request) {
                        if($request->from_date != '' && $request->to_date != ''){
                          $query->where('sls_sales_order.transaction_date' ,'>=' , $request->from_date);
                          $query->where('sls_sales_order.transaction_date' ,'<=' , $request->to_date);
                        }
                      })
                      ->groupBy('sls_sales_order.id')
                      ->orderBy('sls_sales_order.transaction_date','ASC')
                      ->orderBy('sls_sales_order.courier_id','ASC')
                      ->orderBy('sls_sales_order.market_id','ASC')
                      ->get();
          return makeResponse(200, 'success', null, $result);
        }
        $table = new static::$resource();
        $advertise_id = \DB::raw('"Global" as advertise_name');
        if($request->advertise_id){
          $advertise_id = \DB::raw('IFNULL(mst_advertise.name,"") as advertise_name');
        }
        $transaction_status = \DB::raw('"Global" as confirm_status');
        if($request->transaction_status){
          $transaction_status = \DB::raw('confirm_status');
        }
        $result = \DB::table($table->getTable())
                  ->select($table->getTable().'.author'
                    ,$table->getTable().'.sales_id'
                    ,\DB::raw('IFNULL(SUM(sls_sales_order_detail.quantity),0) as sum_quantity')
                    ,\DB::raw('IFNULL(SUM((sls_sales_order_detail.quantity*sls_sales_order_detail.price)-sls_sales_order_detail.voucer),0) as sum_price')
                    ,\DB::raw('IFNULL(SUM('.$table->getTable().'.courier_cost+'.$table->getTable().'.insurance),0) as sum_courier')
                    ,\DB::raw('IFNULL(SUM('.$table->getTable().'.cost_handler),0) as sum_cost_handler')
                    ,\DB::raw('IFNULL(SUM('.$table->getTable().'.transaction),0) as sum_transaction')
                    ,$advertise_id
                    ,$transaction_status
                    ,\DB::raw('SUM(IF(IFNULL(sls_sales_order_detail.status,0) = 1 ,mst_company.commission_price,IFNULL(mst_commission_rate.commission_price,mst_company.commission_price))*sls_sales_order_detail.quantity) as sum_commission')
                  )
                  ->leftjoin('mst_commission_rate', function ($join) {
                      $join->on('mst_commission_rate.sales_id', '=', 'sls_sales_order.sales_id');
                      $join->on('sls_sales_order.transaction_date', '>=', 'mst_commission_rate.start_date');
                      $join->on('sls_sales_order.transaction_date', '<=', 'mst_commission_rate.end_date');
                  })
                  ->leftjoin('sls_sales_order_detail','sls_sales_order_detail.sales_order_id','=', $table->getTable().'.id')
                  ->join('mst_company','mst_company.id','=', $table->getTable().'.company_id')
                  ->join('mst_advertise','mst_advertise.id','=', $table->getTable().'.advertise_id')
                  ->where(function ($query)use($request,$table) {
                    if($request->company_id){
                        $query->where($table->getTable().'.company_id',$request->company_id);
                    }else{
                      $query->where($table->getTable().'.company_id',sess_user('company_id'));
                    }
                  });
        if($request->from_date != '' && $request->to_date != ''){
          $result->where($table->getTable().'.transaction_date' ,'>=' , $request->from_date);
          $result->where($table->getTable().'.transaction_date' ,'<=' , $request->to_date);
        }

        if($request->advertise_id){
          $result->where($table->getTable().'.advertise_id' , $request->advertise_id);
        }
        if($request->bank_id){
          $result->where($table->getTable().'.bank_id' , $request->bank_id);
        }
        if($request->transaction_status){
          $result->where($table->getTable().'.confirm_status' , $request->transaction_status);
        }
        if($request->courier_id){
          $result->where($table->getTable().'.courier_id' , $request->courier_id);
        }
        if($request->resi_status){
          if($request->resi_status == 'off'){
            $result->where($table->getTable().'.delivery_no', '=' , null);
          }else{
            $result->where($table->getTable().'.delivery_no', '!=' , null);
          }
        }

        $result->groupBy($table->getTable().'.sales_id');
        $result->orderBy($table->getTable().'.author','ASC');
        // dd(getSql($result));
        return DataTables::of($result)
          ->addIndexColumn()
          ->addColumn('sum_quantity', function($module) {
              return number_format($module->sum_quantity,0)." Botol";
          })
          ->addColumn('sum_commission', function($module) {
              return "Rp. ".number_format($module->sum_commission,2);
          })
          ->addColumn('sum_courier', function($module) {
              return "Rp. ".number_format($module->sum_courier,2);
          })
          ->addColumn('sum_cost_handler', function($module) {
              return "Rp. ".number_format($module->sum_cost_handler,2);
          })
          ->addColumn('sum_courier', function($module) {
              return "Rp. ".number_format($module->sum_courier,2);
          })
          ->addColumn('sum_price', function($module) {
              return "Rp. ".number_format($module->sum_price,2);
          })
          ->addColumn('sum_transaction', function($module) {
              return "Rp. ".number_format($module->sum_transaction,2);
          })
          ->rawColumns(['sum_commission','sum_courier','sum_cost_handler','sum_courier','sum_price','sum_transaction'])
          ->make(true);
    }

}
