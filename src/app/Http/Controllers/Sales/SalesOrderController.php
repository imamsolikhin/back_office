<?php

namespace App\Http\Controllers\Sales;

use DataTables;
use Illuminate\Http\Request;
use App\Http\Resources\Product\Visitor;
use App\Http\Resources\Product\Followup;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Carbon\Carbon;

class SalesOrderController extends Controller {
    private static $module;
    private static $module_alias;
    private static $auth;
    private static $path;
    private static $data;
    private static $delete;
    private static $controller;
    private static $resource;
    private static $resource_detail;
    private static $table;
    private static $dashboard_date;
    private static $dashboard_startdate;
    private static $dashboard_enddate;

    public static function init()
    {
        static::$module = 'sales-order';
        static::$module_alias = 'Sales Order';
        static::$auth = 'sales-order';
        static::$path = route('sales.index','sales-order');
        static::$data = route('sales.list','sales-order');
        static::$delete = route('sales.delete',['sales-order','']);
        static::$controller = getControllerName("Sales", "sales-order");
        static::$resource = getResourceName("Sales", "sales-order");
        static::$resource_detail = getResourceName("Sales", "sales-order-detail");
        static::$table = new static::$resource();
        static::$dashboard_date = date('d-m-Y 00:00').' <=> '.date('d-m-Y 24:00');
        static::$dashboard_startdate = date('Y-m-d 00:00');
        static::$dashboard_enddate = date('Y-m-d 23:59');
    }

    public static function index($request) {
      static::init();
      $data["dashboard_date"] = $request->dashboard_date ? $request->dashboard_date:static::$dashboard_date;
      $data["dashboard_startdate"] = $request->dashboard_startdate ? $request->dashboard_startdate:static::$dashboard_startdate;
      $data["dashboard_enddate"] = $request->dashboard_enddate ? $request->dashboard_enddate:static::$dashboard_enddate;
      $data["dashboard_datetime"] = date('m-d-Y H:i',strtotime($data["dashboard_startdate"]))." <=> ".date('m-d-Y H:i',strtotime($data["dashboard_enddate"]));
      $data['module'] = static::$module;
      $data['module_alias'] = static::$module_alias;
      $data['auth'] = static::$auth;
      $data['path_import'] = route('reports.download',['product-sales-closing','xls']);
      $data['path_export'] = route('reports.download',['product-sales','xls']);
      $data['path'] = static::$path;
      $data['data'] = static::$data;
      return view('sales.sales-order',$data);
    }

    public static function data($id) {
        static::init();
        $table = new static::$resource();
        $module = \DB::table($table->getTable())
                  ->select($table->getTable().'.*'
                    ,'mst_customer.full_name as full_name'
                    ,'mst_customer.phone as phone'
                    ,'mst_gender.id as gender_id'
                    ,'mst_advertise.name as advertise_name'
                    ,'mst_company.name as company_name'
                    ,\DB::raw('IFNULL(SUM(sls_sales_order_detail.quantity),0) as quantity')
                    ,\DB::raw('IFNULL(SUM(sls_sales_order_detail.price),0) as price')
                    ,\DB::raw('IFNULL(SUM(sls_sales_order_detail.voucer),0) as voucer')
                    ,\DB::raw('IFNULL(SUM(sls_sales_order_detail.total_transaction),0) as total_transaction')
                    ,\DB::raw('IFNULL(mst_gender.name,"") as gender_name')
                    ,\DB::raw('IFNULL(mst_bank.name,"") as bank_name')
                    ,\DB::raw('IFNULL(mst_market.name,"") as market_name')
                    ,\DB::raw('IFNULL(mst_courier.name,"") as courier_name')
                    ,\DB::raw('IFNULL(mst_payment_type.name,"") as payment_type_name')
                    ,\DB::raw('IFNULL(mst_customer_address.address,"") as address')
                    ,\DB::raw('IFNULL(mst_customer_address.address_no,"") as address_no')
                    ,\DB::raw('IFNULL(mst_customer_address.rt,"") as rt')
                    ,\DB::raw('IFNULL(mst_customer_address.rw,"") as rw')
                    ,\DB::raw('IFNULL(mst_customer_address.village,"") as village')
                    ,\DB::raw('IFNULL(mst_customer_address.sub_district,"") as sub_district')
                    ,\DB::raw('IFNULL(mst_customer_address.benchmark,"") as benchmark')
                    ,\DB::raw('IFNULL(mst_customer_address.city_id,"") as city_id')
                    ,\DB::raw('IFNULL(mst_customer_address.district,"") as district')
                    ,\DB::raw('IFNULL(mst_customer_address.province_id,"") as province_id')
                    ,\DB::raw('IFNULL(mst_customer_address.postal_code,"") as postal_code')
                  )
                  ->leftjoin('sls_sales_order_detail','sls_sales_order_detail.sales_order_id','=', $table->getTable().'.id')
                  ->leftjoin('mst_bank','mst_bank.id','=', $table->getTable().'.bank_id')
                  ->leftjoin('mst_market','mst_market.id','=', $table->getTable().'.market_id')
                  ->leftjoin('mst_courier','mst_courier.id','=', $table->getTable().'.courier_id')
                  ->leftjoin('mst_payment_type','mst_payment_type.id','=', $table->getTable().'.payment_type_id')
                  ->leftjoin('mst_customer','mst_customer.id','=', $table->getTable().'.customer_id')
                  ->leftjoin('mst_company','mst_company.id','=', $table->getTable().'.company_id')
                  ->leftjoin('mst_customer_address','mst_customer_address.id','=', $table->getTable().'.customer_address_id')
                  ->leftjoin('mst_advertise','mst_advertise.id','=', $table->getTable().'.advertise_id')
                  ->leftjoin('mst_gender','mst_gender.id','=', 'mst_customer.gender_id')
                  ->where($table->getTable().'.id',$id)
                  ->first();
        return makeResponse(200, 'success', null, $module);
    }


    public static function update($id, $request) {
        self::init();
        $data = static::$resource::find(str_replace('%20', ' ', $id));
        if ($data){
          $dt = \DB::table('sls_sales_order_detail')
                ->where('sls_sales_order_detail.sales_order_id',$data->id)
                ->update(array('status' => $request->status));
        }
        return makeResponse(200, 'success', null, $data);
    }

    public static function delete($id) {
        static::init();
        $data = static::$resource::find(str_replace('%20', ' ', $id));
        if (!$data) return redirect()->route('sales.index',static::$auth)->with('notif_danger', 'Data '. $id .' not found!');

        $visitor = Visitor::find(str_replace('%20', ' ', $id));
        if($visitor){
          $visitor->lock_status = 0;
          $visitor->status = 0;
          $visitor->updated_by = sess_user('id');
          $visitor->updated_at = currDate();
          $visitor->save();
        }

        $followup = Followup::find(str_replace('%20', ' ', $id));
        if($followup){
          $followup->lock_status = 0;
          $followup->status = 0;
          $followup->updated_by = sess_user('id');
          $followup->updated_at = currDate();
          $followup->save();
        }
        $module = $data->delete();
        $so_detail = static::$resource_detail::where('sales_order_id',$id)->delete();

        return redirect()->back()->with('notif_success', ''.static::$module_alias.' '. $data->full_name .' has been deleted!');
    }

    public static function cancel($id) {
        static::init();
        $data = static::$resource::find(str_replace('%20', ' ', $id));
        if (!$data) return redirect()->route('sales.index',static::$auth)->with('notif_danger', 'Data '. $id .' not found!');

        $module = Visitor::find(str_replace('%20', ' ', $id));
        if($module){
          $module->lock_status = 1;
          $module->status = 1;
          $module->confirm_status = list_confirm_status()[6][0];
          $module->confirm_by = sess_user('id');
          $module->confirm_date = date('Y-m-d H:i');
          $module->save();
        }
        $data->closing_status = 1;
        $data->status = 1;
        $data->confirm_status = list_confirm_status()[6][0];
        $data->confirm_by = sess_user('id');
        $data->confirm_date = date('Y-m-d H:i');
        $data->save();
        return redirect()->back()->with('notif_success', ''.static::$module_alias.' '. $data->full_name .' has been deleted!');
    }

    public static function list($request) {
        static::init();
        $table = new static::$resource();
        $result = \DB::table($table->getTable())
                  ->select($table->getTable().'.*'
                    ,'mst_customer.full_name as full_name'
                    ,'mst_customer.phone as phone'
                    ,'mst_gender.id as gender_id'
                    ,'mst_advertise.name as advertise_name'
                    ,'mst_company.name as company_name'
                    ,\DB::raw('IFNULL(mst_gender.name,"") as gender_name')
                    ,\DB::raw('IFNULL(mst_bank.name,"") as bank_name')
                    ,\DB::raw('IFNULL(mst_market.name,"") as market_name')
                    ,\DB::raw('IFNULL(mst_courier.name,"") as courier_name')
                    ,\DB::raw('IFNULL(mst_payment_type.name,"") as payment_type_name')
                    ,\DB::raw('IFNULL(SUM(sls_sales_order_detail.quantity),0) as quantity')
                    ,\DB::raw('IFNULL(SUM(sls_sales_order_detail.price),0) as price')
                    ,\DB::raw('IFNULL(SUM(sls_sales_order_detail.voucer),0) as voucer')
                    ,\DB::raw('IFNULL(SUM(sls_sales_order_detail.total_transaction),0) as total_transaction')
                    ,\DB::raw('IFNULL(mst_customer_address.address,"") as address')
                    ,\DB::raw('IFNULL(mst_customer_address.address_no,"") as address_no')
                    ,\DB::raw('IFNULL(mst_customer_address.rt,"") as rt')
                    ,\DB::raw('IFNULL(mst_customer_address.rw,"") as rw')
                    ,\DB::raw('IFNULL(mst_customer_address.village,"") as village')
                    ,\DB::raw('IFNULL(mst_customer_address.sub_district,"") as sub_district')
                    ,\DB::raw('IFNULL(mst_customer_address.benchmark,"") as benchmark')
                    ,\DB::raw('IFNULL(mst_customer_address.city_id,"") as city_id')
                    ,\DB::raw('IFNULL(mst_customer_address.district,"") as district')
                    ,\DB::raw('IFNULL(mst_customer_address.province_id,"") as province_id')
                    ,\DB::raw('IFNULL(mst_customer_address.postal_code,"") as postal_code')
                  )
                  ->leftjoin('sls_sales_order_detail','sls_sales_order_detail.sales_order_id','=', $table->getTable().'.id')
                  ->leftjoin('mst_bank','mst_bank.id','=', $table->getTable().'.bank_id')
                  ->leftjoin('mst_market','mst_market.id','=', $table->getTable().'.market_id')
                  ->leftjoin('mst_courier','mst_courier.id','=', $table->getTable().'.courier_id')
                  ->leftjoin('mst_payment_type','mst_payment_type.id','=', $table->getTable().'.payment_type_id')
                  ->leftjoin('mst_customer','mst_customer.id','=', $table->getTable().'.customer_id')
                  ->leftjoin('mst_company','mst_company.id','=', $table->getTable().'.company_id')
                  ->leftjoin('mst_customer_address','mst_customer_address.id','=', $table->getTable().'.customer_address_id')
                  ->leftjoin('mst_advertise','mst_advertise.id','=', $table->getTable().'.advertise_id')
                  ->leftjoin('mst_gender','mst_gender.id','=', 'mst_customer.gender_id')
                  ->where(function ($query)use($request,$table) {
                    if($request->user_id){
                      foreach ($request->user_id as $key => $rs) {
                        if($rs){
                          $query->orwhere('sls_sales_order.sales_id',$rs);
                        }
                      }
                    }
                    if($request->company_id){
                        $query->where($table->getTable().'.company_id',$request->company_id);
                    }else{
                      $query->where($table->getTable().'.company_id',sess_user('company_id'));
                    }
                  });

        if($request->from_date != '' && $request->to_date != ''){
          $result->where($table->getTable().'.transaction_date' ,'>=' , $request->from_date);
          $result->where($table->getTable().'.transaction_date' ,'<=' , $request->to_date);
        }

        if($request->advertise_id){
          $result->where($table->getTable().'.advertise_id' , $request->advertise_id);
        }
        if($request->payment_type_id){
          $result->where($table->getTable().'.payment_type_id' , $request->payment_type_id);
        }
        if($request->transaction_status){
          $result->where($table->getTable().'.confirm_status' , $request->transaction_status);
        }else{
          $result->where($table->getTable().'.confirm_status' , '!=','Cancel');
        }
        if($request->courier_id){
          $result->where($table->getTable().'.courier_id' , $request->courier_id);
        }
        if($request->resi_status){
          if($request->resi_status == 'off'){
            $result->where($table->getTable().'.delivery_no', '=' , null);
          }else{
            $result->where($table->getTable().'.delivery_no', '!=' , null);
          }
        }

        $result->groupBy($table->getTable().'.id');
        $result->orderBy($table->getTable().'.status','ASC');
        $result->orderBy($table->getTable().'.transaction_date','DESC');
        // dd(getSql($result));
        return DataTables::of($result)
          ->addIndexColumn()
          ->addColumn('full_name', function($module) {
              return $module->gender_name." ".$module->full_name;
          })
          ->addColumn('active', function($module) {
              $created =  "transaction: ".date('d-m-Y H:i',strtotime($module->transaction_date))."<br/>";
              $status =  '<span class="label font-weight-bold label-lg  label-light-warning label-inline">'.$module->confirm_status.'</span>';
              $newold = $module->status ? '<span class="label font-weight-bold label-lg  label-light-danger label-inline">Lama</span>' : '<span class="label font-weight-bold label-lg  label-light-info label-inline">Baru</span>';
              return '<center>'.$created.$newold."&nbsp".$status.'</center>';
          })
          ->addColumn('action', function($module) {
              $id = "'".$module->id."'";
              $btn ='<a onclick="show_data('.$id.')" class="btn btn-icon btn-light btn-hover-success btn-sm" data-placement="top" title="Delete" ><i class="la icon-md la-whatsapp"></i></a>';
              if($module->closing_status){
                return '<span class="label font-weight-bold label-lg  label-light-danger label-inline"><i class="fas fa-lock pr-2 text-warning "></i> Data Closed</span>';
              }else{
                if($module->confirm_status == "Packing" or $module->confirm_status == "Waiting" or $module->confirm_status == "Ready Delived"){
                  return $btn.' </br> <a data-href="' . route('sales.cancel',['sales-order',$module->id]) . '" class="btn btn-info btn-hover-warning btn-sm" data-placement="top" title="Cancel" data-toggle="modal" data-target="#confirm-modal">Cancel</a>';
                }
                if($module->confirm_status != "Packing" and $module->confirm_status != "Waiting"){
                  return $btn.'<span class="label font-weight-bold label-lg  label-light-danger label-inline"><i class="fas fa-lock pr-2 text-warning "></i> ('.$module->confirm_status.')</span>';
                }else{
                    return $btn.'
                           <a data-href="' . route('sales.delete',['sales-order',$module->id]) . '" class="btn btn-icon btn-light btn-hover-danger btn-sm" data-placement="top" title="Delete" data-toggle="modal" data-target="#confirm-delete-modal"><i class="fa flaticon-delete-1"></i></a>
                           <span class="label font-weight-bold label-lg  label-light-warning label-inline">'.$module->confirm_status.'</span>';
                }
              }
          })
          ->rawColumns(['active', 'action'])
          ->make(true);
    }

    public static function detail($request) {
        static::init();
        $table = new static::$resource();
        $result['header'] = \DB::table($table->getTable())
                  ->select($table->getTable().'.*'
                    ,'mst_customer.full_name as full_name'
                    ,'mst_customer.phone as phone'
                    ,'mst_voucher.name as voucher_name'
                    ,'mst_voucher.total as voucher_price'
                    ,'mst_advertise.name as advertise_name'
                    ,'mst_company.name as company_name'
                    ,\DB::raw('IFNULL(mst_gender.name,"") as gender_name')
                    ,\DB::raw('IFNULL(mst_bank.name,"") as bank_name')
                    ,\DB::raw('IFNULL(mst_market.name,"") as market_name')
                    ,\DB::raw('IFNULL(mst_courier.name,"") as courier_name')
                    ,\DB::raw('IFNULL(mst_payment_type.name,"") as payment_type_name')
                    ,\DB::raw('IFNULL(mst_customer_address.address,"") as address')
                    ,\DB::raw('IFNULL(mst_customer_address.address_no,"") as address_no')
                    ,\DB::raw('IFNULL(mst_customer_address.rt,"") as rt')
                    ,\DB::raw('IFNULL(mst_customer_address.rw,"") as rw')
                    ,\DB::raw('IFNULL(mst_customer_address.village,"") as village')
                    ,\DB::raw('IFNULL(mst_customer_address.sub_district,"") as sub_district')
                    ,\DB::raw('IFNULL(mst_customer_address.benchmark,"") as benchmark')
                    ,\DB::raw('IFNULL(mst_customer_address.city_id,"") as city_id')
                    ,\DB::raw('IFNULL(mst_customer_address.district,"") as district')
                    ,\DB::raw('IFNULL(mst_customer_address.province_id,"") as province_id')
                    ,\DB::raw('IFNULL(mst_customer_address.postal_code,"") as postal_code')
                  )
                  ->leftjoin('mst_voucher','mst_voucher.id','=', $table->getTable().'.voucher_id')
                  ->leftjoin('mst_bank','mst_bank.id','=', $table->getTable().'.bank_id')
                  ->leftjoin('mst_market','mst_market.id','=', $table->getTable().'.market_id')
                  ->leftjoin('mst_courier','mst_courier.id','=', $table->getTable().'.courier_id')
                  ->leftjoin('mst_payment_type','mst_payment_type.id','=', $table->getTable().'.payment_type_id')
                  ->join('mst_customer','mst_customer.id','=', $table->getTable().'.customer_id')
                  ->join('mst_gender','mst_gender.id','=', 'mst_customer.gender_id')
                  ->leftjoin('mst_customer_address','mst_customer_address.id','=', $table->getTable().'.customer_address_id')
                  ->join('mst_company','mst_company.id','=', $table->getTable().'.company_id')
                  ->join('mst_advertise','mst_advertise.id','=', 'mst_customer.advertise_id')
                  ->where($table->getTable().'.id',$request->sales_order_id)
                  ->first();

        $table = new static::$resource_detail();
        $result['detail'] = \DB::table($table->getTable())
                  ->select($table->getTable().'.*'
                  ,'mst_company.name as company_name'
                  ,'mst_item.name as item_name'
                  )
                  ->leftjoin('mst_item','mst_item.id','=', $table->getTable().'.item_id')
                  ->join('mst_company','mst_company.id','=', $table->getTable().'.company_id')
                  ->where($table->getTable().'.sales_order_id',$request->sales_order_id)
                  ->orderBy($table->getTable().'.id','ASC')
                  ->get();
        return makeResponse(200, 'success', null, $result);
    }


}
