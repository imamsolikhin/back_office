<?php

namespace App\Http\Controllers\Sales;

use DataTables;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Carbon\Carbon;

class SalesOrderClosingController extends Controller {

    private static $module;
    private static $module_alias;
    private static $auth;
    private static $path;
    private static $path_detail;
    private static $data;
    private static $delete;
    private static $controller;
    private static $resource;
    private static $resource_detail;
    private static $resource_so;
    private static $resource_so_detail;
    private static $table;
    private static $dashboard_date;
    private static $dashboard_startdate;
    private static $dashboard_enddate;

    public static function init()
    {
        static::$module = 'sales-order-closing';
        static::$module_alias = 'SO Closing';
        static::$auth = 'sales-order-closing';
        static::$path = route('sales.index','sales-order-closing');
        static::$path_detail = route('sales.index','sales-order-closing-detail');
        static::$data = route('sales.list','sales-order-closing');
        static::$delete = route('sales.delete',['sales-order-closing','']);
        static::$controller = getControllerName("Sales", "sales-order-closing");
        static::$resource = getResourceName("Sales", "sales-order-closing");
        static::$resource_detail = getResourceName("Sales", "sales-order-closing-detail");
        static::$resource_so = getResourceName("Sales", "sales-order");
        static::$resource_so_detail = getResourceName("Sales", "sales-order-detail");
        static::$table = new static::$resource();
        static::$dashboard_date = date('d-m-Y 00:00').' <=> '.date('d-m-Y 24:00');
        static::$dashboard_startdate = date('Y-m-d 00:00');
        static::$dashboard_enddate = date('Y-m-d 23:59');
    }

    public static function index($request) {
      static::init();
      $data["dashboard_date"] = $request->dashboard_date ? $request->dashboard_date:static::$dashboard_date;
      $data["dashboard_startdate"] = $request->dashboard_startdate ? $request->dashboard_startdate:static::$dashboard_startdate;
      $data["dashboard_enddate"] = $request->dashboard_enddate ? $request->dashboard_enddate:static::$dashboard_enddate;
      $data["dashboard_datetime"] = date('m-d-Y H:i',strtotime($data["dashboard_startdate"]))." <=> ".date('m-d-Y H:i',strtotime($data["dashboard_enddate"]));
      $data['module'] = static::$module;
      $data['module_alias'] = static::$module_alias;
      $data['auth'] = static::$auth;
      $data['path'] = static::$path;
      $data['path_detail'] = static::$path_detail;
      $data['data'] = static::$data;
      return view('sales.sales-order-closing',$data);
    }

    public static function data($id) {
        self::init();
        $module = static::$resource::withoutGlobalScopes(['active'])->findOrFail($id);
        return makeResponse(200, 'success', null, $module);
    }

    public static function save($request) {
        self::init();
        $module = static::$controller::execute($request);
        return redirect()->route('sales.index',static::$auth)->with('notif_success', 'New '.static::$module_alias.' '. $request->name .' has been added successfully!');
    }

    public static function update($id, $request) {
        self::init();
        $data = static::$resource::find(str_replace('%20', ' ', $id));
        if (!$data) return redirect()->route('sales.index',static::$auth)->with('notif_danger', 'Data '. $id .' not found!');

        $module = static::$controller::execute($request,$data);
        return redirect()->route('sales.index',static::$auth)->with('notif_success', static::$module_alias.' '. $data->name .' has been update successfully!');
    }

    public static function delete($id) {
        self::init();
        $data = static::$resource::find(str_replace('%20', ' ', $id));
        if (!$data) return redirect()->route('sales.index',static::$auth)->with('notif_danger', 'Data '. $id .' not found!');

        $module = $data->delete();
        return redirect()->back()->with('notif_success', static::$module_alias.' '. $data->name .' has been deleted!');
    }

    public static function list($request) {
        self::init();
        $table = new static::$resource();
        $result = \DB::table($table->getTable())
                  ->select($table->getTable().'.*'
                  ,'mst_company.name as company_name'
                  ,'mst_bank.name as bank_name'
                  ,\DB::raw('COUNT(sls_sales_order_detail.id) as total_packet')
                  ,\DB::raw('SUM(sls_sales_order_detail.quantity) as total_quantity')
                  ,\DB::raw('SUM(sls_sales_order_closing_detail.balance_price) as total_balance_price')
                  )
                  ->join('mst_company','mst_company.id','=', $table->getTable().'.company_id')
                  ->join('mst_bank','mst_bank.id','=', $table->getTable().'.bank_id')
                  ->leftjoin('sls_sales_order_closing_detail','sls_sales_order_closing_detail.sales_order_closing_id','=', $table->getTable().'.id')
                  ->leftjoin('sls_sales_order_detail','sls_sales_order_detail.sales_order_id','=', 'sls_sales_order_closing_detail.sales_order_id')
                  ->where(function ($query)use($request,$table) {
                    if($request->company_id){
                        $query->where($table->getTable().'.company_id',$request->company_id);
                    }else{
                      $query->where($table->getTable().'.company_id',sess_user('company_id'));
                    }
                    if($request->from_date != '' && $request->to_date != ''){
                      $query->where($table->getTable().'.closing_date' ,'>=' , $request->from_date);
                      $query->where($table->getTable().'.closing_date' ,'<=' , $request->to_date);
                    }
                  })
                  ->groupBy($table->getTable().'.id')
                  ->orderBy($table->getTable().'.closing_date','DESC')
                  ->get();

        return DataTables::of($result)
          ->addIndexColumn()
          ->addColumn('grand_total', function($module) {
              return "Rp. ".number_format($module->grand_total,2);
          })
          ->addColumn('total_packet', function($module) {
              return number_format($module->total_packet).' SO';
          })
          ->addColumn('total_quantity', function($module) {
              return number_format($module->total_quantity).' Botol';
          })
          ->addColumn('total_balance_price', function($module) {
              return 'Rp. '.number_format($module->total_balance_price);
          })
          ->addColumn('closing_date', function($module) {
              return date('d-m-Y H:i',strtotime($module->closing_date));
          })
          ->addColumn('action', function($module) {
              $data_id ="'".$module->id."'";
              $data_date = "'".date('d-m-Y H:i',strtotime($module->closing_date))."'";
              $process = '<div class="align-items-center bg-dark">
                            <a onclick="show_so('.$data_id.')" class="btn btn-transparent-warning font-weight-bold mr-2" title="Check Valid SO" >Packet</a>
                            &nbsp;
                            <a onclick="export_copy('.$data_id.','.$data_date.')" class="btn btn-transparent-warning font-weight-bold mr-2" title="WA Laporan" >Laporan</a>
                            &nbsp;
                            <a onclick="show_data('.$data_id.')" class="btn btn-transparent-warning font-weight-bold mr-2" title="Edit" >Edit</a>
                        </div>';
              // $delete = '<a data-href="' . static::$delete.'/'.$module->id . '" class="btn btn-icon btn-light btn-hover-danger btn-sm" "data-toggle="tooltip" data-placement="top" title="Delete" data-toggle="modal" data-target="#confirm-delete-modal">
          		// 					    <span class="svg-icon svg-icon-md svg-icon-danger">
          		// 					        <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
          		// 					            <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
          		// 					                <rect x="0" y="0" width="24" height="24"/>
          		// 					                <path d="M6,8 L6,20.5 C6,21.3284271 6.67157288,22 7.5,22 L16.5,22 C17.3284271,22 18,21.3284271 18,20.5 L18,8 L6,8 Z" fill="#000000" fill-rule="nonzero"/>
          		// 					                <path d="M14,4.5 L14,4 C14,3.44771525 13.5522847,3 13,3 L11,3 C10.4477153,3 10,3.44771525 10,4 L10,4.5 L5.5,4.5 C5.22385763,4.5 5,4.72385763 5,5 L5,5.5 C5,5.77614237 5.22385763,6 5.5,6 L18.5,6 C18.7761424,6 19,5.77614237 19,5.5 L19,5 C19,4.72385763 18.7761424,4.5 18.5,4.5 L14,4.5 Z" fill="#000000" opacity="0.3"/>
          		// 					            </g>
          		// 					        </svg>
          		// 					    </span>
          		// 					</a>';
              // if(sess_user('role_id') == 'MNG' OR sess_user('role_id') == 'DEV'){
              //     return $process . ' ' . $delete;
              // }else{
                  return $process;
              // }
          })
          ->rawColumns(['total_packet','total_quantity','closing_date','action'])
          ->make(true);
    }

     public static function execute($request, $data = null) {
        self::init();
        if (is_null($data)) {
            $data = static::$table;
            $data->author = sess_user('name');
            $data->created_by = sess_user('id');
            $data->created_at = currDate();
        }else{
            $data->updated_by = sess_user('id');
            $data->updated_at = currDate();

        }
        if ($request->company_id){
          $data->company_id = $request->company_id;
        }else{
          $data->company_id = sess_user('company_id');
        }
        if ($request->id) {
            $data->id = strtoupper($request->id);
        }else{
            $data->id = generadeCode("Sales","sales-order-closing",$data->company_id,date('ymd'), $numb=3);
        }
        if ($request->bank_id){
          $data->bank_id = $request->bank_id;
        }
        if ($request->grand_total){
          $data->grand_total = $request->grand_total;
        }
        if ($request->closing_refno){
          $data->closing_refno = $request->closing_refno;
        }
        if ($request->closing_remark){
          $data->closing_remark = $request->closing_remark;
        }
        if ($request->closing_date){
          if(!preg_match("/^[0-9]{4}-(0[1-9]|1[0-2])-(0[1-9]|[1-2][0-9]|3[0-1])$/",$request->closing_date)){
            $data->closing_date = Carbon::createFromFormat('d-m-Y H:i', $request->closing_date)->format('Y-m-d H:i');
          }else{
            $data->closing_date = $request->closing_date;
          }
        }
        $data->save();

        return $data;
    }

}
