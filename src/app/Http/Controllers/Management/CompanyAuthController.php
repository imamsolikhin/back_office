<?php

namespace App\Http\Controllers\Management;

use DataTables;
use Illuminate\Http\Request;
use App\Models\CompanyAuth;
use App\Http\Resources\Master\Company;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;

class CompanyAuthController extends Controller {

    public function update($id, $sts, Request $request) {
        // $module = $this->save($request);
        $data = CompanyAuth::find(str_replace('%20', ' ', $id));
        if(!$data){
          return makeResponse(200, 'error', 'fails', 'company not found!');
        }
        $data["status"] = $sts;
        $module = $data->save();
        if(!$sts){
            return makeResponse(200, 'success', 'notif_success', 'Update Company Auth '. $data->company_child .' has been disable join!');
        }
        return makeResponse(200, 'success', 'notif_success', 'Update Company Auth '. $data->company_child .' has been success join!');
    }

    public function getData($com_id, Request $request) {
         $select = Company::select(array(\DB::raw('CONCAT("'.$com_id.'","-",mst_company.id) AS id'),\DB::raw('"'.$com_id.'" AS company_id'),'mst_company.id AS company_child'))
                       ->leftJoin('company_auth', function($leftJoin)use($com_id){
                            $leftJoin->on('mst_company.id', '=', 'company_auth.company_child');
                            $leftJoin->where('company_auth.company_id',$com_id);
                        })
                        ->where('company_auth.company_id',null);
         $bindings = $select->getBindings();
         // dd(getSql($select));
         $insertQuery = 'INSERT into company_auth (company_auth.id,company_auth.company_id,company_auth.company_child) '
                        .$select->toSql();
         \DB::insert($insertQuery, $bindings);

      	$datas = CompanyAuth::select(
                        'company_auth.id',
                        'company_auth.company_id',
                        'company_auth.company_child',
                        'company_auth.status',
                        'mst_company.name as company_name'
                      )
                       ->leftJoin('mst_company', function($join){
                            $join->on('mst_company.id', '=', 'company_auth.company_child');
                        })
                       ->where('mst_company.id',"!=",'DEV')
                       ->where('company_auth.company_id',$com_id)
                       ->orderby('company_auth.company_child','ASC')
                       ->get();
        return makeResponse(200, 'success', null, $datas);
     }

     public function save($request, $data = null) {
        if (is_null($data)) {
            $data = new CompanyAuth;
        }

        if ($request->id) {
            $data->id = $request->id;
        }
        if ($request->company_id) {
            $data->company_id = $request->company_id;
        }
        if ($request->company_child) {
            $data->company_child = $request->company_child;
        }
        $data->save();

        return $data;
    }

}
