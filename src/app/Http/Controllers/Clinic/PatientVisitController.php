<?php

namespace App\Http\Controllers\Clinic;

use DataTables;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Carbon\Carbon;

class PatientVisitController extends Controller {

    private static $module;
    private static $module_alias;
    private static $auth;
    private static $path;
    private static $data;
    private static $delete;
    private static $controller;
    private static $resource;
    private static $table;

    public static function init()
    {
        static::$module = 'Kunjungan Pasien';
        static::$module_alias = 'Kunjungan Pasien';
        static::$auth = 'patient-visit';
        static::$path = route('clinic.index','patient-visit');
        static::$data = route('clinic.list','patient-visit');
        static::$delete = route('clinic.delete',['patient-visit','']);
        static::$controller = getControllerName("Clinic", "patient-visit");
        static::$resource = getResourceName("Clinic", "patient-visit");
        static::$table = new static::$resource();
    }

    public static function validation($request, $type = null) {
        $rules = [
           'name' => 'required|max:250',
        ];
        // if (is_null($type)) {
        //     $rules = array_merge($rules, ['id' => 'required|max:250|unique:'.static::$table->getTable().',id']);
        // }
        return Validator::make($request->all(), $rules);
    }

    public static function index() {
      self::init();
      $data['module'] = static::$module;
      $data['auth'] = static::$auth;
      $data['path'] = static::$path;

      $resv = \DB::table('sls_clinic_reservation')->select('id','full_name as name')
                  ->where('lock_status','=',0)
                  ->where('schedule_date','>=',date('Y-m-d'));
      $data['reservasi'] = \DB::table('mst_patient')->select('id','name')
                  ->where('mst_patient.company_id',sess_user('company_id'))
                  ->union($resv)
                  ->get();
      return view('clinic.patient-visit',$data);
    }

    public static function data($id) {
        self::init();
        $resource = \DB::table('sls_clinic_reservation')->select('sls_clinic_reservation.id'
                                ,'sls_clinic_reservation.full_name AS name'
                                ,\DB::raw('null as img_url')
                                ,'mst_gender.name as gender_name'
                                ,\DB::raw('null as birthdate')
                                ,'sls_clinic_reservation.phone'
                                ,\DB::raw('"" as email')
                                ,'sls_clinic_reservation.consultation'
                                ,'sls_clinic_reservation.schedule_date as consultation_date'
                                ,'sls_clinic_reservation.city_id'
                                ,'sls_clinic_reservation.address'
                                ,'sls_clinic_reservation.sales_id'
                                ,\DB::raw('"Baru" as register_status')
                                ,\DB::raw('false as is_consultation')
                                ,\DB::raw('false as is_order')
                                ,\DB::raw('false as is_therapy')
                                ,\DB::raw('users.name as sales_name')
                                ,\DB::raw('"" as note'))
                                ->join('mst_gender','mst_gender.id','=', 'sls_clinic_reservation.gender_id')
                                ->join("users","users.id","=","sls_clinic_reservation.sales_id")
                                ->leftjoin("mst_patient_visit","mst_patient_visit.id","=","sls_clinic_reservation.id")
                                ->where('sls_clinic_reservation.id',$id)
                                ->where('sls_clinic_reservation.lock_status','=',0)
                                ->where('mst_patient_visit.id','=',null)
                                ->where('sls_clinic_reservation.schedule_date','>=',date('Y-m-d'));
        $module = \DB::table('mst_patient_visit')->select('mst_patient_visit.id'
                                ,'mst_patient_visit.name'
                                ,'img_url'
                                ,'mst_gender.name as gender_name'
                                ,'birthdate'
                                ,'mst_patient_visit.phone'
                                ,\DB::raw('ifnull(mst_patient_visit.email,"") as email')
                                ,'consultation'
                                ,'consultation_date'
                                ,'city_id'
                                ,'address'
                                ,'sales_id'
                                ,'register_status'
                                ,'is_consultation'
                                ,'is_order'
                                ,'is_therapy'
                                ,'users.name as sales_name'
                                ,'note')
                                ->join('mst_gender','mst_gender.id','=', 'mst_patient_visit.gender_id')
                                ->leftjoin("users","users.id","=","mst_patient_visit.sales_id")
                                ->where('mst_patient_visit.company_id',sess_user('company_id'))
                                ->where('mst_patient_visit.id',$id)
                    ->union($resource)
                    ->first();
                    // dd($module);
        return makeResponse(200, 'success', null, $module);
    }

    public static function save($request) {
        self::init();
        // $validator = static::$controller::validation($request);
        // dd($request);
        // if ($validator->fails()) return redirect()->route('clinic.index',static::$auth)->with('notif_danger', 'New '.static::$module_alias.' '. $request->name .' can not be save!');

        $data = static::$resource::find(str_replace('%20', ' ', $request->id));
        if ($data) {
          $module = static::$controller::execute($request,$data);
        }else{
          $module = static::$controller::execute($request);
        }

        if($request->img_url){
          $file = $request->file('img_url');
          $name_file = $module->id.".".$file->getClientOriginalExtension();
          $path_upload = 'upload/'.sess_user("company_id").'//clinic-visit//'.date('Y/m-d',time());
          $file->move($path_upload,$name_file);
          $module->img_url = $path_upload."/".$name_file;
          $module = static::$controller::execute($request,$module);
        }

        if($module->register_status == "Baru"){
          getResourceName("Clinic", "Patient")::destroy($module->id);
          $patient = getResourceName("Clinic", "Patient");
          $patient = new $patient();
          $module->brithdate = date('d-m-Y',strtotime($module->brithdate));
          $patient = getControllerName("Clinic", "Patient")::execute($module,$patient);
        }
        return redirect()->route('clinic.index',static::$auth)->with('notif_success', 'New '.static::$module_alias.' '. $request->name .' has been added successfully!');
    }

    public static function update($id, $request) {
        self::init();
        $validator = static::$controller::validation($request,'update');
        if ($validator->fails()) return redirect()->route('clinic.index',static::$auth)->with('notif_danger', 'Update '.static::$module_alias.' '. $request->name .' can not be udate!');

        $data = static::$resource::find(str_replace('%20', ' ', $id));
        if (!$data) return redirect()->route('clinic.index',static::$auth)->with('notif_danger', 'Data '. $id .' not found!');

        $module = static::$controller::execute($request,$data);
        if($request->img_url){
          $file = $request->file('img_url');
          $name_file = $module->id.".".$file->getClientOriginalExtension();
          $path_upload = 'upload/'.sess_user("company_id").'//clinic-visit//'.date('Y/m-d',time());
          $file->move($path_upload,$name_file);
          $data->img_url = $path_upload."/".$name_file;
          $module = static::$controller::execute($request,$data);
        }

        if(in_array($module->register_status, array('Baru','Lolos'))){
          getResourceName("Clinic", "Patient")::destroy($module->id);
        }

        if($module->register_status == "Baru"){
          $patient = getResourceName("Clinic", "Patient");
          $patient = new $patient();
          $patient = getControllerName("Clinic", "Patient")::execute($module,$patient);
        }
        return redirect()->route('clinic.index',static::$auth)->with('notif_success', static::$module_alias.' '. $data->name .' has been update successfully!');
    }

    public static function delete($id) {
        self::init();
        $data = static::$resource::find(str_replace('%20', ' ', $id));
        if (!$data) return redirect()->route('clinic.index',static::$auth)->with('notif_danger', 'Data '. $id .' not found!');

        $module = $data->delete();
        return redirect()->back()->with('notif_success', static::$module_alias.' '. $data->name .' has been deleted!');
    }

    public static function list($request) {
        self::init();
        $table = new static::$resource();
        $result = \DB::table($table->getTable())
                  ->select($table->getTable().'.*', 'mst_gender.name as gender_name')
                  ->join('mst_gender','mst_gender.id','=', $table->getTable().'.gender_id')
                  ->where($table->getTable().'.company_id',sess_user('company_id'));

        if($request->from_date != '' && $request->to_date  != ''){
          $result->where($table->getTable().'.created_at' ,'>=' , $request->from_date);
          $result->where($table->getTable().'.created_at' ,'<=' , $request->to_date);
        }

        return DataTables::of($result)
          ->addIndexColumn()
          ->addColumn('name', function($module) {
              return $module->gender_name.' '.$module->name;
          })
          ->addColumn('reservation_desc', function($module) {
              $id = "'".$module->id."'";
              $img = '<div class="symbol-label"><img title="'.$module->name.'" id="'.$module->id.'" alt="img" src="'.asset($module->img_url).'" height="30" width="50"/>';
              return $img.'<center style="font-weight:900">'.date('d-m-Y H:i',strtotime($module->consultation_date)).'<br>'.'<button onClick="show_laporan('.$id.')" class="btn btn-primary btn-sm m-1 p-1">Laporan</button><a href="'.asset($module->img_url).'" target="blank" class="btn btn-info btn-sm m-1 p-1">show image</a></center></div>' ;
          })
          ->addColumn('action', function($module) {
              $data_id ="'".$module->id."'";
              $edit = '<a href="#edithost" onclick="show_data(' .$data_id. ')" class="btn btn-icon btn-light btn-hover-primary btn-sm" data-toggle="tooltip" data-placement="top" title="Edit">
          							    <span class="svg-icon svg-icon-md svg-icon-primary">
          							        <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
          							            <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
          							                <rect x="0" y="0" width="24" height="24"/>
          							                <path d="M12.2674799,18.2323597 L12.0084872,5.45852451 C12.0004303,5.06114792 12.1504154,4.6768183 12.4255037,4.38993949 L15.0030167,1.70195304 L17.5910752,4.40093695 C17.8599071,4.6812911 18.0095067,5.05499603 18.0083938,5.44341307 L17.9718262,18.2062508 C17.9694575,19.0329966 17.2985816,19.701953 16.4718324,19.701953 L13.7671717,19.701953 C12.9505952,19.701953 12.2840328,19.0487684 12.2674799,18.2323597 Z" fill="#000000" fill-rule="nonzero" transform="translate(14.701953, 10.701953) rotate(-135.000000) translate(-14.701953, -10.701953) "/>
          							                <path d="M12.9,2 C13.4522847,2 13.9,2.44771525 13.9,3 C13.9,3.55228475 13.4522847,4 12.9,4 L6,4 C4.8954305,4 4,4.8954305 4,6 L4,18 C4,19.1045695 4.8954305,20 6,20 L18,20 C19.1045695,20 20,19.1045695 20,18 L20,13 C20,12.4477153 20.4477153,12 21,12 C21.5522847,12 22,12.4477153 22,13 L22,18 C22,20.209139 20.209139,22 18,22 L6,22 C3.790861,22 2,20.209139 2,18 L2,6 C2,3.790861 3.790861,2 6,2 L12.9,2 Z" fill="#000000" fill-rule="nonzero" opacity="0.3"/>
          							            </g>
          							        </svg>
          							    </span>
          							</a>';
              $delete = '<a data-href="' . static::$delete.'/'.$module->id . '" class="btn btn-icon btn-light btn-hover-danger btn-sm" "data-toggle="tooltip" data-placement="top" title="Delete" data-toggle="modal" data-target="#confirm-delete-modal">
          							    <span class="svg-icon svg-icon-md svg-icon-danger">
          							        <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
          							            <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
          							                <rect x="0" y="0" width="24" height="24"/>
          							                <path d="M6,8 L6,20.5 C6,21.3284271 6.67157288,22 7.5,22 L16.5,22 C17.3284271,22 18,21.3284271 18,20.5 L18,8 L6,8 Z" fill="#000000" fill-rule="nonzero"/>
          							                <path d="M14,4.5 L14,4 C14,3.44771525 13.5522847,3 13,3 L11,3 C10.4477153,3 10,3.44771525 10,4 L10,4.5 L5.5,4.5 C5.22385763,4.5 5,4.72385763 5,5 L5,5.5 C5,5.77614237 5.22385763,6 5.5,6 L18.5,6 C18.7761424,6 19,5.77614237 19,5.5 L19,5 C19,4.72385763 18.7761424,4.5 18.5,4.5 L14,4.5 Z" fill="#000000" opacity="0.3"/>
          							            </g>
          							        </svg>
          							    </span>
          							</a>';
              return $edit . ' ' . $delete;
          })
          ->rawColumns(['reservation_desc', 'action'])
          ->make(true);
    }

     public static function execute($request, $data = null) {
        if (is_null($data)) {
            $data = static::$table;
            $data->created_by = sess_user('id');
            $data->created_at = currDate();
        }else{
            $data->updated_by = sess_user('id');
            $data->updated_at = currDate();

        }
        if ($request->company_id){
          $data->company_id = $request->company_id;
        }else{
          $data->company_id = sess_user('company_id');
        }

        if ($request->id || $request->id != "") {
            $data->id = strtoupper($request->id);
        }else{
            $data->id = generadeCode("Clinic","patient-visit",sess_user('company_id'),date('md'), $numb=3);
        }
        if ($request->name){
          $data->name = $request->name;
        }
        if ($request->gender_id){
          $data->gender_id = $request->gender_id;
        }
        if ($request->gender_id){
          $data->gender_id = $request->gender_id;
        }
        if ($request->gender_id){
          $data->gender_id = $request->gender_id;
        }
        if ($request->birthdate){
          if(!preg_match("/^[0-9]{4}-(0[1-9]|1[0-2])-(0[1-9]|[1-2][0-9]|3[0-1])$/",$request->birthdate)){
            $data->birthdate = Carbon::createFromFormat('d-m-Y', $request->birthdate)->format('Y-m-d');
          }else{
            $data->birthdate = $request->birthdate;
          }
        }
        if ($request->phone){
          $data->phone = $request->phone;
        }
        if ($request->email){
          $data->email = $request->email;
        }
        if ($request->consultation){
          $data->consultation = $request->consultation;
        }
        if ($request->city_id){
          $data->city_id = $request->city_id;
        }
        if ($request->address){
          $data->address = $request->address;
        }
        if ($request->sales_id){
          $data->sales_id = $request->sales_id;
        }
        if ($request->register_status){
          $data->register_status = $request->register_status;
        }
        if ($request->note){
          $data->note = $request->note;
        }
        if ($request->except("is_consultation")){
          $data->is_consultation = to_bool($request->is_consultation);
        }
        if ($request->except("is_order")){
          $data->is_order = to_bool($request->is_order);
        }
        if ($request->except("is_therapy")){
          $data->is_therapy = to_bool($request->is_therapy);
        }
        if ($request->consultation_date){
          if(!preg_match("/^[0-9]{4}-(0[1-9]|1[0-2])-(0[1-9]|[1-2][0-9]|3[0-1])$/",$request->consultation_date)){
            $data->consultation_date = Carbon::createFromFormat('d-m-Y H:i', $request->consultation_date)->format('Y-m-d H:i');
          }else{
            $data->consultation_date = $request->consultation_date;
          }
        }

        $data->save();

        return $data;
    }

}
