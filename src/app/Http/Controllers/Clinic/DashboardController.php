<?php

namespace App\Http\Controllers\Clinic;

use Session;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use PHPJasper\PHPJasper;

class DashboardController extends Controller {
    private static $module;
    private static $dashboard_date;
    public static function init(){
        static::$module = 'Dashboard Klinik';
        static::$dashboard_date = date('Y-m-d');
    }
    public static function index($request){
        static::init();
        $list_kunjungan = \DB::table('mst_patient_visit')
                    ->where('company_id',sess_user('company_id'))
                    ->where('consultation_date', $request->dashboard_date ? $request->dashboard_date:static::$dashboard_date)
                    ->orderBy('consultation_date','DESC')
                    ->get();
        $list_jadwal = \DB::table('sls_clinic_reservation')
                    ->select(\DB::raw('mst_patient_visit.id as reservationId'),'sls_clinic_reservation.*')
                    ->leftjoin('mst_patient_visit','sls_clinic_reservation.id','mst_patient_visit.id')
                    ->where('mst_patient_visit.company_id',sess_user('company_id'))
                    ->where('sls_clinic_reservation.schedule_date', $request->dashboard_date ? $request->dashboard_date:static::$dashboard_date)
                    ->orderBy('sls_clinic_reservation.schedule_date','DESC')
                    ->get();
        $counter_data = \DB::table('mst_patient_visit')
                    ->select(\DB::raw('count(mst_patient_visit.id) as jml'),'mst_patient_visit.register_status as name')
                    ->where('mst_patient_visit.company_id',sess_user('company_id'))
                    ->where('consultation_date', $request->dashboard_date ? $request->dashboard_date:static::$dashboard_date)
                    ->groupBy('mst_patient_visit.register_status')
                    ->get();
        $data_baru = \DB::table('mst_patient_visit')
                    ->select(\DB::raw('count(mst_patient_visit.id) as jml'))
                    ->where('mst_patient_visit.register_status','=','Baru')
                    ->where('mst_patient_visit.company_id',sess_user('company_id'))
                    ->where('consultation_date', $request->dashboard_date ? $request->dashboard_date:static::$dashboard_date)
                    ->groupBy('mst_patient_visit.register_status')
                    ->first();
        $data_ulang = \DB::table('mst_patient_visit')
                    ->select(\DB::raw('count(mst_patient_visit.id) as jml'))
                    ->where('mst_patient_visit.register_status','=','Ulang')
                    ->where('mst_patient_visit.company_id',sess_user('company_id'))
                    ->where('consultation_date', $request->dashboard_date ? $request->dashboard_date:static::$dashboard_date)
                    ->groupBy('mst_patient_visit.register_status')
                    ->first();
        $data_lunas = \DB::table('mst_patient_visit')
                    ->select(\DB::raw('count(mst_patient_visit.id) as jml'))
                    ->where('mst_patient_visit.register_status','=','Lunas')
                    ->where('mst_patient_visit.company_id',sess_user('company_id'))
                    ->where('consultation_date', $request->dashboard_date ? $request->dashboard_date:static::$dashboard_date)
                    ->groupBy('mst_patient_visit.register_status')
                    ->first();
        $data_lolos = \DB::table('mst_patient_visit')
                    ->select(\DB::raw('count(mst_patient_visit.id) as jml'))
                    ->where('mst_patient_visit.register_status','=','Lolos')
                    ->where('mst_patient_visit.company_id',sess_user('company_id'))
                    ->where('consultation_date', $request->dashboard_date ? $request->dashboard_date:static::$dashboard_date)
                    ->groupBy('mst_patient_visit.register_status')
                    ->first();

        $data["list_kunjungan"] = $list_kunjungan ? $list_kunjungan:null;
        $data["list_jadwal"] = $list_jadwal ? $list_jadwal:null;
        $data["data_baru"] = $data_baru ? ["jml"=>"Baru","name"=>$data_baru->jml]:["jml"=>"Baru","name"=>0];
        $data["data_ulang"] = $data_ulang ? ["jml"=>"Ulang","name"=>$data_ulang->jml]:["jml"=>"Ulang","name"=>0];
        $data["data_lunas"] = $data_lunas ? ["jml"=>"Lunas","name"=>$data_lunas->jml]:["jml"=>"Lunas","name"=>0];
        $data["data_lolos"] = $data_lolos ? ["jml"=>"Lolos","name"=>$data_lolos->jml]:["jml"=>"Lolos","name"=>0];
        $data["dashboard_date"] = $request->dashboard_date ? $request->dashboard_date:date('Y-m-d');
        $data["module"] = $request->dashboard_date ? $request->dashboard_date:date('Y-m-d');
        return view('clinic.dashboard',$data);
    }

}
