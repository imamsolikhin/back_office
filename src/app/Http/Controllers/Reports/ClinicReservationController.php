<?php

namespace App\Http\Controllers\Reports;

use DataTables;
use PDF;
use DB;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Anam\PhantomMagick\Converter;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use App\Http\Resources\Clinic\Reservation;

class ClinicReservationController extends Controller {

    public static function export ($table, $export, $is_reqs, $request) {
      $table = new Reservation();
      $query = DB::table($table->getTable())
                  ->select($table->getTable().".*","mst_gender.name as gender_name","users.name as sales_name", "users.phone as sales_phone")
                  ->join("mst_gender","mst_gender.id","=",$table->getTable().".gender_id")
                  ->join("users","users.id","=",$table->getTable().".sales_id")
                  ->where($table->getTable().".id",$request->id)
                  ->first();
      $data['btn_url'] = route('reports.download',['clinic-reservation',$export]).'?id='.$request->id;
      $data['data'] = $query;
      $data['is_btn'] = $is_reqs;
      $path = "reports.clinic.reservation.".strtolower("umi");

      if($is_reqs){
        return view($path,$data);
      }
      $exp = PDF::loadHTML(view($path, $data)->render())->setPaper('a4', 'portrait')->setWarnings(false);
      return $exp->download(strtoupper("thi")."-".$request->id.".pdf");
    }

}
