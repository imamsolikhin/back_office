<?php
namespace App\Http\Controllers\Advertising;

use Session;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class AdvertisingController extends Controller
{
    public function index($table, Request $request)
    {
      return getControllerName("Advertising", $table)::index($request);
    }

    public function list($table, Request $request)
    {
      return getControllerName("Advertising", $table)::list($request);
    }

    public function data($table, $id)
    {
      return getControllerName("Advertising", $table)::data($id);
    }

    public function save($table, Request $request)
    {
      return getControllerName("Advertising", $table)::save($request);
    }

    public function update($table, $id, Request $request)
    {
      return getControllerName("Advertising", $table)::update($id, $request);
    }

    public function delete($table,$id)
    {
      return getControllerName("Advertising", $table)::delete($id);
    }

}
