<?php

namespace App\Http\Controllers\Advertising;

use Session;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use PHPJasper\PHPJasper;

class DashboardController extends Controller {
    private static $module;
    private static $dashboard_date;
    private static $dashboard_startdate;
    private static $dashboard_enddate;
    public static function init(){
        static::$module = 'Dashboard Advertising';
        static::$dashboard_date = date('d-m-Y 00:00').' <=> '.date('d-m-Y 24:00');
        static::$dashboard_startdate = date('Y-m-d 00:00');
        static::$dashboard_enddate = date('Y-m-d 23:59');
    }
    public static function index($request){
        static::init();
        $start_date = $request->dashboard_startdate ? $request->dashboard_startdate:static::$dashboard_startdate;
        $end_date = $request->dashboard_enddate ? $request->dashboard_enddate:static::$dashboard_enddate;
        $dash = \DB::table('sls_product_visitor')
                    ->select('sls_product_visitor.*'
                      ,'mst_customer.full_name as full_name'
                      ,'mst_customer.phone as phone'
                      ,'mst_gender.id as gender_id'
                      ,'mst_gender.name as gender_name'
                      ,'mst_advertise.name as advertise_name'
                    )
                    ->join('mst_customer','mst_customer.id','=', 'sls_product_visitor.customer_id')
                    ->join('mst_gender','mst_gender.id','=', 'mst_customer.gender_id')
                    ->join('mst_advertise','mst_advertise.id','=', 'mst_customer.advertise_id')
                    ->where(function ($query)use($request) {
                      if($request->company_id){
                          $query->where('sls_product_visitor.company_id',$request->company_id);
                      }else{
                        $query->where('sls_product_visitor.company_id',sess_user('company_id'));
                      }
                    })
                    ->where('sls_product_visitor.created_at','>=' , $start_date)
                    ->where('sls_product_visitor.created_at','<=' , $end_date)
                    ->orderBy('sls_product_visitor.created_at','DESC')
                    ->get();

        $dash1 = \DB::table('sls_product_visitor')
                    ->select(\DB::raw('count(sls_product_visitor.id) as jml')
                    ,\DB::raw('sls_product_visitor.followup_status as status'))
                    ->where(function ($query)use($request) {
                      if($request->company_id){
                          $query->where('sls_product_visitor.company_id',$request->company_id);
                      }else{
                        $query->where('sls_product_visitor.company_id',sess_user('company_id'));
                      }
                    })
                    ->where('sls_product_visitor.created_at','>=' , $start_date)
                    ->where('sls_product_visitor.created_at','<=' , $end_date)
                    ->groupBy('sls_product_visitor.followup_status')
                    ->get();

        $dash2 = \DB::table('sls_product_visitor')
                    ->select(\DB::raw('count(sls_product_visitor.id) as jml')
                    ,\DB::raw('sls_product_visitor.followup_status as status'))
                    ->where(function ($query)use($request) {
                      if($request->company_id){
                          $query->where('sls_product_visitor.company_id',$request->company_id);
                      }else{
                        $query->where('sls_product_visitor.company_id',sess_user('company_id'));
                      }
                    })
                    ->where('sls_product_visitor.confirm_status',list_confirm_status()[5][0])
                    ->where('sls_product_visitor.created_at','>=' , $start_date)
                    ->where('sls_product_visitor.created_at','<=' , $end_date)
                    ->groupBy('sls_product_visitor.followup_status')
                    ->get();

        $dash3 = \DB::table('ads_campaign')
                    ->select(
                    \DB::raw('IFNULL(SUM(ads_campaign.price),0) as price')
                    ,\DB::raw('IFNULL(COUNT(ads_campaign.id),0) as jml')
                    ,\DB::raw('IFNULL(COUNT(sls_product_visitor.id),0)+IFNULL(COUNT(sls_clinic_visitor.id),0) as visitor')
                    )
                    ->leftjoin('sls_product_visitor', function ($join) {
                        $join->on('sls_product_visitor.advertise_id', '=', 'ads_campaign.advertise_id');
                        $join->on('sls_product_visitor.transaction_date', '>=', 'ads_campaign.start_date');
                        $join->on('sls_product_visitor.transaction_date', '<=', 'ads_campaign.end_date');
                        $join->where('sls_product_visitor.company_id','ads_campaign.company_id');
                        $join->where('sls_product_visitor.followup_status',0);
                    })
                    ->leftjoin('sls_clinic_visitor', function ($join) {
                        $join->on('sls_clinic_visitor.advertise_id', '=', 'ads_campaign.advertise_id');
                        $join->on('sls_clinic_visitor.created_at', '>=', 'ads_campaign.start_date');
                        $join->on('sls_clinic_visitor.created_at', '<=', 'ads_campaign.end_date');
                        $join->where('sls_clinic_visitor.company_id','ads_campaign.company_id');
                        $join->where('sls_clinic_visitor.followup_status',0);
                    })
                    ->where('ads_campaign.created_at','>=' , $start_date)
                    ->where('ads_campaign.created_at','<=' , $end_date)
                    ->first();

        $dash4 = \DB::table('sls_sales_order')
                    ->select(
                    \DB::raw('count(sls_sales_order.id) as jml'),
                    \DB::raw('sls_sales_order.confirm_status as status')
                    )
                    ->join('mst_customer','mst_customer.id','=', 'sls_sales_order.customer_id')
                    ->join('mst_gender','mst_gender.id','=', 'mst_customer.gender_id')
                    ->join('mst_advertise','mst_advertise.id','=', 'mst_customer.advertise_id')
                    ->where(function ($query)use($request) {
                      if($request->company_id){
                          $query->where('sls_sales_order.company_id',$request->company_id);
                      }else{
                        $query->where('sls_sales_order.company_id',sess_user('company_id'));
                      }
                    })
                    ->where('sls_sales_order.confirm_status','!=',null)
                    ->where('sls_sales_order.created_at','>=' , $start_date)
                    ->where('sls_sales_order.created_at','<=' , $end_date)
                    ->groupBy('sls_sales_order.confirm_status')
                    ->get();

        $dash5 = \DB::table('sls_product_visitor')
                    ->select(\DB::raw('IFNULL(users.name,sls_sales_order.author) as author'))
                    ->join('users','users.id','=', 'sls_product_visitor.created_by')
                    ->leftjoin('sls_sales_order','sls_sales_order.id','=', 'sls_product_visitor.id')
                    ->where(function ($query)use($request) {
                      if($request->company_id){
                          $query->where('sls_product_visitor.company_id',$request->company_id);
                      }else{
                        $query->where('sls_product_visitor.company_id',sess_user('company_id'));
                      }
                    })
                    ->where('sls_product_visitor.created_at','>=' , $start_date)
                    ->where('sls_product_visitor.created_at','<=' , $end_date)
                    ->groupBy('sls_product_visitor.created_by')
                    ->orderBy('sls_product_visitor.created_by','ASC')
                    ->get();

        $dash6 = \DB::table('sls_product_visitor')
                    ->select(
                    \DB::raw('IFNULL(count(sls_product_visitor.id),0) as visit'),
                    \DB::raw('IFNULL(count(sls_sales_order.id),0) as so_order'),
                    \DB::raw('IFNULL(sls_sales_order.confirm_status,"Order") as so_status'),
                    \DB::raw('IFNULL(sls_product_visitor.confirm_status,"Lose") as visit_status'),
                    \DB::raw('IFNULL(users.name,sls_product_visitor.author) as author')
                    )
                    ->join('users','users.id','=', 'sls_product_visitor.created_by')
                    ->leftjoin('sls_sales_order','sls_sales_order.id','=', 'sls_product_visitor.id')
                    ->where(function ($query)use($request) {
                      if($request->company_id){
                          $query->where('sls_product_visitor.company_id',$request->company_id);
                      }else{
                        $query->where('sls_product_visitor.company_id',sess_user('company_id'));
                      }
                    })
                    ->where('sls_product_visitor.created_at','>=' , $start_date)
                    ->where('sls_product_visitor.created_at','<=' , $end_date)
                    ->groupBy('sls_product_visitor.created_by')
                    ->groupBy('sls_product_visitor.confirm_status')
                    ->orderBy('sls_product_visitor.created_by','ASC')
                    ->get();

        $dash7 = \DB::table('ads_campaign')
                    ->select(\DB::raw('mst_advertise.name as advertise_name'))
                    ->join('mst_advertise','mst_advertise.id','=', 'ads_campaign.advertise_id')
                    ->where('ads_campaign.company_id',sess_user('company_id'))
                    ->groupBy('ads_campaign.advertise_id')
                    ->orderBy('ads_campaign.advertise_id','ASC')
                    ->get();

        $dash8 = \DB::table('ads_campaign')
                    ->select('ads_campaign.*'
                    ,\DB::raw('mst_advertise.name as advertise_name')
                    ,\DB::raw('IFNULL(COUNT(sls_product_visitor.id),0)+IFNULL(COUNT(sls_clinic_visitor.id),0) as visitor')
                    ,\DB::raw('IFNULL(COUNT(sales_order.id),0) as so_order')
                    )
                    ->join('mst_advertise','mst_advertise.id','ads_campaign.advertise_id')
                    ->leftjoin(\DB::raw('sls_product_visitor sales_order'), function ($join) {
                        $join->on('sales_order.advertise_id', '=', 'ads_campaign.advertise_id');
                        $join->on('sales_order.created_at', '>=', 'ads_campaign.start_date');
                        $join->on('sales_order.created_at', '<=', 'ads_campaign.end_date');
                        $join->on('sales_order.company_id','ads_campaign.company_id');
                        $join->where('sales_order.followup_status',0);
                        $join->where('sales_order.confirm_status','!=',null);
                    })
                    ->leftjoin('sls_product_visitor', function ($join) {
                        $join->on('sls_product_visitor.advertise_id', '=', 'ads_campaign.advertise_id');
                        $join->on('sls_product_visitor.created_at', '>=', 'ads_campaign.start_date');
                        $join->on('sls_product_visitor.created_at', '<=', 'ads_campaign.end_date');
                        $join->on('sls_product_visitor.company_id','ads_campaign.company_id');
                        $join->where('sls_product_visitor.followup_status',0);
                    })
                    ->leftjoin('sls_clinic_visitor', function ($join) {
                        $join->on('sls_clinic_visitor.advertise_id', '=', 'ads_campaign.advertise_id');
                        $join->on('sls_clinic_visitor.created_at', '>=', 'ads_campaign.start_date');
                        $join->on('sls_clinic_visitor.created_at', '<=', 'ads_campaign.end_date');
                        $join->on('sls_clinic_visitor.company_id','ads_campaign.company_id');
                        $join->where('sls_clinic_visitor.followup_status',0);
                    })
                    ->where('ads_campaign.company_id',sess_user('company_id'))
                    ->groupBy('ads_campaign.id')
                    ->orderBy('ads_campaign.created_at','DESC')
                    ->get();

        $data["module"] = static::$module;
        $data["dash"] = $dash ? $dash:null;
        $data["dash1"] = $dash1;
        $data["dash2"] = $dash2;
        $data["dash3"] = $dash3;
        $data["dash4"] = $dash4;
        $data["dash5"] = $dash5;
        $data["dash6"] = $dash6;
        $data["dash7"] = $dash7;
        $data["dash8"] = $dash8;
        // dd($data);
        $data["dashboard_date"] = $request->dashboard_date ? $request->dashboard_date:static::$dashboard_date;
        $data["dashboard_startdate"] = $request->dashboard_startdate ? $request->dashboard_startdate:static::$dashboard_startdate;
        $data["dashboard_enddate"] = $request->dashboard_enddate ? $request->dashboard_enddate:static::$dashboard_enddate;
        $data["dashboard_datetime"] = date('m-d-Y H:i',strtotime($data["dashboard_startdate"]))." <=> ".date('m-d-Y H:i',strtotime($data["dashboard_enddate"]));
        return view('advertising.dashboard',$data);
    }

}
