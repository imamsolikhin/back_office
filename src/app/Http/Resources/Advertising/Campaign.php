<?php

namespace App\Http\Resources\Advertising;

use Illuminate\Database\Eloquent\Model;

class Campaign extends Model {

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'ads_campaign';
    protected $guarded = [];

    protected $primaryKey = 'id';
    public $incrementing = false;
    protected $keyType = 'string';

}
