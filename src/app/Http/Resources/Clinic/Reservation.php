<?php

namespace App\Http\Resources\Clinic;

use Illuminate\Database\Eloquent\Model;

class Reservation extends Model {

    /**
    * The table associated with the model.
    *
    * @var string
    */
    protected $table = 'sls_clinic_reservation';
    protected $guarded = [];

    protected $primaryKey = 'id';
    public $incrementing = false;
    protected $keyType = 'string';

}
