<?php

namespace App\Http\Resources\Clinic;

use Illuminate\Database\Eloquent\Model;

class PatientRecept extends Model {

      /**
      * The table associated with the model.
      *
      * @var string
      */
      protected $table = 'mst_patient_visit';
      protected $guarded = [];

      protected $primaryKey = 'id';
      public $incrementing = false;
      protected $keyType = 'string';

  }
