<?php

namespace App\Http\Resources\Sales;

use Illuminate\Database\Eloquent\Model;

class SalesOrderClosingDetail extends Model {

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'sls_sales_order_closing_detail';
    protected $guarded = [];

    protected $primaryKey = 'id';
    public $incrementing = false;
    protected $keyType = 'string';

}
