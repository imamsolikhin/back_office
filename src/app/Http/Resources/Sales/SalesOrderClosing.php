<?php

namespace App\Http\Resources\Sales;

use Illuminate\Database\Eloquent\Model;

class SalesOrderClosing extends Model {

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'sls_sales_order_closing';
    protected $guarded = [];

    protected $primaryKey = 'id';
    public $incrementing = false;
    protected $keyType = 'string';

}
