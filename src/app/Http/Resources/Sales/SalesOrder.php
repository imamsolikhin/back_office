<?php

namespace App\Http\Resources\Sales;

use Illuminate\Database\Eloquent\Model;

class SalesOrder extends Model {

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'sls_sales_order';
    protected $guarded = [];

    protected $primaryKey = 'id';
    public $incrementing = false;
    protected $keyType = 'string';

}
