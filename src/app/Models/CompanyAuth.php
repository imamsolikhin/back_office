<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;

class CompanyAuth extends Model
{
     protected $table = 'company_auth';
     protected $primaryKey = 'id';

     public $incrementing = false;
     protected $keyType = 'string';
}
