<!doctype html>
  <html lang="en">
  <head>
  <meta charset="UTF-8">
  <title>@isset($title){{$title}}@endisset</title>
  </head>
  <body style="width: 40rem;">
  <style type="text/css">
    *, input, textarea {
       font-size: 1rem;
       font-weight: 900;
    }
  </style>
    <div class="card" style="border-radius: 10px; background-color:#bbbaa9">
      <div style="padding-top:10px;"/>
      <div class="card" style="border-radius: 10px;margin: 0px 10px; background-color:#3a4138">
        <table>
          <tr>
            <td><img style="border-radius:10px; margin:0px 2px; height:80px" src="{{asset('media/logos/logo_tga.png')}}" alt="" width="200"/></td>
            <td valign="bottom"><img style="border-radius:10px; margin:0px 2px; height:50px" src="{{asset('media/logos/medical_terapi.png')}}" alt="" width="100%"/></td>
          </tr>
        </table>
      </div>
      <div style="padding-top:5px;"/>
      <div class="card" style="border-radius: 10px;margin: 0px 10px; background-color:#c3cebe; color:#3a4138;">
         <table width="100%">
           <tr>
             <td colspan="3" valign="top" style="white-space: nowrap;">
               <center style="padding-top: 10px; ">
                 <label style="font-weight:900; font-size:1.5rem; border-bottom:thick double; ">ANDA BERHASIL RESERVASI</label>
               </center>
             </td>
           </tr>
           <tr>
             <td valign="top" width="100%">
               <table  width="100%">
                 <tr><td valign="top" style="white-space: nowrap">No Reservasi</td></tr>
                 <tr><td valign="top" style="text-align: justify; text-justify: newspaper;"><input style="border-radius:5px; width:98%;" value="RSM001-150621001"/></td></tr>
                 <tr><td valign="top">Kunjungan</td></tr>
                 <tr><td valign="top" style="text-align: justify; text-justify: newspaper;"><input style="border-radius:5px; width:98%;" value="Minggu, 15 Jun 2021 13:00"/></td></tr>
                 <tr><td valign="top">Nama Pasien</td></tr>
                 <tr><td valign="top" style="text-align: justify; text-justify: newspaper;"><input style="border-radius:5px; width:98%;" value="Imam Solikhin"/></td></tr>
                 <tr><td valign="top">No Telp</td></tr>
                 <tr><td valign="top" style="text-align: justify; text-justify: newspaper;"><input style="border-radius:5px; width:98%;" value="081808178118"/></td></tr>
                 <tr><td valign="top">Keluhan</td></tr>
                 <tr><td valign="top" style="text-align: justify; text-justify: newspaper;"><textarea style="border-radius:5px; width:98%;height:3rem">Pusing karena kurang tidur semalaman</textarea></td></tr>
                 <tr><td valign="top">Alamat</td></tr>
                 <tr><td valign="top" style="text-align: justify; text-justify: newspaper;" height="100"><textarea style="border-radius:5px; width:98%; height:5rem">Taman Griya Permai, Jl. Pinang Griya Raya, RT.001/RW.005, Pinang, Kec. Pinang, Kota Tangerang, Banten 15145</textarea></td></tr>
                 <tr><td valign="top">Cust Services</td></tr>
                 <tr><td valign="top" style="text-align: justify; text-justify: newspaper;"><input style="border-radius:5px; width:98%;" value="Anna / 081808178118"/></td></tr>
               </table>
             </td>
             <td valign="mid">
               <center>
                 <img style="border-radius:10px; margin:8px 2px;" src="https://terapigarangarang.com/wp-content/uploads/2019/04/IMG-1544-Copy.jpg" alt="" height="450" width="300"/>
               </center>
             </td>
           </tr>
           <tr>
             <td colspan="2" valign="bottom"><img style="border-radius:10px; margin:0px 2px;" src="{{asset('media/logos/reservasi_bawah_tga.png')}}" alt="" width="100%"/></td>
           </tr>
           <tr>
             <td colspan="2" valign="center" bgcolor="#3A4138" style="border-radius:10px;">
               <center style="color:#E3E0E4;">
                 <label style="font-size:1rem; font-weight:100;">Jl. Raden Saleh, Ruko bumi permata indah Blok RI No 38C Karang Mulya</label></br>
                 <label style="font-size:1rem; font-weight:100;">Karang Tengah Ciledug Tangerang</label>
               </center>
             </td>
           </tr>
         </table>
      </div>
      <div style="padding-bottom:10px;"/>
    </div>
  </body>
  @if ($is_btn)
    <div style="text-align: center;">
      <form action="{{$btn_url}}">
          <input type="submit" value="download" />
      </form>
    </div>
  @endif
  </html>
