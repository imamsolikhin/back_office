<!doctype html>
  <html lang="en">
  <head>
  <meta charset="UTF-8">
  <title>@isset($title){{$title}}@endisset</title>
  </head>
  <body style="width: 40rem;">
  <style type="text/css">
    *, input, textarea {
       font-size: 1rem;
       font-weight: 900;
    }
  </style>
    <div class="card" style=" background-color:#866f47">
      <div style=" padding-top:2rem; margin:  2rem 2rem -1rem 2rem;">
        <div style="border-top:3px #3e2803 solid; border-left:3px #3e2803 solid; border-right:3px #3e2803 solid; padding-top:10px;">
          <div style="margin: 0px 10px; background-color:#3e2803">
          <table>
            <tr>
              <td><img style="border-radius:10px; margin:0px 2px; height:80px" src="{{asset('media/logos/logo_tga.png')}}" alt="" width="200"/></td>
              <td valign="bottom"><img style="border-radius:10px; margin:0px 2px; height:50px" src="{{asset('media/logos/medical_terapi.png')}}" alt="" width="100%"/></td>
            </tr>
          </table>
        </div>
          <div style="padding-top:5px;"/>
          <div style="margin: 10px 0px;">
           <table width="100%">
             <tr>
               <td colspan="3" valign="top" style="white-space: nowrap;">
                 <center style="padding-top: 10px; ">
                   <label style="background-color:#3e2803; color:#866f47; font-weight:900; font-size:1.2rem; padding:.1rem 3rem;">ANDA TELAH BERHASIL RESERVASI</label>
                 </center>
               </td>
             </tr>
             <tr>
               <td valign="top" width="100%">
                 <center>
                     <table width="80%" style="color:black; padding:1rem 1rem;">
                       <tr><td valign="top">Nama Pasien</td><td valign="top" style="text-align: justify; text-justify: newspaper;"><input style="border-radius:5px; width:98%;" value="{{$data->gender_name}} {{$data->full_name}}" readonly/></td></tr>
                       <tr><td valign="top">No Telp</td><td valign="top" style="text-align: justify; text-justify: newspaper;"><input style="border-radius:5px; width:98%;" value="{{$data->phone}}" readonly/></td></tr>
                       <tr><td valign="top" style="white-space: nowrap">No Reservasi</td><td valign="top" style="text-align: justify; text-justify: newspaper;"><input style="border-radius:5px; width:98%;" value="{{$data->id}}" readonly/></td></tr>
                       <tr><td valign="top">Kunjungan</td><td valign="top" style="text-align: justify; text-justify: newspaper;"><input style="border-radius:5px; width:98%;" value="{{hari_ini(strtotime($data->schedule_date))}} {{date('d-m-Y H:i',strtotime($data->schedule_date))}}" readonly/></td></tr>
                       <tr><td valign="top">Cust Services</td><td valign="top" style="text-align: justify; text-justify: newspaper;"><input style="border-radius:5px; width:98%;" value="{{$data->sales_name}} / {{$data->sales_phone}}" readonly/></td></tr>
                     </table>
                 </center>
               </td>
             </tr>
             <tr>
               <td colspan="3" valign="top" style="white-space: nowrap;">
                 <center style="padding-top: 10px; ">
                   <label style="background-color:#3e2803; color:#866f47; font-weight:900; font-size:1.2rem; padding:.1rem 3rem;">ANDA TELAH BERHASIL RESERVASI</label>
                 </center>
               </td>
             </tr>
             <tr>
               <td colspan="3" valign="top" style="white-space: nowrap;">
                 <center style="padding-top: 10px; ">
                   <label style="background-color:#3e2803; color:#866f47; font-weight:900; font-size:1.2rem; padding:.1rem 3rem;">ANDA TELAH BERHASIL RESERVASI</label>
                 </center>
               </td>
             </tr>
           </table>
        </div>
        </div>
      </div>
    </div>
    <div style="background-color:#3e2803">
      <img style="border-radius:10px; margin:0px 2px; height:80px" src="" alt="" width="570"/>
    </div>
  </body>
  @if ($is_btn)
    <div style="text-align: center;">
      <a href="{{$btn_url}}">download</a>
    </div>
  @endif
  </html>
