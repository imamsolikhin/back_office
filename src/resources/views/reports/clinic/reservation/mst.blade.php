<!doctype html>
  <html lang="en">
  <head>
  <meta charset="UTF-8">
  <title>@isset($title){{$title}}@endisset</title>
  </head>
  <body style="width: 40rem;">
  <style type="text/css">
    *, input, textarea {
       font-size: 1rem;
       font-weight: 900;
    }
  </style>
    <div class="card" style=" background-color:#e3e9ee8c">
      <div style="padding-top:10px;"/>
      <div class="card" style="margin: 0px 10px; background-color:#042f66">
        <table>
          <tr>
            <td><img style="border-radius:10px; margin:0px 2px; height:80px" src="{{asset('media/logos/logo_tga.png')}}" alt="" width="200"/></td>
            <td valign="bottom"><img style="border-radius:10px; margin:0px 2px; height:50px" src="{{asset('media/logos/medical_terapi.png')}}" alt="" width="100%"/></td>
          </tr>
        </table>
      </div>
      <div style="padding-top:5px;"/>
      <div class="card" style="margin: 0px 10px;">
         <table width="100%">
         <tr>
           <td colspan="2" valign="center" bgcolor="#042f66" style="">
             <center style="color:white;">
               <label style="font-size:1rem; font-weight:100;">MEDIASI TERAPI PENYAKIT DALAM DAN SYARAF</label></br>
             </center>
           </td>
         </tr>
           <tr>
             <td colspan="3" valign="top" style="white-space: nowrap;">
               <center style="padding-top: 8px; ">
                 <label style="background-color:#042f66; color:white; font-weight:300; font-size:1rem; padding:.1rem 10rem; border-radius: 10px;">ANDA TELAH BERHASIL RESERVASI</label>
               </center>
             </td>
           </tr>
           <tr></tr>
           <tr>
             <td valign="top" width="100%">
               <center>
                   <table width="80%" style="padding:1rem 1rem;">
                     <tr><td valign="top"><input style="width:100%; border-radius:5px" value="Nama Pasien&emsp;&emsp; : {{$data->gender_name}} {{$data->full_name}}"/></td></tr>
                     <tr><td valign="top"><input style="width:100%; border-radius:5px" value="No Telp&emsp;&emsp;&emsp;&emsp;&emsp;: {{$data->phone}}"/></td></tr>
                     <tr><td valign="top"><input style="width:100%; border-radius:5px" value="Keluhan&emsp;&emsp;&emsp;&emsp;  : {{$data->consultation}}"/></td></tr>
                     <tr><td valign="top"><input style="width:100%; border-radius:5px" value="No Reservasi&emsp;&emsp; : {{$data->id}}"/></td></tr>
                     <tr><td valign="top"><input style="width:100%; border-radius:5px" value="Kunjungan&emsp;&emsp;&emsp; : {{hari_ini(strtotime($data->schedule_date))}} {{date('d-m-Y H:i',strtotime($data->schedule_date))}}"/></td></tr>
                     <tr><td valign="top"><input style="width:100%; border-radius:5px" value="Cust Services&emsp;&emsp;: {{$data->sales_name}} / {{$data->sales_phone}}"/></td></tr>
                   </table>
               </center>
             </td>
           </tr>
           <tr>
             <td colspan="2" valign="bottom"><img style="border-radius:10px; margin:0px 2px;" src="{{asset('media/logos/reservasi_bawah_tga.png')}}" alt="" width="100%"/></td>
           </tr>
           <tr>
             <td colspan="2" valign="center" bgcolor="#042f66" style="">
               <center style="color:#E3E0E4;">
                 <label style="font-size:1rem; font-weight:100;">JL. Akses Graha Raya Boulevard Regency Ruko</label></br>
                 <label style="font-size:1rem; font-weight:100;">Grand Fortune FB G.10 Kec. Ciledug Tangerang Banten 15324</label>
               </center>
             </td>
           </tr>
         </table>
      </div>
      <div style="padding-bottom:10px;"/>
    </div>
  </body>
  @if ($is_btn)
    <div style="text-align: center;">
      <a href="{{$btn_url}}">download</a>
    </div>
  @endif
  </html>
