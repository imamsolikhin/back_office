<!doctype html>
  <html lang="en">
  <head>
  <meta charset="UTF-8">
  <title>@isset($title){{$title}}@endisset</title>
  </head>
  <body style="width: 40rem;">
  <style type="text/css">
    *, input, textarea {
       font-size: 1rem;
       font-weight: 900;
    }
  </style>
    <div class="card" style="border-radius: 3px; background-color:#010000">
      <div style="padding:1rem 2rem;"/>
      <div class="card" style="border-radius: 10px;margin: 0px 10px; background-color:#010000">
        <table>
          <tr>
            <td><img style="border-radius:10px; margin:0px 2px; height:80px" src="{{asset('media/logos/logo_tga.png')}}" alt="" width="200"/></td>
            <td valign="bottom"><img style="border-radius:10px; margin:0px 2px; height:50px" src="{{asset('media/logos/medical_terapi.png')}}" alt="" width="100%"/></td>
          </tr>
        </table>
      </div>
      <div style="padding-top:5px;"/>
      <div class="card" style="border-radius: 10px;margin: 0px 10px; background-color:#010000; color:#8F7248;">
         <table width="100%">
           <tr>
             <td colspan="3" valign="top" style="white-space: nowrap;">
               <center style="padding-top: 10px; ">
                 <label style="font-weight:900; padding:0rem 5rem; font-size:1.2rem; background-color:#8F7248; color:#010000;">ANDA TELAH BERHASIL RESERVASI</label>
               </center>
             </td>
           </tr>
           <tr>
             <td valign="top" width="100%">
               <table  width="100%">
                 <tr><td valign="top" style="white-space: nowrap">No Reservasi</td></tr>
                 <tr><td valign="top" style="text-align: justify; text-justify: newspaper;"><input style="border-radius:5px; width:98%;" value="{{$data->id}}" readonly/></td></tr>
                 <tr><td valign="top">Nama Pasien</td></tr>
                 <tr><td valign="top" style="text-align: justify; text-justify: newspaper;"><input style="border-radius:5px; width:98%;" value="{{$data->gender_name}} {{$data->full_name}}" readonly/></td></tr>
                 <tr><td valign="top" style="white-space: nowrap">No Telpon</td></tr>
                 <tr><td valign="top" style="text-align: justify; text-justify: newspaper;"><input style="border-radius:5px; width:98%;" value="{{$data->phone}}" readonly/></td></tr>
                 <tr><td valign="top">Keluhan</td></tr>
                 <tr><td valign="top" style="text-align: justify; text-justify: newspaper;"><textarea style="border-radius:5px; width:98%;height:3rem" readonly>{{$data->consultation}}</textarea></td></tr>
                 <tr><td valign="top">Kunjungan</td></tr>
                 <tr><td valign="top" style="text-align: justify; text-justify: newspaper;"><input style="border-radius:5px; width:98%;" value="{{hari_ini(strtotime($data->schedule_date))}} {{date('d-m-Y H:i',strtotime($data->schedule_date))}}" readonly/></td></tr>
                 <tr><td valign="top">Cust Services</td></tr>
                 <tr><td valign="top" style="text-align: justify; text-justify: newspaper;"><input style="border-radius:5px; width:98%;" value="{{$data->sales_name}} / {{$data->sales_phone}}" readonly/></td></tr>
               </table>
             </td>
             <td valign="mid">
               <center>
                 <img style="border-radius:10px; margin:8px 2px;" src="https://terapigarangarang.com/wp-content/uploads/2019/04/IMG-1544-Copy.jpg" alt="" height="325" width="300"/>
               </center>
             </td>
           </tr>
           <tr>
             <td valign="mid" colspan="2">
               <center style="padding: 1rem 1rem; ">
                 <label style="font-weight:900; font-size:1.5rem; ">BUKA SETIAP HARI</label>
               </center>
             </td>
           </tr>
           <tr>
             <td colspan="2" valign="center" bgcolor="#8F7248" style="border-radius:10px;">
               <center style="color:#010000;">
                 <label style="font-size:1rem; font-weight:100;">Jl. Raden Saleh, Ruko bumi permata indah Blok RI No 38C Karang Mulya</label></br>
                 <label style="font-size:1rem; font-weight:100;">Karang Tengah Ciledug Tangerang</label>
               </center>
             </td>
           </tr>
         </table>
      </div>
    </div>
  </body>
  @if ($is_btn)
    <div style="text-align: center;">
      <a href="{{$btn_url}}">download</a>
    </div>
  @endif
  </html>
