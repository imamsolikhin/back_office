@extends('layout.default')

@section('content')
<div class="subheader py-2 {{ Metronic::printClasses('subheader', false) }}" id="kt_subheader">
  <div class="{{ Metronic::printClasses('subheader-container', false) }} d-flex align-items-center justify-content-between flex-wrap flex-sm-nowrap">
    <div class="d-flex align-items-baseline flex-wrap mr-5">
      <h5 class="text-dark font-weight-bold my-1 mr-5">
        {{ $module_alias }}
      </h5>
      <ul class="breadcrumb breadcrumb-transparent breadcrumb-dot font-weight-bold p-0 my-2 font-size-sm">
        <li class="breadcrumb-item">
          <a href="{{ route('dashboard') }}" class="text-muted">Dashboard</a>
        </li>
        <li class="breadcrumb-item">
          <a href="{{ $path }}" class="text-muted">{{ $module_alias }}</a>
        </li>
        <li class="breadcrumb-item">
          <a href="#view" class="text-muted">View</a>
        </li>
      </ul>
    </div>
  </div>
</div>

@include('inc.error-list')
@include('inc.success-notif')
@include('inc.danger-notif')
<div class="card card-custom body-container">
  <div class="card-header bg-danger flex-wrap border-1 pt-1 pb-0 mb-2" style="min-height: 0;">
    <div class="card-title pt-1 pb-1">
      <h3 class="card-label font-weight-bolder text-white">{{ $module_alias }}
        <div class="text-muted pt-2 font-size-lg">show Datatable from table {{ $module_alias }}</div>
      </h3>
    </div>
    <div class="card-toolbar pt-1 pb-0 col-lg-8 d-flex flex-row-reverse">
        <div class="col-lg-4">
            <select class="form-control select2" id="company_id" name="company_id" style="width: 100%;">
              @php $data = list_model('Master','Company') @endphp
              @isset ($data)
                @foreach($data as $rs)
                  @if($rs->id == sess_user('company_id'))
                    <option value="{{ $rs->id }}" selected>{{ $rs->name }}</option>
                  @else
                    <option value="{{ $rs->id }}">{{ $rs->name }}</option>
                  @endif
                @endforeach
              @endisset
           </select>
        </div>
    </div>
  </div>
  <div class="card-body pt-1">
    <table class="table table-bordered table-hover w100" cellspacing="0" id="datatable" style="width: 1070px !important;"></table>
  </div>
</div>
@endsection

{{-- Styles Section --}}
@section('styles')
<link rel="stylesheet" href="{{ config('app.url') }}global/vendor/datatables-bootstrap/dataTables.bootstrap.css">
<link rel="stylesheet" href="{{ config('app.url') }}global/vendor/datatables-responsive/dataTables.responsive.min.css">
<link rel="stylesheet" href="{{ config('app.url') }}plugins/custom/datatables/datatables.bundle.css">
<link rel="stylesheet" href="{{ config('app.url') }}css/inject.css">
@endsection

{{-- Scripts Section --}}
@section('scripts')
@include ('inc.confirm-modal')

<script src="{{ config('app.url') }}global/vendor/datatables/jquery.dataTables.js"></script>
<script src="{{ config('app.url') }}global/vendor/datatables-bootstrap/dataTables.bootstrap.js"></script>
<script src="{{ config('app.url') }}global/vendor/datatables-responsive/dataTables.responsive.js"></script>
<script src="{{ config('app.url') }}js/inject.js"></script>
<script type="text/javascript">
  var start_date = "";
  var end_date = "";
  $(document).ready(function() {
    $(".is_reqs").hide();
    $('.select2').select2({});
    $('.datetime-input').datepicker({
        format: 'dd-mm-yyyy',
        inline: true,
    });
    $('.datetimepicker-input').datetimepicker({
        format: 'dd-mm-yyyy hh:ii',
        inline: true,
    });

    $("div.datesearchbox").html('<div class="input-group"> <div class="input-group-addon"> <i class="glyphicon glyphicon-calendar"></i> </div><input type="text" class="form-control text-center datetimepicker-input" id="datesearch_start" placeholder="Search by date range" value="{{date('d-m-Y H:i', strtotime($dashboard_startdate))}}">&nbsp;to&nbsp;<input type="text" class="form-control  text-center datetimepicker-input" id="datesearch_end" placeholder="Search by date range" value="{{date('d-m-Y H:i', strtotime($dashboard_enddate))}}"></div>');
    $("#datesearch_start, #datesearch_end").attr("readonly",true);
    $("#datesearch_start, #datesearch_end, #company_id").change(function(){
      refresh_table();
    });
    $('#datesearch_start, #datesearch_end').datetimepicker({
        format: 'dd-mm-yyyy hh:ii',
        inline: true,
     });
  });

  $('[data-switch=true]').bootstrapSwitch('state', true);
  $('#status').on('switchChange.bootstrapSwitch', function (event, state) {
      var x = $(this).data('on-text');
      var y = $(this).data('off-text');
      if ($("#status").is(':checked')) {
          $(".is_reqs").show(500);
      } else {
          $(".is_reqs").hide(500);
      }
  });


  $('.datetimepicker-input').datetimepicker({
      format: 'dd-mm-yyyy hh:ii'
  });
  $('[data-switch=true]').bootstrapSwitch();
  var table = $('#datatable').dataTable({
    pageLength: 5,
		scrollCollapse: true,
    responsive: true,
    searchDelay: 800,
    processing: true,
    serverSide: true,
    render: true,
    select: true,
    searching: true,
    lengthMenu: [[5, 10, 25, 50, 100, 200], [5, 10, 25, 50, 100, 200]],
    ajax: {
      method: 'POST',
      url : '{{ $path }}/list',
      headers: {
        'X-CSRF-TOKEN': '{{ csrf_token() }}'
      },
      data: function (d) {
        d.from_date = formatdate($("#datesearch_start").val());
        d.to_date = formatdate($(" #datesearch_end").val(),true);
        d.company_id = $("#company_id").val();
      }
    },
    columns: [
      {title: "No", data: 'DT_RowIndex', defaultContent: '-', class: 'text-center dt-body-nowrap', orderable: false, searchable: false, autoHide: false},
      {title: "No Order", data: 'id', defaultContent: '-', class: 'text-center dt-body-nowrap'},
      {title: "Resi", data: 'delivery_no', defaultContent: '-', class: 'text-center dt-body-nowrap'},
      {title: "Cust ID", data: 'customer_id', defaultContent: '-', class: 'text-center dt-body-nowrap'},
      {title: "Advertise", data: 'advertise_name', name: 'mst_advertise.name', defaultContent: '-', class: 'text-center dt-body-nowrap'},
      {title: "Cust Name", data: 'full_name', name: 'mst_customer.full_name', defaultContent: '-', class: 'text-center dt-body-nowrap', autoHide: false},
      {title: "Cust Phone", data: 'phone', name: 'mst_customer.phone', defaultContent: '-', class: 'text-center dt-body-nowrap', autoHide: false},
      {title: "Marketing", data: 'author', defaultContent: '-', class: 'text-center dt-body-nowrap',orderable: false, searchable: false},
      {title: "Status", data: 'status', defaultContent: '-', class: 'text-center dt-body-nowrap',orderable: false, searchable: false},
      {title: "Actions", data: 'action', orderable: false, searchable: false, responsivePriority: -1},
    ],
    order: [[1, 'asc']],
    bStateSave: true,
    dom:  "<'row'<'col-sm-4'l><'col-sm-5' <'datesearchbox'>><'col-sm-3'f>>" +
          "<'row'<'col-sm-12'tr>>" +
          "<'row'<'col-sm-5'i><'col-sm-7'p>>",
    columnDefs: [
      {
        targets: [0,-1],
        className: 'text-center visible dt-body-nowrap'
      },
    ],
    initComplete: function() {
      $('.tl-tip').tooltip();
    }
  });

  function check_data(id = "",name = "",phone = "") {
      $.ajax({
          method: 'POST',
          url : '{{ $path }}/list',
          headers: {
            'X-CSRF-TOKEN': '{{ csrf_token() }}'
          },
          data: {
            // from_date : $("#dashboard_startdate").val(),
            // to_date : $("#dashboard_enddate").val(),
            company_id : $("#company_id").val(),
            check_so_order : id,
            check_so_phone : phone,
            check_so_customer : name,
          },
          success: function (response) {
            var format = "";
            // format += "*LAPORAN CHECK SO Validation*";
            // format += "<br>";
            var head = "";
            var list = "";
            var no = 1;
            for(var i in response.data){
              var rs = response.data[i];
              var voucher = parseFloat(rs.voucher_total);
              var quantity = parseFloat(rs.quantity);
              var cost_handler = parseFloat(rs.cost_handler);
              var price = parseFloat(rs.price);
              var courier_cost = parseFloat(rs.courier_cost);
              var insurance = parseFloat(rs.insurance);
              var total = ((quantity*price)-voucher);
              var grandtotal = total+courier_cost+insurance+cost_handler;
              var vouchertemp = "Rp. "+addCommas(voucher);
              var quantitytemp = "Rp. "+addCommas(quantity);
              var cost_handlertemp = "Rp. "+addCommas(cost_handler);
              var pricetemp = "Rp. "+addCommas(price);
              var courier_costtemp = "Rp. "+addCommas(courier_cost);
              var insurancetemp = "Rp. "+addCommas(insurance);
              var totaltemp = "Rp. "+addCommas(total);
              var grandtotaltemp = "Rp. "+addCommas(grandtotal);

              if(rs.id == id){
                format += "<br>*Department: "+rs.company_name+"*";
                format += "<br>*Marketing: "+rs.author+"*";
                format += "<br>===========================";
                format += "<br>*Berikut data pengiriman pemesanan customer:*";
                format += "<br>*-Nama Pembeli:* "+rs.full_name;
                format += "<br>*-No. Telp:* "+rs.phone;
                format += "<br>*-Alamat:* "+rs.address_no;
                format += "<br>*-Rt/Rw:* "+rs.rt+"/"+rs.rw;
                format += "<br>*-Desa:* "+rs.village;
                format += "<br>*-Patokan:* "+rs.sub_district;
                format += "<br>*-Kelurahan:* "+rs.benchmark;
                format += "<br>*-Kecamatan:* "+rs.district;
                format += "<br>*-Kota:* "+rs.city_id;
                format += "<br>*-Provinsi:* "+rs.province_id;
                format += "<br>*-Kode Pos:* "+rs.postal_code;
                format += "<br>";
                format += "<br>*Berikut data Pesanan:*";
                format += "<br>*-No. Pesanan :* "+rs.id.toUpperCase();
                format += "<br>*-Jumlah Pemesanan :* "+quantity;
                format += "<br>*-Biaya Voucher :* "+vouchertemp;
                format += "<br>*-Biaya Pesanan :* "+totaltemp;
                format += "<br>*-Biaya Pengiriman :* "+courier_costtemp;
                format += "<br>*-Biaya Asuransi :* Rp "+insurancetemp;
                format += "<br>*-Biaya Penanganan :* Rp "+cost_handlertemp;
                format += "<br>*-Total Pembayaran :* Rp "+grandtotaltemp;
                format += "<br>*-Market :* "+rs.market_name;
                format += "<br>*-Bank   :* "+rs.bank_name;
                format += "<br>*-Jenis transaksi :* "+rs.payment_type_name;
                format += "<br>*-Kurir Pengiriman :* "+rs.courier_name;
                format += "<br>*-Tanggal Order :* "+formatdate(rs.transaction_date);
                format += "<br>*-No. Resi:* "+rs.delivery_no;
                format += "<br>*-No. Ref:* "+rs.delivery_refno;
                format += "<br>*-Status Pemesanan: Waiting ACC*";
                head += "<br>";
              }else{
                list += "<br>*Department: "+rs.company_name+"*";
                list += "<br>*"+(no++)+".) CS Marketing: "+rs.author+"  *";
                list += "<br>   *-No Order: "+rs.id+" ["+rs.confirm_status+"]*";
                list += "<br>   *-Tanggal : "+formatdate(rs.transaction_date);
                list += "<br>   *-Nama Pembeli:* "+rs.full_name;
                list += "<br>   *-No. Telp:* "+rs.phone;
                list += "<br>   *-Jumlah Pemesanan :* "+quantity;
                list += "<br>   *-Total Pembayaran :* Rp "+grandtotaltemp;
                list += "<br>   *-Alamat:*  "+rs.customer_address;
                list += "<br>";
              }
            }

            var options = {
              title:'CHECK SO VALIDATION'
            };

            format += head;
            format += "<br>*Berikut informasi customer:*";
            format += list;
            copyToClipboard(format);
            showDialog.show(format,options);
          },
          error: function (xhr, status, error) {
              showDialog.show(xhr.status + " " + status + " " + error, false);
          }
      });
  }

  function show_data(id = "") {
      if (id !== "") {
          $.ajax({
              url: "{{ $path }}/data/" + id,
              type: "GET",
              headers: {
                'X-CSRF-TOKEN': '{{ csrf_token() }}'
              },
              success: function (response) {

                var format = "";
                format += "*LAPORAN PEMESANAN CS "+response.data.company_name.toUpperCase()+"*";
                format += "<br>*IKLAN : "+response.data.advertise_name+"*";
                format += "<br>*Tanggal : "+formatdate(response.data.transaction_date)+"*";
                // format += "<br>*Jam : 00.00 s/d 00.00*";
                format += "<br>";
                format += "<br>*Berikut data pengiriman pemesanan customer:*";
                format += "<br>*-Nama Pembeli:* "+response.data.gender_name+" "+response.data.full_name;
                format += "<br>*-No. Telp:* "+response.data.phone;
                format += "<br>*-Alamat:* "+response.data.address_no;
                format += "<br>*-Rt/Rw:* "+response.data.rt+"/"+response.data.rw;
                format += "<br>*-Desa:* "+response.data.village;
                format += "<br>*-Patokan:* "+response.data.sub_district;
                format += "<br>*-Kelurahan:* "+response.data.benchmark;
                format += "<br>*-Kecamatan:* "+response.data.district;
                format += "<br>*-Kota:* "+response.data.city_id;
                format += "<br>*-Provinsi:* "+response.data.province_id;
                format += "<br>*-Kode Pos:* "+response.data.postal_code;
                format += "<br>";

                var voucher = parseFloat(response.data.voucher_total);
                var quantity = parseFloat(response.data.quantity);
                var cost_handler = parseFloat(response.data.cost_handler);
                var price = parseFloat(response.data.price);
                var courier_cost = parseFloat(response.data.courier_cost);
                var insurance = parseFloat(response.data.insurance);
                var total = ((quantity*price)-voucher);
                var grandtotal = total+courier_cost+insurance+cost_handler;
                var vouchertemp = "Rp. "+addCommas(voucher);
                var quantitytemp = "Rp. "+addCommas(quantity);
                var cost_handlertemp = "Rp. "+addCommas(cost_handler);
                var pricetemp = "Rp. "+addCommas(price);
                var courier_costtemp = "Rp. "+addCommas(courier_cost);
                var insurancetemp = "Rp. "+addCommas(insurance);
                var totaltemp = "Rp. "+addCommas(total);
                var grandtotaltemp = "Rp. "+addCommas(grandtotal);

                format += "<br>*Berikut data Pesanan:*";
                format += "<br>*-No. Pesanan :* "+response.data.id.toUpperCase();
                format += "<br>*-Jumlah Pemesanan :* "+quantity;
                format += "<br>*-Biaya Voucher :* "+vouchertemp;
                format += "<br>*-Biaya Pesanan :* "+totaltemp;
                format += "<br>*-Biaya Pengiriman :* "+courier_costtemp;
                format += "<br>*-Biaya Asuransi :* Rp "+insurancetemp;
                format += "<br>*-Biaya Penanganan :* Rp "+cost_handlertemp;
                format += "<br>*-Total Pembayaran :* Rp "+grandtotaltemp;
                format += "<br>*-Market :* "+response.data.market_name;
                format += "<br>*-Bank   :* "+response.data.bank_name;
                format += "<br>*-Jenis transaksi :* "+response.data.payment_type_name;
                format += "<br>*-Kurir Pengiriman :* "+response.data.courier_name;
                format += "<br>*-Tanggal Order :* "+formatdate(response.data.transaction_date);
                format += "<br>*-No. Resi:* "+response.data.delivery_no;
                format += "<br>*-No. Ref:* "+response.data.delivery_refno;
                format += "<br>*-Status Pemesanan: Waiting*";
                format += "<br>";
                format += "<br>*ACC*";
                format += "<br>*CS "+response.data.company_name+"* ✅";
                format += "<br>*Nama : "+response.data.author+"*";
                format += "<br>";
                format += "<br>*Admin*";
                format += "<br>*Nama :{{sess_user('name')}}*";

                copyToClipboard(format);
                var options = {
                  title:'Laporan Kerja Harian',
                  form_url:'{{ $path }}/update/'+response.data.id,
                  form_csrf:'{!! csrf_field() !!}'
                };
                showDialog.show(format,options);
              },
              error: function (xhr, status, error) {
                  showDialog.show(xhr.status + " " + status + " " + error, false);
              }
          });
      }
  }

  function refresh_table() {
      $('#datatable').DataTable().ajax.reload();
      $('#datatable').DataTable().responsive.recalc();
  }
</script>
@endsection
