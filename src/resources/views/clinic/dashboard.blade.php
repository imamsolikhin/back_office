@extends('layout.default')

@section('content')
    <div class="subheader py-2 {{ Metronic::printClasses('subheader', false) }}" id="kt_subheader">
        <div class="{{ Metronic::printClasses('subheader-container', false) }} d-flex align-items-center justify-content-between flex-wrap flex-sm-nowrap">
            <div class="d-flex align-items-baseline flex-wrap mr-5">
                <h5 class="text-dark font-weight-bold my-1 mr-5">
                    {{$module}}
                </h5>
            </div>
        </div>
    </div>
    <div class="row">
      <div class="col-lg-6 col-xxl-4">
        <!--begin::Engage Widget 15-->
        <div class="card card-custom body-container">
          <div class="card-body rounded p-0 d-flex bg-light">
            <div class="col-lg-12 py-5 py-md-1 px-1 px-md-5 pr-lg-0">
              <!-- <h1 class="font-weight-bolder text-dark mb-0">Search Goods</h1> -->
              <div class="font-size-h4 mb-0">Show data current date</div>
              <!--begin::Form-->
              <form class="col-lg-11 d-flex flex-center py-2 px-6 bg-white rounded">
                <input type="date" class="form-control border-0 font-weight-bold pl-2" placeholder="Search Goods" name="dashboard_date" value="{{$dashboard_date}}">
                <input type="submit" class="btn btn-success btn-sm" value="show">
              </form>
              <!--end::Form-->
            </div>
            <div class="d-none d-md-flex flex-row-fluid bgi-no-repeat bgi-position-y-center bgi-position-x-left bgi-size-cover" ></div>
          </div>
        </div>
        <!--end::Engage Widget 15-->
      </div>
      <div class="col-lg-6 col-xxl-4">
        <div class="row col-12 mx-1">
    			<div class="col-lg-3 col-sm-3 col-3">
    				<!--begin::Stats Widget 29-->
    				<div class="card card-custom bg-success card-stretch gutter-b">
    					<!--begin::Body-->
    					<div class="card-body m-2 p-1">
    						<span class="card-title font-weight-bolder text-white font-size-h2 mb-0 mt-0 d-block">{{$data_baru['jml']}}</span>
    						<span class="font-weight-bold text-white font-size-sm">Pasien {{$data_baru['name']}}</span>
    					</div>
    					<!--end::Body-->
    				</div>
    				<!--end::Stats Widget 29-->
    			</div>
    			<div class="col-lg-3 col-sm-3 col-3">
    				<!--begin::Stats Widget 30-->
    				<div class="card card-custom bg-warning card-stretch gutter-b">
    					<!--begin::Body-->
    					<div class="card-body m-2 p-1">
    						<span class="card-title font-weight-bolder text-white font-size-h2 mb-0 mt-0 d-block">{{$data_ulang['jml']}}</span>
    						<span class="font-weight-bold text-white font-size-sm">Pasien {{$data_ulang['name']}}</span>
    					</div>
    					<!--end::Body-->
    				</div>
    				<!--end::Stats Widget 30-->
    			</div>
    			<div class="col-lg-3 col-sm-3 col-3">
    				<!--begin::Stats Widget 31-->
    				<div class="card card-custom bg-info card-stretch gutter-b">
    					<!--begin::Body-->
    					<div class="card-body m-2 p-1">
    						<span class="card-title font-weight-bolder text-white font-size-h2 mb-0 mt-0 d-block">{{$data_lunas['jml']}}</span>
    						<span class="font-weight-bold text-white font-size-sm">Pasien {{$data_lunas['name']}}</span>
    					</div>
    					<!--end::Body-->
    				</div>
    				<!--end::Stats Widget 31-->
    			</div>
    			<div class="col-lg-3 col-sm-3 col-3">
    				<!--begin::Stats Widget 32-->
    				<div class="card card-custom bg-danger card-stretch gutter-b">
    					<!--begin::Body-->
    					<div class="card-body m-2 p-1">
    						<span class="card-title font-weight-bolder text-white font-size-h2 mb-0 mt-0 text-hover-primary d-block">{{$data_lolos['jml']}}</span>
    						<span class="font-weight-bold text-white font-size-sm">Pasien {{$data_lolos['name']}}</span>
    					</div>
    					<!--end::Body-->
    				</div>
    				<!--end::Stats Widget 32-->
    			</div>
    		</div>
  		</div>
		</div>
    <div class="row">
        <div class="col-lg-6 col-xxl-4">
          <div class="card card-custom {{ @$class }}">
              <div class="card-header align-items-center border-0 mt-4">
                  <h3 class="card-title align-items-start flex-column">
                      <span class="font-weight-bolder text-dark">Kunjungan Hari Ini</span>
                      <!-- <span class="text-muted mt-3 font-weight-bold font-size-sm">890,344 Sales</span> -->
                  </h3>
              </div>
              <div class="card-body pt-4">
                  <div class="timeline timeline-5 mt-3">
                        @if($list_kunjungan)
                          @foreach($list_kunjungan as $data)
                          <div class="timeline-item align-items-start">
                            <div class="timeline-label font-weight-bolder text-dark-75 font-size-lg text-right pr-3">{{date('H:i', strtotime($data->consultation_date))}}</div>
                            @if($data->register_status=='Baru')
                              <div class="timeline-badge"><i class="fa fa-genderless text-success icon-xxl"></i></div>
                            @elseif($data->register_status=='Lunas')
                              <div class="timeline-badge"><i class="fa fa-genderless text-info icon-xxl"></i></div>
                            @elseif($data->register_status=='Ulang')
                              <div class="timeline-badge"><i class="fa fa-genderless text-warning icon-xxl"></i></div>
                            @else
                              <div class="timeline-badge"><i class="fa fa-genderless text-danger icon-xxl"></i></div>
                            @endif
                            <div class="timeline-content d-flex">
                                <span class="mr-4 font-weight-bolder text-dark-75">{{$data->register_status}}</span>
                                <div class="timeline-content text-dark-50">{{$data->name}}</div>
                                <div class="d-flex align-items-start mt-n2">
                                    <a href="#" class="symbol symbol-35 symbol-light-success mr-2">
                                        <span class="symbol-label">
                                            <img src="{{ asset($data->img_url) }}" class="h-75 align-self-end"/>
                                        </span>
                                    </a>
                                </div>
                            </div>
                          </div>
                        @endforeach
                      @endif
                      <div class="timeline-item align-items-start">
                          <div class="timeline-label font-weight-bolder text-dark-75 font-size-lg text-right pr-3">{{date('D')}}</div>
                          <div class="timeline-badge">
                              <i class="fa fa-genderless text-primary icon-xxl"></i>
                          </div>
                          <div class="timeline-content text-dark-50">
                              Jadwal Pasien Hari ini yang sudah berkunjung.
                          </div>
                      </div>
                  </div>
              </div>
          </div>
        </div>
        <div class="col-lg-6 col-xxl-4">
          <div class="card card-custom {{ @$class }}">
              <div class="card-header align-items-center border-0 mt-4">
                  <h3 class="card-title align-items-start flex-column">
                      <span class="font-weight-bolder text-dark">Jadwal Hari Ini</span>
                      <!-- <span class="text-muted mt-3 font-weight-bold font-size-sm">890,344 Sales</span> -->
                  </h3>
              </div>
              <div class="card-body pt-4">
                  <div class="timeline timeline-5 mt-3">
                        @if($list_jadwal)
                          @foreach($list_jadwal as $data)
                          <div class="timeline-item align-items-start">
                            @if($data->reservationId)
                              <div class="timeline-label text-success font-weight-bolder text-dark-75 font-size-lg text-right pr-3">{{date('H:i', strtotime($data->schedule_date))}}</div>
                              <div class="timeline-badge"><i class="fa fa-genderless text-success icon-xxl"></i></div>
                            @else
                              <div class="timeline-label text-warning font-weight-bolder text-dark-75 font-size-lg text-right pr-3">{{date('H:i', strtotime($data->schedule_date))}}</div>
                              <div class="timeline-badge"><i class="fa fa-genderless text-warning icon-xxl"></i></div>
                            @endif
                            <div class="timeline-content d-flex">
                                <span class="mr-4 font-weight-bolder text-dark-75">{{$data->full_name}}</span>
                                <div class="timeline-content text-dark-50">RESV: {{$data->id}}<br>{{$data->consultation}}</div>
                                <div class="d-flex align-items-start mt-n2">
                                    <a href="#" class="symbol symbol-35 symbol-light-success mr-2">
                                        <span class="symbol-label">
                                            <img src="{{ asset($data->img_reservation) }}" class="h-75 align-self-end"/>
                                        </span>
                                    </a>
                                </div>
                            </div>
                          </div>
                        @endforeach
                      @endif
                      <div class="timeline-item align-items-start">
                          <div class="timeline-label font-weight-bolder text-dark-75 font-size-lg text-right pr-3">{{date('D')}}</div>
                          <div class="timeline-badge">
                              <i class="fa fa-genderless text-primary icon-xxl"></i>
                          </div>
                          <div class="timeline-content text-dark-50">
                              Jadwal Pasien Hari ini yang akan berkunjung.
                          </div>
                      </div>
                  </div>
              </div>
          </div>
        </div>

    </div>

@endsection

{{-- Scripts Section --}}
@section('scripts')
    <script src="{{ asset('js/pages/widgets.js') }}" type="text/javascript"></script>
@endsection
