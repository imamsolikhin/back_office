@extends('layout.default')

@section('content')
<div class="subheader py-2 {{ Metronic::printClasses('subheader', false) }}" id="kt_subheader">
  <div class="{{ Metronic::printClasses('subheader-container', false) }} d-flex align-items-center justify-content-between flex-wrap flex-sm-nowrap">
    <div class="d-flex align-items-baseline flex-wrap mr-5">
      <h5 class="text-dark font-weight-bold my-1 mr-5">
        {{ $module }}
      </h5>
      <ul class="breadcrumb breadcrumb-transparent breadcrumb-dot font-weight-bold p-0 my-2 font-size-sm">
        <li class="breadcrumb-item">
          <a href="{{ route('dashboard') }}" class="text-muted">Dashboard</a>
        </li>
        <li class="breadcrumb-item">
          <a href="{{ $auth }}" class="text-muted">{{ $module }}</a>
        </li>
        <li class="breadcrumb-item">
          <a href="#view" class="text-muted">View</a>
        </li>
      </ul>
    </div>
  </div>
</div>

@include('inc.error-list')
@include('inc.success-notif')
@include('inc.danger-notif')
<div class="card card-custom body-container">
  <div class="card-header bg-danger flex-wrap border-1 pt-1 pb-0 mb-2" style="min-height: 0;">
    <div class="card-title pt-1 pb-1">
      <h3 class="card-label font-weight-bolder text-white">{{ $module }}
        <div class="text-muted pt-2 font-size-lg">show Datatable from table {{ $module }}</div>
      </h3>
    </div>
    <div class="card-toolbar pt-1 pb-0">
    </div>
  </div>
  <div class="card-body pt-1">
    <div hidden><input id="start-date"/><input id="end-date"/></div>
    <table class="table table-bordered table-hover w100" cellspacing="0" id="datatable" style="width: 1070px !important;"></table>
  </div>
</div>


<!-- Modal-->
<div class="modal fade" id="modal-form" data-backdrop="static" tabindex="-1" role="dialog" aria-labelledby="staticBackdrop" aria-hidden="true">
    <div class="modal-dialog modal-md" role="document">
        <div class="modal-content">
            <div class="modal-header bg-danger pt-3 pb-3">
                <h5 class="modal-title text-white bold" id="modal">Form {{ $module }}</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <i aria-hidden="true" class="ki ki-close"></i>
                </button>
            </div>
            <form class="form" id="form-input" action="{{ $path }}" method="POST" enctype="multipart/form-data">
              {!! csrf_field() !!}
              <input type="hidden" class="form-control" id="method" name="_method" placeholder="Enter method" value="POST"/>
              <div class="card-body pt-3">
                <div class="mb-2">
                    <div class="form-group row">
                        <div class="col-lg-4">
                          <center>
                            <div class="float-right col-12 symbol symbol-100 symbol-xxl-400 align-self-start align-self-xxl-center">
                              <div id="img_show_form" class="symbol-label" style="background-image:url('')"></div>
                            </div>
                          </center>
                          <input type="text" class="form-control" id="id" name="id" placeholder="Enter NO" value="" readonly required/>
                        </div>
                        <div class="col-lg-8">
                            <div class="col-lg-4 mr-0 ml-0 pr-1 pl-0 float-left">
                              Description:
                            </div>
                            <div class="col-lg-8 mr-0 ml-0 pr-0 pl-0 float-right">
                              <div id="show_description"/>
                            </div>
                        </div>
                    </div>
                  </div>
                  <div class="mb-2">
                      <div class="form-group row">
                          <label class="col-lg-4 col-form-label">Pilih Obat</label>
                          <div class="col-lg-8">
                              <div class="col-lg-12 mr-0 ml-0 pr-1 pl-0 float-left">
                                <select class="form-control select2" id="recept" name="recept" style="width: 100%;">
                                  <option value="" selected>Chose</option>
                                  @if($reservasi)
                                   @foreach($reservasi as $data)
                                    <option value="{{ $data->id }}" title="{{ $data->name }}">{{ $data->id }}</option>
                                   @endforeach
                                  @endif
                               </select>
                              </div>
                          </div>
                      </div>
                  </div>
                  <div class="mb-2">
                      <div class="form-group row">
                          <div class="card-body pt-1">
                            <table class="table table-bordered table-hover w100" cellspacing="0" id="datatable_resept">
                              <thead>
                                <th style="padding-top: 0.5rem; padding-bottom: 0.5rem;">Obat</th>
                                <th style="padding-top: 0.5rem; padding-bottom: 0.5rem;">Jumlah</th>
                                <th style="padding-top: 0.5rem; padding-bottom: 0.5rem;">Cara Konsumsi</th>
                                <th style="padding-top: 0.5rem; padding-bottom: 0.5rem;">Action</th>
                              </thead>
                              <tbody>
                              </tbody>
                            </table>
                          </div>
                      </div>
                  </div>
                  <div class="mb-2">
                      <div class="form-group row">
                          <label class="col-lg-4 col-form-label"></label>
                          <div class="col-lg-8">
                              <button type="submit" class="btn btn-success font-weight-bold">Save</button>
                              <button type="button" data-dismiss="modal" class="btn btn-danger">Cancel</button>
                          </div>
                      </div>
                  </div>
              </div>
            </form>
        </div>
    </div>
</div>
@endsection

{{-- Styles Section --}}
@section('styles')
<link rel="stylesheet" href="{{ config('app.url') }}global/vendor/datatables-bootstrap/dataTables.bootstrap.css">
<link rel="stylesheet" href="{{ config('app.url') }}global/vendor/datatables-responsive/dataTables.responsive.min.css">
<link rel="stylesheet" href="{{ config('app.url') }}plugins/custom/datatables/datatables.bundle.css">
<link rel="stylesheet" href="{{ config('app.url') }}css/inject.css">
@endsection

{{-- Scripts Section --}}
@section('scripts')
@include ('inc.confirm-delete-modal')
<script src="{{ config('app.url') }}global/vendor/datatables/jquery.dataTables.js"></script>
<script src="{{ config('app.url') }}global/vendor/datatables-bootstrap/dataTables.bootstrap.js"></script>
<script src="{{ config('app.url') }}global/vendor/datatables-responsive/dataTables.responsive.js"></script>
<script src="{{ config('app.url') }}js/inject.js"></script>
<script type="text/javascript">
  var start_date = "";
  var end_date = "";
  $(document).ready(function() {
    $('.select2').select2({templateResult: formatOption});
    $('div.datetimepicker').css('top',0);
    $('.datetime-input').datepicker({
        format: 'dd-mm-yyyy',
        inline: true,
    });
    $('.datetimepicker-input').datetimepicker({
        format: 'dd-mm-yyyy hh:ii',
        inline: true,
    });
    $('[data-switch=true]').bootstrapSwitch('state', true);

    $("div.datesearchbox").html('<div class="input-group"> <div class="input-group-addon"> <i class="glyphicon glyphicon-calendar"></i> </div><input type="text" class="form-control pull-right" id="datesearch" placeholder="Search by date range"> </div>');
    document.getElementsByClassName("datesearchbox")[0].style.textAlign = "center";
    $("#datesearch").attr("readonly",true);
    $('#datesearch').daterangepicker({
       autoUpdateInput: false
     });
     //menangani proses saat apply date range
      $('#datesearch').on('apply.daterangepicker', function(ev, picker) {
         $(this).val(picker.startDate.format('DD-MM-YYYY') + ' - ' + picker.endDate.format('DD-MM-YYYY'));
         $("#start-date").val(picker.startDate.format('YYYY-MM-DD'));
         $("#end-date").val(picker.endDate.format('YYYY-MM-DD'));
         refresh_table();
      });

      $('#datesearch').on('cancel.daterangepicker', function(ev, picker) {
        $(this).val('');
        $("#start-date").val('');
        $("#end-date").val('');
        refresh_table();
      });
  });
  $("#img_url").change(function() {
    bg_readURL(this,'img_show_form');
  });

  $('[data-switch=true]').bootstrapSwitch();
  var table = $('#datatable').dataTable({
    pageLength: 5,
    responsive: true,
    searchDelay: 800,
    processing: true,
    serverSide: true,
    searching: true,
    lengthMenu: [[5, 10, 25, 50, 100, 200, -1], [5, 10, 25, 50, 100, 200, "All"]],
    ajax: {
      method: 'POST',
      url : '{{ $path }}/list',
      headers: {
        'X-CSRF-TOKEN': '{{ csrf_token() }}'
      },
      data: function (d) {
        d.from_date = $("#start-date").val();
        d.to_date = $("#end-date").val();
      }
    },
    columns: [
      {title: "No", data: 'DT_RowIndex', orderable: false, searchable: false, autoHide: false},
      {title: "Foto Pasien", data: 'reservation_desc', defaultContent: '-', class: 'text-center dt-body-nowrap', autohide: true},
      {title: "Reservasi ID", data: 'id', defaultContent: '-', class: 'text-center dt-body-nowrap', autohide: true},
      {title: "Nama Pasien", data: 'name', defaultContent: '-', class: 'text-center dt-body-nowrap', autohide: true},
      {title: "No Telp", data: 'phone', defaultContent: '-', class: 'text-center dt-body-nowrap', autohide: true},
      {title: "Email", data: 'email', defaultContent: '-', class: 'text-center dt-body-nowrap', autohide: true},
      {title: "Keluhan", data: 'consultation', defaultContent: '-', class: 'text-center dt-body-nowrap', autohide: true},
      {title: "Actions", data: 'action', orderable: false, autohide: true},
    ],
    order: [[1, 'asc']],
    bStateSave: true,
    dom:  "<'row'<'col-sm-4'l><'col-sm-5' <'datesearchbox'>><'col-sm-3'f>>" +
          "<'row'<'col-sm-12'tr>>" +
          "<'row'<'col-sm-5'i><'col-sm-7'p>>",
    columnDefs: [
      {
        targets: [0,-1],
        className: 'text-center visible dt-body-nowrap'
      },
      'colvis'
    ]
  });
  $('#btn_tools > li > a.tool-action').on('click', function () {
      var action = $(this).attr('data-action');
      table.dataTable().button(action).trigger();
  });

  function show_laporan(id = "") {
      $.ajax({
          url: "{{ $path }}/data/" + id,
          type: "GET",
          headers: {
            'X-CSRF-TOKEN': '{{ csrf_token() }}'
          },
          success: function (response) {
            if(!response){
              showDialog.show("data "+id+" not found");
              return;
            }
            $('#form-input').trigger("reset");
            $("#form-input").attr("action", "{{ $path }}/update/"+id);
            $('#id').attr('readonly',true);
            var format = '';
            format += '*Laporan: ' +formatdate(response.data.consultation_date)+'*';
            format += '<br> *Pendaftaran: Pasien '+response.data.register_status+'*';
            format += '<br> *Nama Pasien:* '+response.data.gender_name+' '+response.data.name;
            format += '<br> *No Telp:* '+response.data.phone;
            format += '<br> *Email:* '+response.data.email;
            format += '<br> *Alamat:* <br>  '+response.data.address.replace(/(?:\r\n|\r|\n)/g, '<br>  ');
            format += '<br> *Konsultasi:* <br>  '+response.data.consultation.replace(/(?:\r\n|\r|\n)/g, '<br>  ');
            format += '<br> *Resep:* <br>  '+response.data.note.replace(/(?:\r\n|\r|\n)/g, '<br>  ');
            format += '<br> *Reservasi ID:* '+response.data.id;
            format += '<br> *Marketing:* '+response.data.sales_name;
            copyToClipboard(format);

            var img_url = '{{ENV('APP_URL')}}'+response.data.img_url;
            var options = {
              title:'Laporan: '+formatdate(response.data.consultation_date)+'<br> Pasien '+response.data.register_status +':'+response.data.name,
              imgUrl:img_url,
            };
            showDialog.show(format,options);
          },
          error: function (xhr, status, error) {
              showDialog.show(xhr.status + " " + status + " " + error, false);
          }
      });
  }


  function show_data(id = "") {
      if (id !== "") {
          $.ajax({
              url: "{{ $path }}/data/" + id,
              type: "GET",
              headers: {
                'X-CSRF-TOKEN': '{{ csrf_token() }}'
              },
              success: function (response) {
                if(!response){
                  showDialog.show("data "+id+" not found");
                }
                  $('#form-input').trigger("reset");
                  $("#form-input").attr("action", "{{ $path }}/update/"+id);
                  var img_url = '{{ENV('APP_URL')}}'+response.data.img_url;
                  $('#img_show_form').css("background-image", "url('" +img_url+ "')");

                  var format = '';
                  format += 'Laporan: ' +formatdate(response.data.consultation_date)+'';
                  format += '<br> Pendaftaran: Pasien '+response.data.register_status+'';
                  format += '<br> Nama Pasien: '+response.data.gender_name+' '+response.data.name;
                  format += '<br> No Telp: '+response.data.phone;
                  format += '<br> Email: '+response.data.email;
                  format += '<br> Alamat: <br>  '+response.data.address.replace(/(?:\r\n|\r|\n)/g, '<br>  ');
                  format += '<br> Konsultasi: <br>  '+response.data.consultation.replace(/(?:\r\n|\r|\n)/g, '<br>  ');
                  $('#show_description').html(format);
                  $('#id').val(response.data.id);
                  $('#modal-form').modal('show');
              },
              error: function (xhr, status, error) {
                  showDialog.show(xhr.status + " " + status + " " + error, false);
              }
          });
      } else {
        form_reset();
      }
  }

  $('#recept').on( 'change', function () {
      if (this.value !== "") {
          $.ajax({
              url: "{{ $path }}/data/" + this.value,
              type: "GET",
              headers: {
                'X-CSRF-TOKEN': '{{ csrf_token() }}'
              },
              success: function (response) {
                if(!response){
                  showDialog.show("data "+id+" not found");
                }
                  $('#form-input').trigger("reset");
                  var img_url = '{{ENV('APP_URL')}}'+response.data.img_url;
                  $('#img_show_form').css("background-image", "url('" +img_url+ "')");

                    // <center>
                    //   <div class="float-right col-12 symbol symbol-100 symbol-xxl-400 align-self-start align-self-xxl-center">
                    //     <div id="img_show_form" class="symbol-label" style="background-image:url('')"></div>
                    //   </div>
                    // </center>
                  var table = document.getElementById("datatable_resept").getElementsByTagName('tbody')[0];
                  var row = table.insertRow(-1);
                  row.insertCell(0).innerHTML = '<label>'+response.data.name+'</label><input type="hidden" class="form-input col-12" name="id[]" value="'+response.data.id+'" readonly hidden/>';
                  row.insertCell(1).innerHTML = '<input type="number" class="form-input col-12" name="total[]" value=""/>';
                  row.insertCell(2).innerHTML = '<textarea type="text" class="form-input col-12" name="note[]" value=""></textarea>';
                  row.insertCell(3).innerHTML = '<a type="button" value="del" onclick="deleteRow(this)" class="pt-2"><i class="text-center text-danger fas fa-backspace"></i><a/>';
              },
              error: function (xhr, status, error) {
                  showDialog.show(xhr.status + " " + status + " " + error, false);
              }
          });
      }
  });

  function form_reset(){
      $('#form-input').trigger("reset");
      $("#form-input").attr("action", "{{ $path }}/save");
      $("#id").removeAttr('disabled');

      $('#gender_id').val('').trigger('change');
      $('#address, #consultation, #note').text('');
      $('#is_consultation, #is_order, #is_therapy').prop('checked', false);
      $('.btn-group > .btn').removeClass('active');
      $('.btn-group > .rdb-Baru').addClass('active');
      $("input[name=register_status][value=Baru]").prop('checked', true);
      $('#birthdate').datepicker('setDate', "{{date('d-m-Y',strtotime('1994-06-15'))}}");
      $('#consultation_date').datetimepicker('setDate', new Date());
      $('#modal-form').modal('show');
  }

  function deleteRow(btn) {
    var row = btn.parentNode.parentNode;
    row.parentNode.removeChild(row);
  }

  function refresh_table() {
      table.fnDraw();
  }

  function formatOption (option) {
    var $option = $(
      '<div><strong>' + option.text + '</strong></div><div>' + option.title + '</div>'
    );
    return $option;
  };
</script>
@endsection
