@extends('layout.default')

@section('content')
<div class="subheader py-2 {{ Metronic::printClasses('subheader', false) }}" id="kt_subheader">
  <div class="{{ Metronic::printClasses('subheader-container', false) }} d-flex align-items-center justify-content-between flex-wrap flex-sm-nowrap">
    <div class="d-flex align-items-baseline flex-wrap mr-5">
      <h5 class="text-dark font-weight-bold my-1 mr-5">
        {{ $module }}
      </h5>
      <ul class="breadcrumb breadcrumb-transparent breadcrumb-dot font-weight-bold p-0 my-2 font-size-sm">
        <li class="breadcrumb-item">
          <a href="{{ route('dashboard') }}" class="text-muted">Dashboard</a>
        </li>
        <li class="breadcrumb-item">
          <a href="{{ $auth }}" class="text-muted">{{ $module }}</a>
        </li>
        <li class="breadcrumb-item">
          <a href="#view" class="text-muted">View</a>
        </li>
      </ul>
    </div>
  </div>
</div>

@include('inc.error-list')
@include('inc.success-notif')
@include('inc.danger-notif')
<div class="card card-custom body-container">
  <div class="card-header bg-danger flex-wrap border-1 pt-1 pb-0 mb-2" style="min-height: 0;">
    <div class="card-title pt-1 pb-1">
      <h3 class="card-label font-weight-bolder text-white">{{ $module }}
        <div class="text-muted pt-2 font-size-lg">show Datatable from table {{ $module }}</div>
      </h3>
    </div>
    <div class="card-toolbar pt-1 pb-0">
      <a href="#" onclick="show_data('')" class="btn btn-primary font-weight-bolder" style="background-color: #1e1e2d;border-color: #0c8eff;">
        <span class="svg-icon svg-icon-md">
          <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
            <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
              <rect x="0" y="0" width="24" height="24"></rect>
              <circle fill="#000000" cx="9" cy="15" r="6"></circle>
              <path d="M8.8012943,7.00241953 C9.83837775,5.20768121 11.7781543,4 14,4 C17.3137085,4 20,6.6862915 20,10 C20,12.2218457 18.7923188,14.1616223 16.9975805,15.1987057 C16.9991904,15.1326658 17,15.0664274 17,15 C17,10.581722 13.418278,7 9,7 C8.93357256,7 8.86733422,7.00080962 8.8012943,7.00241953 Z" fill="#000000" opacity="0.3"></path>
            </g>
          </svg>
        </span>Add New
      </a>
    </div>
  </div>
  <div class="card-body pt-1">
    <div hidden><input id="start-date"/><input id="end-date"/></div>
    <table class="table table-bordered table-hover w100" cellspacing="0" id="datatable" style="width: 1070px !important;"></table>
  </div>
</div>


<!-- Modal-->
<div class="modal fade" id="modal-form" data-backdrop="static" tabindex="-1" role="dialog" aria-labelledby="staticBackdrop" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header bg-danger pt-3 pb-3">
                <h5 class="modal-title text-white bold" id="modal">Form {{ $module }}</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <i aria-hidden="true" class="ki ki-close"></i>
                </button>
            </div>
            <form class="form" id="form-input" action="{{ $path }}" method="POST" enctype="multipart/form-data">
              {!! csrf_field() !!}
              <input type="hidden" class="form-control" id="method" name="_method" placeholder="Enter method" value="POST"/>
              <div class="card-body pt-3">
                  <div class="mb-1">
                      <div class="form-group row">
                          <label class="col-lg-4 col-form-label">Upload Foto Pasien</label>
                          <div class="col-lg-8">
                            <div class="float-left col-4"><input type="file" id="img_url" name="img_url" multiple></div>
                            <div class="float-right col-8 symbol symbol-100 symbol-xxl-400 mr-5 align-self-start align-self-xxl-center">
                              <div id="img_show_form" class="symbol-label" style="background-image:url('')"></div>
                              <i class="symbol-badge bg-success"></i>
                            </div>
                          </div>
                      </div>
                  </div>
                  <div class="mb-2">
                      <div class="form-group row">
                          <label class="col-lg-4 col-form-label">Rservasi ID</label>
                          <div class="col-lg-8">
                              <div class="col-lg-4 mr-0 ml-0 pr-1 pl-0 float-left">
                                <select class="form-control select2" id="id" name="id" style="width: 100%;">
                                  <option value="" selected>Pasien Baru</option>
                                  @if($reservasi)
                                   @foreach($reservasi as $data)
                                    <option value="{{ $data->id }}" title="{{ $data->name }}">{{ $loop->index+1 }}.) {{ $data->id }}</option>
                                   @endforeach
                                  @endif
                               </select>
                               <input type="text" class="form-control" id="reservation_id" name="reservation_id" placeholder="Enter Reservastion ID" value="" readonly hidden/>
                              </div>
                              <div class="col-lg-8 mr-0 ml-0 pr-0 pl-0 float-right">
                                  <input type="text" class="form-control" id="reservation_name" name="reservation_name" placeholder="Enter Reservastion Name" value="" readonly/>
                              </div>
                          </div>
                      </div>
                  </div>
                  <div class="mb-2">
                      <div class="form-group row">
                          <label class="col-lg-4 col-form-label">Nama Pasien</label>
                          <div class="col-lg-8">
                              <div class="col-lg-4 mr-0 ml-0 pr-1 pl-0 float-left">
                                <select class="form-control select2" id="gender_id" name="gender_id" style="width: 100%;" required>
                                  <option value="" selected>Chose </option>
                                   @if(list_model('Master','Gender'))
                                     @foreach(list_model('Master','Gender') as $data)
                                      <option value="{{ $data->id }}">{{ $data->name }}</option>
                                     @endforeach
                                   @endif
                               </select>
                              </div>
                              <div class="col-lg-8 mr-0 ml-0 pr-0 pl-0 float-right">
                                  <input type="text" class="form-control" id="name" name="name" placeholder="Enter Name" value="" required/>
                              </div>
                          </div>
                      </div>
                  </div>
                  <div class="mb-2">
                      <div class="form-group row">
                          <label class="col-lg-4 col-form-label">Tanggal Lahir</label>
                          <div class="col-lg-8">
                              <div class="input-icon">
                                  <input type="input" class="form-control datetime-input" placeholder="dd/mm/yyyy" id="birthdate" name="birthdate" data-date-format="dd-mm-yyyy" value="{{date('d-m-Y')}}" readonly required/>
                                  <span>
                                      <i class="far fa-calendar-alt text-muted"></i>
                                  </span>
                              </div>
                          </div>
                      </div>
                  </div>
                  <div class="mb-1">
                      <div class="form-group row">
                          <label class="col-lg-4 col-form-label">No Telp</label>
                          <div class="col-lg-8">
                              <input type="tel" class="form-control" id="phone" name="phone" placeholder="Enter No Telp" value=""/>
                          </div>
                      </div>
                  </div>
                  <div class="mb-1">
                      <div class="form-group row">
                          <label class="col-lg-4 col-form-label">Email</label>
                          <div class="col-lg-8">
                              <input type="text" class="form-control" id="email" name="email" placeholder="Enter Email" value="-"/>
                          </div>
                      </div>
                  </div>
                  <div class="mb-2">
                      <div class="form-group row">
                          <label class="col-lg-4 col-form-label">Keluhan</label>
                          <div class="col-lg-8">
                              <textarea type="text" class="form-control" id="consultation" name="consultation" placeholder="Enter Keluhan" value="" rows="4" required></textarea>
                          </div>
                      </div>
                  </div>
                  <div class="mb-1">
                      <div class="form-group row">
                          <label class="col-lg-4 col-form-label">Kota</label>
                          <div class="col-lg-8">
                              <input type="text" class="form-control" id="city_id" name="city_id" placeholder="Enter Kota" value="" required/>
                          </div>
                      </div>
                  </div>
                  <div class="mb-2">
                      <div class="form-group row">
                          <label class="col-lg-4 col-form-label">Alamat</label>
                          <div class="col-lg-8">
                              <textarea type="text" class="form-control" id="address" name="address" placeholder="Enter Alamat" value="" rows="4" required></textarea>
                          </div>
                      </div>
                  </div>
                  <div class="mb-2">
                      <div class="form-group row">
                          <style>
                          .btn.btn-success:not(:disabled):not(.disabled).active {
                            color: black;
                            border-radius: 5px;
                            font-weight: 900;
                          }
                          .btn.btn-warning:not(:disabled):not(.disabled).active {
                            color: black;
                            border-radius: 5px;
                            font-weight: 900;
                          }
                          .btn.btn-info:not(:disabled):not(.disabled).active {
                            color: black;
                            border-radius: 5px;
                            font-weight: 900;
                          }
                          .btn.btn-danger:not(:disabled):not(.disabled).active {
                            color: black;
                            border-radius: 5px;
                            font-weight: 900;
                          }

                          </style>
                          <label class="col-lg-4 col-form-label">Pendaftaran</label>
                          <div class="col-lg-8">
                            <div class="btn-group btn-group-toggle" data-toggle="buttons">
                              <label class="btn btn-success rdb-Baru active">
                                <input type="radio" name="register_status" value="Baru" checked> Baru
                              </label>
                              <label class="btn btn-warning rdb-Ulang">
                                <input type="radio" name="register_status" value="Ulang"> Ulang
                              </label>
                              <label class="btn btn-info rdb-Lunas">
                                <input type="radio" name="register_status" value="Lunas"> Lunas
                              </label>
                              <label class="btn btn-danger rdb-Lolos">
                                <input type="radio" name="register_status" value="Lolos"> Lolos
                              </label>
                            </div>
                          </div>
                      </div>
                  </div>
                  <div class="mb-2">
                      <div class="form-group row">
                          <label class="col-lg-4 col-form-label">Konsultasi Pasien</label>
                          <div class="col-lg-8">
                            <div class="form-check form-check-inline">
                              <input class="form-check-input" type="checkbox" name="is_consultation" id="is_consultation" value="1">
                              <label class="form-check-label" for="is_consultation">1. Konsultasi</label>
                            </div>
                            <div class="form-check form-check-inline">
                              <input class="form-check-input" type="checkbox" name="is_order" id="is_order" value="1">
                              <label class="form-check-label" for="is_order">2. Beli Obat/Produk</label>
                            </div>
                            <div class="form-check form-check-inline">
                              <input class="form-check-input" type="checkbox" name="is_therapy" id="is_therapy" value="1">
                              <label class="form-check-label" for="is_therapy">3. Terapi</label>
                            </div>
                          </div>
                      </div>
                  </div>
                  <div class="mb-2">
                      <div class="form-group row">
                          <label class="col-lg-4 col-form-label">Kedatangan</label>
                          <div class="col-lg-8">
                              <div class="input-icon">
                                  <input type="input" class="form-control datetimepicker-input" placeholder="dd/mm/yyyy hh:ii" id="consultation_date" name="consultation_date" data-date-format="dd-mm-yyyy hh:ii" value="{{date('d-m-Y H:i')}}" readonly required/>
                                  <span>
                                      <i class="far fa-calendar-alt text-muted"></i>
                                  </span>
                              </div>
                          </div>
                      </div>
                  </div>
                  <div class="mb-2">
                      <div class="form-group row">
                          <label class="col-lg-4 col-form-label">Resep</label>
                          <div class="col-lg-8">
                              <textarea type="text" class="form-control" id="note" name="note" placeholder="Enter Resep Pasien" value="" rows="10" required></textarea>
                          </div>
                      </div>
                  </div>
                  <div class="mb-2">
                      <div class="form-group row">
                          <label class="col-lg-4 col-form-label"></label>
                          <div class="col-lg-8">
                              <button type="submit" class="btn btn-success font-weight-bold">Save</button>
                              <button type="button" data-dismiss="modal" class="btn btn-danger">Cancel</button>
                          </div>
                      </div>
                  </div>
              </div>
            </form>
        </div>
    </div>
</div>
@endsection

{{-- Styles Section --}}
@section('styles')
<link rel="stylesheet" href="{{ config('app.url') }}global/vendor/datatables-bootstrap/dataTables.bootstrap.css">
<link rel="stylesheet" href="{{ config('app.url') }}global/vendor/datatables-responsive/dataTables.responsive.min.css">
<link rel="stylesheet" href="{{ config('app.url') }}plugins/custom/datatables/datatables.bundle.css">
<link rel="stylesheet" href="{{ config('app.url') }}css/inject.css">
@endsection

{{-- Scripts Section --}}
@section('scripts')
@include ('inc.confirm-delete-modal')
<script src="{{ config('app.url') }}global/vendor/datatables/jquery.dataTables.js"></script>
<script src="{{ config('app.url') }}global/vendor/datatables-bootstrap/dataTables.bootstrap.js"></script>
<script src="{{ config('app.url') }}global/vendor/datatables-responsive/dataTables.responsive.js"></script>
<script src="{{ config('app.url') }}js/inject.js"></script>
<script type="text/javascript">
  var start_date = "";
  var end_date = "";
  $(document).ready(function() {
    $('.select2').select2({templateResult: formatOption});
    $('div.datetimepicker').css('top',0);
    $('.datetime-input').datepicker({
        format: 'dd-mm-yyyy',
        inline: true,
    });
    $('.datetimepicker-input').datetimepicker({
        format: 'dd-mm-yyyy hh:ii',
        inline: true,
    });
    $('[data-switch=true]').bootstrapSwitch('state', true);

    $("div.datesearchbox").html('<div class="input-group"> <div class="input-group-addon"> <i class="glyphicon glyphicon-calendar"></i> </div><input type="text" class="form-control pull-right" id="datesearch" placeholder="Search by date range"> </div>');
    document.getElementsByClassName("datesearchbox")[0].style.textAlign = "center";
    $("#datesearch").attr("readonly",true);
    $('#datesearch').daterangepicker({
       autoUpdateInput: false
     });
     //menangani proses saat apply date range
      $('#datesearch').on('apply.daterangepicker', function(ev, picker) {
         $(this).val(picker.startDate.format('DD-MM-YYYY') + ' - ' + picker.endDate.format('DD-MM-YYYY'));
         $("#start-date").val(picker.startDate.format('YYYY-MM-DD'));
         $("#end-date").val(picker.endDate.format('YYYY-MM-DD'));
         refresh_table();
      });

      $('#datesearch').on('cancel.daterangepicker', function(ev, picker) {
        $(this).val('');
        $("#start-date").val('');
        $("#end-date").val('');
        refresh_table();
      });
  });
  $("#img_url").change(function() {
    bg_readURL(this,'img_show_form');
  });

  $('[data-switch=true]').bootstrapSwitch();
  var table = $('#datatable').dataTable({
    pageLength: 5,
    responsive: true,
    searchDelay: 800,
    processing: true,
    serverSide: true,
    searching: true,
    lengthMenu: [[5, 10, 25, 50, 100, 200, -1], [5, 10, 25, 50, 100, 200, "All"]],
    ajax: {
      method: 'POST',
      url : '{{ $path }}/list',
      headers: {
        'X-CSRF-TOKEN': '{{ csrf_token() }}'
      },
      data: function (d) {
        d.from_date = $("#start-date").val();
        d.to_date = $("#end-date").val();
      }
    },
    columns: [
      {title: "No", data: 'DT_RowIndex', orderable: false, searchable: false, autoHide: false},
      {title: "Foto Pasien", data: 'reservation_desc', defaultContent: '-', class: 'text-center dt-body-nowrap', autohide: true},
      {title: "Reservasi ID", data: 'id', defaultContent: '-', class: 'text-center dt-body-nowrap', autohide: true},
      {title: "Nama Pasien", data: 'name', defaultContent: '-', class: 'text-center dt-body-nowrap', autohide: true},
      {title: "No Telp", data: 'phone', defaultContent: '-', class: 'text-center dt-body-nowrap', autohide: true},
      {title: "Email", data: 'email', defaultContent: '-', class: 'text-center dt-body-nowrap', autohide: true},
      {title: "Keluhan", data: 'consultation', defaultContent: '-', class: 'text-center dt-body-nowrap', autohide: true},
      {title: "Actions", data: 'action', orderable: false, autohide: true},
    ],
    order: [[1, 'asc']],
    bStateSave: true,
    dom:  "<'row'<'col-sm-4'l><'col-sm-5' <'datesearchbox'>><'col-sm-3'f>>" +
          "<'row'<'col-sm-12'tr>>" +
          "<'row'<'col-sm-5'i><'col-sm-7'p>>",
    columnDefs: [
      {
        targets: [0,-1],
        className: 'text-center visible dt-body-nowrap'
      },
      'colvis'
    ]
  });
  $('#btn_tools > li > a.tool-action').on('click', function () {
      var action = $(this).attr('data-action');
      table.dataTable().button(action).trigger();
  });

  function show_laporan(id = "") {
      $.ajax({
          url: "{{ $path }}/data/" + id,
          type: "GET",
          headers: {
            'X-CSRF-TOKEN': '{{ csrf_token() }}'
          },
          success: function (response) {
            if(!response){
              showDialog.show("data "+id+" not found");
              return;
            }
            $('#form-input').trigger("reset");
            $("#form-input").attr("action", "{{ $path }}/update/"+id);
            $('#id').attr('readonly',true)
            var format = '';
            format += '*Laporan: ' +formatdate(response.data.consultation_date)+'*';
            format += '<br> *Pendaftaran: Pasien '+response.data.register_status+'*';
            format += '<br> *Nama Pasien:* '+response.data.gender_name+' '+response.data.name;
            format += '<br> *No Telp:* '+response.data.phone;
            format += '<br> *Email:* '+response.data.email;
            format += '<br> *Alamat:* <br>  '+response.data.address.replace(/(?:\r\n|\r|\n)/g, '<br>  ');
            format += '<br> *Konsultasi:* <br>  '+response.data.consultation.replace(/(?:\r\n|\r|\n)/g, '<br>  ');
            format += '<br> *Resep:* <br>  '+response.data.note.replace(/(?:\r\n|\r|\n)/g, '<br>  ');
            format += '<br> *Reservasi ID:* '+response.data.id;
            format += '<br> *Marketing:* '+response.data.sales_name;
            copyToClipboard(format);

            var img_url = '{{ENV('APP_URL')}}'+response.data.img_url;
            var options = {
              title:'Laporan: '+formatdate(response.data.consultation_date)+'<br> Pasien '+response.data.register_status +':'+response.data.name,
              imgUrl:img_url,
            };
            showDialog.show(format,options);
          },
          error: function (xhr, status, error) {
              showDialog.show(xhr.status + " " + status + " " + error, false);
          }
      });
  }


  function show_data(id = "") {
      if (id !== "") {
          $.ajax({
              url: "{{ $path }}/data/" + id,
              type: "GET",
              headers: {
                'X-CSRF-TOKEN': '{{ csrf_token() }}'
              },
              success: function (response) {
                if(!response){
                  showDialog.show("data "+id+" not found");
                }
                  $('#form-input').trigger("reset");
                  $("#form-input").attr("action", "{{ $path }}/update/"+id);
                  var img_url = '{{ENV('APP_URL')}}'+response.data.img_url;
                  $('#img_show_form').css("background-image", "url('" +img_url+ "')");
                  $('#id').val(response.data.id).trigger('change.select2');
                  $("#id").select2().prop("disabled", true);

                  $('#id').val(response.data.id);
                  $('#reservation_id').val(response.data.id);
                  $('#reservation_name').val(response.data.name);
                  $('#name').val(response.data.name);
                  $('#address').text(response.data.address);
                  $('#phone').val(response.data.phone);
                  $('#email').val(response.data.email);
                  $('#consultation').text(response.data.consultation);
                  $('#city_id').val(response.data.city_id);
                  $('#note').text(response.data.note);
                  $('#is_order').val(response.data.is_order);
                  if(response.data.is_consultation){$('#is_consultation').prop('checked', true);}
                  if(response.data.is_order){$('#is_order').prop('checked', true);}
                  if(response.data.is_therapy){$('#is_therapy').prop('checked', true);}
                  $('.btn-group > .btn').removeClass('active')
                  $('.btn-group > .rdb-'+response.data.register_status).addClass('active')
                  $("input[name=register_status][value="+response.data.register_status+"]").prop('checked', true);
                  $('#gender_id').val(response.data.gender_id).trigger('change');
                  $('#birthdate').val(formatdate(response.data.birthdate));
                  $('#consultation_date').val(formatdate(response.data.consultation_date));
                  $('#modal-form').modal('show');
              },
              error: function (xhr, status, error) {
                  showDialog.show(xhr.status + " " + status + " " + error, false);
              }
          });
      } else {
        form_reset();
      }
  }

  $('#id').on( 'change', function () {
      if (this.value !== "") {
          $.ajax({
              url: "{{ $path }}/data/" + this.value,
              type: "GET",
              headers: {
                'X-CSRF-TOKEN': '{{ csrf_token() }}'
              },
              success: function (response) {
                if(!response){
                  showDialog.show("data "+id+" not found");
                }
                  $('#form-input').trigger("reset");
                  var img_url = '{{ENV('APP_URL')}}'+response.data.img_url;
                  $('#img_show_form').css("background-image", "url('" +img_url+ "')");
                  $('#id').val(response.data.id);
                  $('#reservation_id').val(response.data.id);
                  $('#reservation_name').val(response.data.name);
                  $('#name').val(response.data.name);
                  $('#address').text(response.data.address);
                  $('#phone').val(response.data.phone);
                  $('#email').val(response.data.email);
                  $('#consultation').text(response.data.consultation);
                  $('#city_id').val(response.data.city_id);
                  $('#note').text(response.data.note);
                  $('#is_order').val(response.data.is_order);
                  if(response.data.is_consultation){$('#is_consultation').prop('checked', true);}
                  if(response.data.is_order){$('#is_order').prop('checked', true);}
                  if(response.data.is_therapy){$('#is_therapy').prop('checked', true);}
                  $('.btn-group > .btn').removeClass('active')
                  $('.btn-group > .rdb-'+response.data.register_status).addClass('active')
                  $("input[name=register_status][value="+response.data.register_status+"]").prop('checked', true);
                  $('#gender_id').val(response.data.gender_id).trigger('change');
                  $('#birthdate').val(formatdate(response.data.birthdate));
                  $('#consultation_date').val(formatdate(response.data.consultation_date));
              },
              error: function (xhr, status, error) {
                  showDialog.show(xhr.status + " " + status + " " + error, false);
              }
          });
      }else{
        form_reset();
      }
  });

  function form_reset(){
      $('#form-input').trigger("reset");
      $("#form-input").attr("action", "{{ $path }}/save");
      $("#id").removeAttr('disabled');

      $('#gender_id').val('').trigger('change');
      $('#address, #consultation, #note').text('');
      $('#is_consultation, #is_order, #is_therapy').prop('checked', false);
      $('.btn-group > .btn').removeClass('active');
      $('.btn-group > .rdb-Baru').addClass('active');
      $("input[name=register_status][value=Baru]").prop('checked', true);
      $('#birthdate').datepicker('setDate', "{{date('d-m-Y',strtotime('1994-06-15'))}}");
      $('#consultation_date').datetimepicker('setDate', new Date());
      $('#modal-form').modal('show');
  }

  function refresh_table() {
      table.fnDraw();
  }

  function formatOption (option) {
    var $option = $(
      '<div><strong>' + option.text + '</strong></div><div>' + option.title + '</div>'
    );
    return $option;
  };
</script>
@endsection
