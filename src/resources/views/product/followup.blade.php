@extends('layout.default')

@section('content')
<div class="subheader py-2 {{ Metronic::printClasses('subheader', false) }}" id="kt_subheader">
  <div class="{{ Metronic::printClasses('subheader-container', false) }} d-flex align-items-center justify-content-between flex-wrap flex-sm-nowrap">
    <div class="d-flex align-items-baseline flex-wrap mr-5">
      <h5 class="text-dark font-weight-bold my-1 mr-5">
        {{ $module_alias }}
      </h5>
      <ul class="breadcrumb breadcrumb-transparent breadcrumb-dot font-weight-bold p-0 my-2 font-size-sm">
        <li class="breadcrumb-item">
          <a href="{{ route('dashboard') }}" class="text-muted">Dashboard</a>
        </li>
        <li class="breadcrumb-item">
          <a href="{{ route('product.index','interaksi') }}" class="text-muted">{{ $module_alias }}</a>
        </li>
        <li class="breadcrumb-item">
          <a href="#view" class="text-muted">View</a>
        </li>
      </ul>
    </div>
  </div>
</div>

@include('inc.error-list')
@include('inc.success-notif')
@include('inc.danger-notif')
<div class="card card-custom body-container">
  <div class="card-header bg-danger flex-wrap border-1 pt-1 pb-0 mb-2" style="min-height: 0;">
    <div class="card-title pt-1 pb-1">
      <h3 class="card-label font-weight-bolder text-white">{{ $module_alias }}
        <div class="text-muted pt-2 font-size-lg">show Datatable from table {{ $module_alias }}</div>
      </h3>
    </div>
    <div class="card-toolbar pt-1 pb-0">
      <a href="#" onclick="show_data('')" class="btn btn-primary font-weight-bolder" style="background-color: #1e1e2d;border-color: #0c8eff;">
        <span class="svg-icon svg-icon-md">
          <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
            <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
              <rect x="0" y="0" width="24" height="24"></rect>
              <circle fill="#000000" cx="9" cy="15" r="6"></circle>
              <path d="M8.8012943,7.00241953 C9.83837775,5.20768121 11.7781543,4 14,4 C17.3137085,4 20,6.6862915 20,10 C20,12.2218457 18.7923188,14.1616223 16.9975805,15.1987057 C16.9991904,15.1326658 17,15.0664274 17,15 C17,10.581722 13.418278,7 9,7 C8.93357256,7 8.86733422,7.00080962 8.8012943,7.00241953 Z" fill="#000000" opacity="0.3"></path>
            </g>
          </svg>
        </span>Add New
      </a>
      <div class="dropdown dropdown-inline px-2">
      </div>
    </div>
  </div>
  <div class="card-body pt-1">
    <table class="table table-bordered table-hover w100" cellspacing="0" id="datatable" style="width: 1070px !important;"></table>
  </div>
</div>

<!-- Modal-->
<div class="modal fade" id="modal-form" data-backdrop="static" tabindex="-1" role="dialog" aria-labelledby="staticBackdrop" aria-hidden="true">
    <div class="modal-dialog modal-md" role="document">
        <div class="modal-content">
            <div class="modal-header bg-danger pt-3 pb-3">
                <h5 class="modal-title text-white bold" id="modal">New {{ $module_alias }}</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <i aria-hidden="true" class="ki ki-close"></i>
                </button>
            </div>
            <form class="form" id="form-input" action="{{ $path }}" method="POST">
                {!! csrf_field() !!}
                <input type="hidden" class="form-control" id="method" name="_method" placeholder="Enter method" value="POST"/>
                <input type="hidden" class="form-control" id="id" name="id" placeholder="Enter ID"/>
                <div class="card-body pt-3">
                  <div class="mb-2">
                      <div class="form-group row">
                          <label class="col-lg-4 col-form-label">Iklan</label>
                          <div class="col-lg-8">
                              <select class="form-control select2" id="advertise_id" name="advertise_id" style="width: 100%;" required>
                                <option value="" selected>Chose Advertise</option>
                                 @php $data = list_model('Master','Ads') @endphp
                                 @isset ($data)
                                   @foreach($data as $rs)
                                    <option value="{{ $rs->id }}">{{ $rs->name }}</option>
                                   @endforeach
                                 @endisset
                              </select>
                          </div>
                      </div>
                  </div>
                  <div class="mb-2">
                      <div class="form-group row">
                          <label class="col-lg-4 col-form-label">Interaksi</label>
                          <div class="col-lg-8">
                              <select class="form-control select2" id="interaction_id" name="interaction_id" style="width: 100%;" required>
                                <option value="" selected>Chose Interaksi</option>
                                 @php $data = list_model('Master','Interaction') @endphp
                                 @isset ($data)
                                   @foreach($data as $rs)
                                    <option value="{{ $rs->id }}">{{ $rs->name }}</option>
                                   @endforeach
                                 @endisset
                              </select>
                          </div>
                      </div>
                  </div>
                  <div class="mb-2">
                      <div class="form-group row">
                          <label class="col-lg-4 col-form-label">Customer</label>
                          <div class="col-lg-8">
                              <select class="form-control select2" id="customer_id" name="customer_id" style="width: 100%;">
                                <option value="" selected>New Customer</option>
                                @php $data = list_model('Master','Customer','created_by="'.sess_user('id').'"') @endphp
                                @isset ($data)
                                  @foreach($data as $rs)
                                   <option value="{{ $rs->id }}">{{ $rs->id }}-{{ $rs->full_name }}</option>
                                  @endforeach
                                @endisset
                             </select>
                          </div>
                      </div>
                  </div>
                  <div class="mb-2">
                      <div class="form-group row">
                          <label class="col-lg-4 col-form-label">Sapaan</label>
                          <div class="col-lg-8">
                              <select class="form-control select2" id="gender_id" name="gender_id" style="width: 100%;" required>
                                <option value="" selected>Chose </option>
                                @php $data = list_model('Master','Gender') @endphp
                                @isset ($data)
                                  @foreach($data as $rs)
                                   <option value="{{ $rs->id }}">{{ $rs->name }}</option>
                                  @endforeach
                                @endisset
                             </select>
                          </div>
                      </div>
                  </div>
                  <div class="mb-1">
                      <div class="form-group row">
                          <label class="col-lg-4 col-form-label">Name Customer</label>
                          <div class="col-lg-8">
                              <input type="text" class="form-control" id="full_name" name="full_name" placeholder="Enter Name" value="" required/>
                          </div>
                      </div>
                  </div>
                  <div class="mb-1">
                      <div class="form-group row">
                          <label class="col-lg-4 col-form-label">No Telfon</label>
                          <div class="col-lg-8">
                              <input type="tel" minlength="8" maxlength="14" class="form-control" id="phone" name="phone" placeholder="Enter Name" value="" required/>
                          </div>
                      </div>
                  </div>
                  <div class="mb-2">
                      <div class="form-group row">
                          <label class="col-lg-4 col-form-label">Keluhan</label>
                          <div class="col-lg-8">
                              <input class="form-control tagify" id='consultation' name='consultation' placeholder="keluhan" value="" required/>
                          </div>
                      </div>
                  </div>
                  <div class="mb-2">
                      <div class="form-group row">
                          <label class="col-lg-4 col-form-label">Status</label>
                          <div class="col-lg-8">
                              <input id="status" name="status" data-switch="true" type="checkbox" data-off-text="Kunjungan" data-handle-width="200" data-on-text="Transaksi" data-off-color="info" data-on-color="warning" />
                          </div>
                      </div>
                  </div>
                  <div class="is_reqs">
                    <div class="separator separator-dashed my-3"></div>
                      <div class="row">
                        <div class="col-lg-6 px-5">
                            <div class="mb-2">
                                <div class="form-group row">
                                    <label class="col-lg-4 col-form-label">Product</label>
                                    <div class="col-lg-8">
                                        <select class="form-control select2" id="item_id" name="item_id" style="width: 100%;">
                                          <option value="" selected>Chose </option>
                                          @php $data = list_model('Master','Item') @endphp
                                          @isset ($data)
                                            @foreach($data as $rs)
                                             <option value="{{ $rs->id }}">{{ $rs->name }}</option>
                                            @endforeach
                                          @endisset
                                       </select>
                                    </div>
                                </div>
                            </div>
                            <div class="mb-2">
                                <div class="form-group row">
                                    <label class="col-lg-4 col-form-label">Voucher</label>
                                    <div class="col-lg-8">
                                        <select class="form-control select2" id="voucher_id" name="voucher_id" style="width: 100%;">
                                          <option value="" selected>Chose </option>
                                          @php $data = list_model('Master','Voucher') @endphp
                                          @isset ($data)
                                            @foreach($data as $rs)
                                             <option value="{{ $rs->id }}">{{ $rs->name }}</option>
                                            @endforeach
                                          @endisset
                                       </select>
                                    </div>
                                </div>
                            </div>
                            <div class="mb-1">
                                <div class="form-group row">
                                    <label class="col-lg-4 col-form-label">Potongan</label>
                                    <div class="col-lg-8">
                                        <input type="number" class="form-control" id="voucher" name="voucher" placeholder="Enter Voucher" value="0" readonly/>
                                    </div>
                                </div>
                            </div>
                            <div class="mb-1">
                                <div class="form-group row">
                                    <label class="col-lg-4 col-form-label">Jumlah</label>
                                    <div class="col-lg-8">
                                        <input type="number" class="form-control" id="quantity" name="quantity" placeholder="Enter quantity" value="0"/>
                                    </div>
                                </div>
                            </div>
                            <div class="mb-1">
                                <div class="form-group row">
                                    <label class="col-lg-4 col-form-label">Harga</label>
                                    <div class="col-lg-8">
                                        <input type="number" class="form-control" id="price" name="price" placeholder="Enter price" value="0"/>
                                    </div>
                                </div>
                            </div>
                            <div class="mb-1">
                                <div class="form-group row">
                                    <label class="col-lg-4 col-form-label">Biaya Kirim</label>
                                    <div class="col-lg-8">
                                        <input type="number" class="form-control" id="courier_cost" name="courier_cost" placeholder="Enter courier cost" value="0"/>
                                    </div>
                                </div>
                            </div>
                            <div class="mb-1">
                                <div class="form-group row">
                                    <label class="col-lg-4 col-form-label">Asuransi</label>
                                    <div class="col-lg-8">
                                        <input type="number" class="form-control" id="insurance" name="insurance" placeholder="Enter insurance" value="0"/>
                                    </div>
                                </div>
                            </div>
                            <div class="mb-1">
                                <div class="form-group row">
                                    <label class="col-lg-4 col-form-label">Biaya Penanganan</label>
                                    <div class="col-lg-8">
                                        <input type="number" class="form-control" id="cost_handler" name="cost_handler" placeholder="Enter insurance" value="0"/>
                                    </div>
                                </div>
                            </div>
                            <div class="mb-1">
                                <div class="form-group row">
                                    <label class="col-lg-4 col-form-label">Transaksi</label>
                                    <div class="col-lg-8">
                                        <input type="text" class="form-control" id="transactiontemp" name="transactiontemp" placeholder="Enter transaction" value="0" readonly/>
                                        <input hidden type="text" class="form-control" id="transaction" name="transaction" placeholder="Enter transaction" value="0" readonly/>
                                        <input hidden type="text" class="form-control" id="voucher_total" name="voucher_total" placeholder="Enter transaction" value="0" readonly/>
                                    </div>
                                </div>
                            </div>
                            <div class="mb-2">
                                <div class="form-group row">
                                    <label class="col-lg-4 col-form-label">Market</label>
                                    <div class="col-lg-8">
                                        <select class="form-control select2" id="market_id" name="market_id" style="width: 100%;">
                                          <option value="" selected>Chose </option>
                                          @php $data = list_model('Master','Market') @endphp
                                          @isset ($data)
                                            @foreach($data as $rs)
                                             <option value="{{ $rs->id }}">{{ $rs->name }}</option>
                                            @endforeach
                                          @endisset
                                       </select>
                                    </div>
                                </div>
                            </div>
                            <div class="mb-2">
                                <div class="form-group row">
                                    <label class="col-lg-4 col-form-label">Kurir</label>
                                    <div class="col-lg-8">
                                        <select class="form-control select2" id="courier_id" name="courier_id" style="width: 100%;">
                                          <option value="" selected>Chose </option>
                                          @php $data = list_model('Master','Courier') @endphp
                                          @isset ($data)
                                            @foreach($data as $rs)
                                             <option value="{{ $rs->id }}">{{ $rs->name }}</option>
                                            @endforeach
                                          @endisset
                                       </select>
                                    </div>
                                </div>
                            </div>
                            <div class="mb-2">
                                <div class="form-group row">
                                    <label class="col-lg-4 col-form-label">COD/TF</label>
                                    <div class="col-lg-8">
                                        <select class="form-control select2" id="payment_type_id" name="payment_type_id" style="width: 100%;">
                                          <option value="" selected>Chose </option>
                                          @php $data = list_model('Master','Payment-Type') @endphp
                                          @isset ($data)
                                            @foreach($data as $rs)
                                             <option value="{{ $rs->id }}">{{ $rs->name }}</option>
                                            @endforeach
                                          @endisset
                                       </select>
                                    </div>
                                </div>
                            </div>
                            <div class="mb-2">
                                <div class="form-group row">
                                    <label class="col-lg-4 col-form-label">Bank</label>
                                    <div class="col-lg-8">
                                      <select class="form-control select2" name="bank_id" style="width:100%;">
                                        <option value="" selected>Chose </option>
                                          @php $data = list_model('Master','Bank') @endphp
                                          @isset ($data)
                                            @foreach($data as $rs)
                                             <option value="{{ $rs->id }}">{{ $rs->name }}</option>
                                            @endforeach
                                          @endisset
                                      </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-6 px-5">
                        <div class="mb-2">
                            <div class="form-group row">
                                <label class="col-lg-4 col-form-label">Destination</label>
                                <div class="col-lg-8">
                                  <select class="form-control select2" id="customer_address_id" name="customer_address_id" style="width:100%;">
                                    <option value="" selected>New Address </option>
                                  </select>
                                </div>
                            </div>
                        </div>
                        <div class="mb-1">
                            <div class="form-group row">
                                <label class="col-lg-4 col-form-label">Alamat</label>
                                <div class="col-lg-8">
                                    <textarea type="text" class="form-control" id="address" name="address" placeholder="Enter customer address" value="" readonly></textarea>
                                </div>
                            </div>
                        </div>
                        <div class="mb-1">
                            <div class="form-group row">
                                <label class="col-lg-4 col-form-label">No. Rumah</label>
                                <div class="col-lg-8">
                                    <input type="text" class="form-control" id="address_no" name="address_no" placeholder="Enter no" value=""/>
                                </div>
                            </div>
                        </div>
                        <div class="mb-1">
                            <div class="form-group row">
                                <label class="col-lg-4 col-form-label">Rt</label>
                                <div class="col-lg-8">
                                    <input type="text" class="form-control" id="rt" name="rt" placeholder="Enter rt" value=""/>
                                </div>
                            </div>
                        </div>
                        <div class="mb-1">
                            <div class="form-group row">
                                <label class="col-lg-4 col-form-label">rw</label>
                                <div class="col-lg-8">
                                    <input type="text" class="form-control" id="rw" name="rw" placeholder="Enter rt" value=""/>
                                </div>
                            </div>
                        </div>
                        <div class="mb-1">
                            <div class="form-group row">
                                <label class="col-lg-4 col-form-label">Desa</label>
                                <div class="col-lg-8">
                                    <input type="text" class="form-control" id="village" name="village" placeholder="Enter village" value=""/>
                                </div>
                            </div>
                        </div>
                        <div class="mb-1">
                            <div class="form-group row">
                                <label class="col-lg-4 col-form-label">Kelurahan</label>
                                <div class="col-lg-8">
                                    <input type="text" class="form-control" id="benchmark" name="benchmark" placeholder="Enter benchmark" value=""/>
                                </div>
                            </div>
                        </div>
                        <div class="mb-1">
                            <div class="form-group row">
                                <label class="col-lg-4 col-form-label">Patokan</label>
                                <div class="col-lg-8">
                                    <input type="text" class="form-control" id="sub_district" name="sub_district" placeholder="Enter sub district" value=""/>
                                </div>
                            </div>
                        </div>
                        <div class="mb-2">
                            <div class="form-group row">
                                <label class="col-lg-4 col-form-label">Provinsi</label>
                                <input hidden type="input" class="form-control" id="province_temp" value=""/>
                                <div class="col-lg-8">
                                    <select class="form-control select2" id="province_id" name="province_id" style="width:100%;">
                                      <option value="" selected>Chose </option>
                                        @php $data = list_province() @endphp
                                        @isset ($data)
                                          @foreach($data as $rs)
                                           <option value="{{ $rs->province }}">{{ $rs->province }}</option>
                                          @endforeach
                                        @endisset
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="mb-2">
                            <div class="form-group row">
                                <label class="col-lg-4 col-form-label">Kota</label>
                                <input hidden type="input" class="form-control" id="city_temp" value=""/>
                                <div class="col-lg-8">
                                    <select class="form-control select2" id="city_id" name="city_id" style="width:100%;">
                                      <option value="" selected>Chose </option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="mb-2">
                            <div class="form-group row">
                                <label class="col-lg-4 col-form-label">Area</label>
                                <input hidden type="input" class="form-control" id="district_temp" value=""/>
                                <div class="col-lg-8">
                                    <select class="form-control select2" id="district" name="district" style="width:100%;">
                                      <option value="" selected>Chose </option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="mb-1">
                            <div class="form-group row">
                                <label class="col-lg-4 col-form-label">Kode Pos</label>
                                <div class="col-lg-8">
                                    <input type="text" class="form-control" id="postal_code" name="postal_code" placeholder="Enter postal_code" value=""/>
                                </div>
                            </div>
                        </div>

                        <div class="mb-2">
                            <div class="form-group row">
                                <label class="col-lg-4 col-form-label">Tanggal</label>
                                <div class="col-lg-8">
                                    <div class="input-icon">
                                        <input type="input" class="form-control datetimepicker-input" id="transaction_date" name="transaction_date" placeholder="dd/mm/yyyy" id="last_date" data-date-format="dd-mm-yyyy hh:ii" value="<?php echo date('d-m-Y H:i'); ?>"/>
                                        <span>
                                            <i class="far fa-calendar-alt text-muted"></i>
                                        </span>
                                    </div>
                                </div>
                            </div>
                        </div>
                      </div>
                    </div>
                </div>
                <div class="mb-2">
                    <div class="form-group row">
                        <label class="col-lg-4 col-form-label"></label>
                        <div class="col-lg-8">
                            <button type="submit" class="btn btn-success font-weight-bold">Save</button>
                            <button type="button" data-dismiss="modal" class="btn btn-danger">Cancel</button>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
@endsection

{{-- Styles Section --}}
@section('styles')
<link rel="stylesheet" href="{{ config('app.url') }}global/vendor/datatables-bootstrap/dataTables.bootstrap.css">
<link rel="stylesheet" href="{{ config('app.url') }}global/vendor/datatables-responsive/dataTables.responsive.min.css">
<link rel="stylesheet" href="{{ config('app.url') }}plugins/custom/datatables/datatables.bundle.css">
<link rel="stylesheet" href="{{ config('app.url') }}css/inject.css">
<style>
.datepicker
{
width: fit-content;
}
</style>
@endsection

{{-- Scripts Section --}}
@section('scripts')
@include ('inc.confirm-delete-modal')
<script src="{{ config('app.url') }}global/vendor/datatables/jquery.dataTables.js"></script>
<script src="{{ config('app.url') }}global/vendor/datatables-bootstrap/dataTables.bootstrap.js"></script>
<script src="{{ config('app.url') }}global/vendor/datatables-responsive/dataTables.responsive.js"></script>
<script src="{{ config('app.url') }}js/inject.js"></script>
<script type="text/javascript">
  var start_date = "";
  var end_date = "";
  $(document).ready(function() {
    $(".is_reqs").hide();
    $('.select2').select2({});
    $('.datetime-input').datepicker({
        format: 'dd-mm-yyyy',
        inline: true,
    });
    $('.datetimepicker-input').datetimepicker({
        format: 'dd-mm-yyyy hh:ii',
        inline: true,
    });
    $("div.datesearchbox").html('<div class="input-group"> <div class="input-group-addon"> <i class="glyphicon glyphicon-calendar"></i> </div><input type="text" class="form-control text-center datetimepicker-input" id="datesearch_start" placeholder="Search by date range" value="{{date('d-m-Y H:i', strtotime($dashboard_startdate))}}">&nbsp;to&nbsp;<input type="text" class="form-control  text-center datetimepicker-input" id="datesearch_end" placeholder="Search by date range" value="{{date('d-m-Y H:i', strtotime($dashboard_enddate))}}"></div>');
    $("#datesearch_start, #datesearch_end").attr("readonly",true);
    $("#datesearch_start, #datesearch_end").change(function(){
      refresh_table();
    });
    $('#datesearch_start, #datesearch_end').datetimepicker({
        format: 'dd-mm-yyyy hh:ii',
        inline: true,
     });
  });

  $('[data-switch=true]').bootstrapSwitch('state', true);
  $('#status').on('switchChange.bootstrapSwitch', function (event, state) {
      var x = $(this).data('on-text');
      var y = $(this).data('off-text');
      if ($("#status").is(':checked')) {
          $(".is_reqs").show(500);
      } else {
          $(".is_reqs").hide(500);
      }
  });


  $('.datetimepicker-input').datetimepicker({
      format: 'dd-mm-yyyy hh:ii'
  });
  $('[data-switch=true]').bootstrapSwitch();
  var table = $('#datatable').dataTable({
    pageLength: 5,
    responsive: true,
    searchDelay: 800,
    processing: true,
    serverSide: true,
    select: true,
    searching: true,
    lengthMenu: [[5, 10, 25, 50, 100, 200, -1], [5, 10, 25, 50, 100, 200, "All"]],
    ajax: {
      method: 'POST',
      url : '{{ $path }}/list',
      headers: {
        'X-CSRF-TOKEN': '{{ csrf_token() }}'
      },
      data: function (d) {
        d.from_date = formatdate($("#datesearch_start").val());
        d.to_date = formatdate($(" #datesearch_end").val(),true);
      }
    },
    columns: [
      {title: "No", data: 'DT_RowIndex', defaultContent: '-', class: 'text-center dt-body-nowrap', orderable: false, searchable: false, autoHide: false},
      {title: "Cust ID", data: 'customer_id', defaultContent: '-', class: 'text-center dt-body-nowrap'},
      {title: "Advertise", data: 'advertise_name', name: 'mst_advertise.name', defaultContent: '-', class: 'text-center dt-body-nowrap'},
      {title: "Interaction", data: 'interaction_name', name: 'mst_interaction.name', defaultContent: '-', class: 'text-center dt-body-nowrap'},
      {title: "Cust Name", data: 'full_name', name: 'mst_customer.full_name', defaultContent: '-', class: 'text-center dt-body-nowrap', autoHide: false},
      {title: "Cust Phone", data: 'phone', name: 'mst_customer.phone', defaultContent: '-', class: 'text-center dt-body-nowrap', autoHide: false},
      {title: "Status", data: 'active', defaultContent: '-', class: 'text-center dt-body-nowrap'},
      {title: "Actions", data: 'action', orderable: false, responsivePriority: -1},
    ],
    order: [[1, 'asc']],
    bStateSave: true,
    dom:  "<'row'<'col-sm-4'l><'col-sm-5' <'datesearchbox'>><'col-sm-3'f>>" +
          "<'row'<'col-sm-12'tr>>" +
          "<'row'<'col-sm-5'i><'col-sm-7'p>>",
    columnDefs: [
      {
        targets: [0,-1],
        className: 'text-center visible dt-body-nowrap'
      },
    ],
    initComplete: function() {
      $('.tl-tip').tooltip();
    }
  });

  function show_data(id = "") {
      form_reset();
      if (id !== "") {
          $.ajax({
              url: "{{ $path }}/data/" + id,
              type: "GET",
              headers: {
                'X-CSRF-TOKEN': '{{ csrf_token() }}'
              },
              success: function (response) {
                  $("#form-input").attr("action", "{{ $path }}/update/"+id);
                  $('#form-input').trigger("reset");
                  $('#method').val("POST");

                  $('#id').val(response.data.id);
                  $('#advertise_id').val(response.data.advertise_id).trigger('change');
                  $('#interaction_id').val(response.data.interaction_id).trigger('change');
                  $('#item_id').val(response.data.item_id).trigger('change');
                  $('#quantity').val(response.data.quantity);
                  $('#price').val(response.data.price);
                  $('#courier_cost').val(response.data.courier_cost);
                  $('#insurance').val(response.data.insurance);
                  $('#cost_handler').val(response.data.cost_handler);
                  $('#voucher_total').val(response.data.voucher_total);
                  $('#transaction').val(response.data.transaction);
                  $('#voucher_id').val(response.data.voucher_id).trigger('change');
                  $('#transactiontemp').val("Rp. "+parseFloat(response.data.transaction).toFixed(2).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ","));
                  $('#market_id').val(response.data.market_id).trigger('change');
                  $('#courier_id').val(response.data.courier_id).trigger('change');
                  $('#payment_type_id').val(response.data.payment_type_id).trigger('change');
                  $('#customer_id').val(response.data.customer_id).trigger('change');
                  list_customer_address(response.data.customer_address_id,response.data.id);
                  $('#transaction_date').val(response.data.transaction_date).datetimepicker('update', response.data.transaction_date);
                  $('#status').bootstrapSwitch('state', Boolean(Number(response.data.status)));
                  $('#modal-form').modal('show');
                  calculation();
              },
              error: function (xhr, status, error) {
                  showDialog.show(xhr.status + " " + status + " " + error, false);
              }
          });
      }
  }

  $('#customer_id').change(function(){
      var id = $('#customer_id').val();
      if (id) {
          $.ajax({
              url: "{{ getRoutes('master','customer') }}/data/" + id,
              type: "GET",
              headers: {
                'X-CSRF-TOKEN': '{{ csrf_token() }}'
              },
              success: function (response) {
                if (response.data){
                  $('#customer_id').val(response.data.id);
                  $('#gender_id').val(response.data.gender_id).trigger('change');
                  $('#full_name').val(response.data.full_name);
                  $('#phone').val(response.data.phone);
                  $('#consultation').val(response.data.consultation);
                  $('#consultation').text(response.data.consultation);
                  if(response.data.id){
                    list_customer_address('',response.data.id);
                  }
                }
              },
              error: function (xhr, status, error) {
                  showDialog.show(xhr.status + " " + status + " " + error, false);
              }
          });
      }
  });

  $('#customer_address_id').change(function(){
      var id = $('#customer_address_id').val();
      if (id) {
          $.ajax({
              url: "{{ getRoutes('master','customer-address') }}/data/"+id,
              type: "GET",
              headers: {
                'X-CSRF-TOKEN': '{{ csrf_token() }}'
              },
              success: function (response) {
                if(response.data){
                    $('#address').val(response.data.address);
                    $('#address_no').val(response.data.address_no);
                    $('#rt').val(response.data.rt);
                    $('#rw').val(response.data.rw);
                    $('#village').val(response.data.village);
                    $('#benchmark').val(response.data.benchmark);
                    $('#sub_district').val(response.data.sub_district);
                    $('#district').val(response.data.district);
                    $('#city_id').val(response.data.city_id);
                    $('#province_id').val(response.data.province_id).trigger('change');
                    $('#postal_code').val(response.data.postal_code);
                    $('#city_temp').val(response.data.city_id);
                    $('#province_temp').val(response.data.province_id);
                    $('#district_temp').val(response.data.district);
                }
              },
              error: function (xhr, status, error) {
                  showDialog.show(xhr.status + " " + status + " " + error, false);
              }
          });
      }
  });

  $('#item_id').change(function(){
      var id = $('#item_id').val();
      if (id) {
          $.ajax({
              url: "{{ getRoutes('master','item') }}/data/"+id,
              type: "GET",
              headers: {
                'X-CSRF-TOKEN': '{{ csrf_token() }}'
              },
              success: function (response) {
                if(response.data){
                    $('#price').val(response.data.price.replace(',','.'));
                }else{
                  $('#price').val(0);
                }
                calculation();
              },
              error: function (xhr, status, error) {
                  showDialog.show(xhr.status + " " + status + " " + error, false);
              }
          });
      }
  });

  $('#voucher_id').change(function(){
      var id = $('#voucher_id').val();
      if (id) {
          $.ajax({
              url: "{{ getRoutes('master','voucher') }}/data/"+id,
              type: "GET",
              headers: {
                'X-CSRF-TOKEN': '{{ csrf_token() }}'
              },
              success: function (response) {
                if(response.data){
                    $('#voucher').val(response.data.total.replace(',','.'));
                    $('#voucher_total').val(response.data.total.replace(',','.'));
                }else{
                  $('#voucher').val(0);
                  $('#voucher_total').val(0);
                }
                calculation();
              },
              error: function (xhr, status, error) {
                  showDialog.show(xhr.status + " " + status + " " + error, false);
              }
          });
      }else{
        $('#voucher').val(0);
        $('#voucher_total').val(0);
      }
  });

  $('#province_id').change(function(){
      $('#district').empty();
      $('#district').append('<option value="" selected> Chose </option>');
      $('#city_id').empty();
      $('#city_id').append('<option value="" selected> Chose </option>');
      var id = $('#province_id').val();
      if (id) {
          $.ajax({
              url: "{{ route('api.destination') }}?province="+id,
              type: "GET",
              headers: {
                'X-CSRF-TOKEN': '{{ csrf_token() }}'
              },
              success: function (response) {
                if(response.data){
                    for (var i in response.data){
                      $('#city_id').append('<option value="'+response.data[i].city +'">'+response.data[i].city+'</option>');
                    }
                    setTimeout(function(){
                      $('#city_id').val($('#city_temp').val()).trigger('change');
                    }, 115);
                }
              },
              error: function (xhr, status, error) {
                  showDialog.show(xhr.status + " " + status + " " + error, false);
              }
          });
      }
  });

  $('#city_id').change(function(){
      $('#district').empty();
      $('#district').append('<option value="" selected> Chose </option>');
      var id = $('#city_id').val();
      if (id) {
          $.ajax({
              url: "{{ route('api.destination') }}?province="+$('#province_id').val()+"&city="+id,
              type: "GET",
              headers: {
                'X-CSRF-TOKEN': '{{ csrf_token() }}'
              },
              success: function (response) {
                if(response.data){
                    for (var i in response.data){
                      $('#district').append('<option value="'+response.data[i].area +'">'+response.data[i].area+'</option>');
                    }
                    setTimeout(function(){
                      $('#district').val($('#district_temp').val()).trigger('change');
                    }, 115);
                }
              },
              error: function (xhr, status, error) {
                  showDialog.show(xhr.status + " " + status + " " + error, false);
              }
          });
      }
  });

  $("#quantity, #cost_handler, #price, #courier_cost, #insurance").change(function(){
    calculation();
  });
  $("#quantity, #cost_handler, #price, #courier_cost, #insurance").keyup(function(){
    calculation();
  });
  $("#quantity, #cost_handler, #price, #courier_cost, #insurance").keydown(function(){
    calculation();
  });
  $("#address_no, #rt, #rw, #village, #benchmark, #sub_district, #province_id, #city_id, #district, #postal_code").change(function(){
    destination();
  });
  $("#address_no, #rt, #rw, #village, #benchmark, #sub_district, #province_id, #city_id, #district, #postal_code").keyup(function(){
    destination();
  });
  $("#address_no, #rt, #rw, #village, #benchmark, #sub_district, #province_id, #city_id, #district, #postal_code").keydown(function(){
    destination();
  });

  function calculation(){
    var voucher = parseFloat($('#voucher').val().replace(',','.'));
    var quantity = parseFloat($('#quantity').val().replace(',','.'));
    var cost_handler = parseFloat($('#cost_handler').val().replace(',','.'));
    var price = parseFloat($('#price').val().replace(',','.'));
    var courier_cost = parseFloat($('#courier_cost').val().replace(',','.'));
    var insurance = parseFloat($('#insurance').val().replace(',','.'));
    var total = ((quantity*price)-voucher)+courier_cost+insurance+cost_handler;
    $('#transaction').val(parseFloat(total).toFixed(2));
    $('#transactiontemp').val("Rp. "+parseFloat(total).toFixed(2).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ","));
  }

  function destination(){
    var address_no = $('#address_no').val();
    var rt = $('#rt').val();
    var rw = $('#rw').val();
    var village = $('#village').val();
    var benchmark = $('#benchmark').val();
    var sub_district = $('#sub_district').val();
    var province_id = $('#province_id').val();
    var city_id = $('#city_id').val();
    var district = $('#district').val();
    var postal_code = $('#postal_code').val();
    var address = "";
    if(address_no != ''){
      address += ' '+address_no;
    }
    if(rt != ''){
      address += ' rt '+rt;
    }
    if(rw != ''){
      address += ' rw '+rw;
    }
    if(village != ''){
      address += ' desa '+village;
    }
    if(benchmark != ''){
      address += ' kel. '+benchmark;
    }
    if(sub_district != ''){
      address += ', '+sub_district;
    }
    if(district != ''){
      address += ', '+district;
    }
    if(city_id != ''){
      address += ', '+city_id;
    }
    if(province_id != ''){
      address += ', '+province_id;
    }
    if(postal_code != ''){
      address += ', '+postal_code;
    }

    $('#address').val(address);
  }

  function list_customer_address(id,id_cust){
    $('#customer_address_id').empty();
    $('#customer_address_id').append('<option value="" selected>  New Address</option>');
    if(id_cust){
      $.ajax({
          url: "{{ $path_customer_address }}/list?customer_id="+id_cust,
          type: "GET",
          headers: {
            'X-CSRF-TOKEN': '{{ csrf_token() }}'
          },
          success: function (response) {
            if(response.data){
              for (var i in response.data){
                $('#customer_address_id').append('<option value="'+response.data[i].id +'">'+response.data[i].id+'</option>');
              }
              if(id){
                setTimeout(function(){
                  $('#customer_address_id').val(id).trigger('change');
                }, 115);
              }
            }
          },
          error: function (xhr, status, error) {
              showDialog.show(xhr.status + " " + status + " " + error, false);
          }
      });
    }
  }

  function form_reset(){
      $('#form-input').trigger("reset");
      $("#form-input").attr("action", "{{ $path }}/save");
      $('#advertise_id, #item_id, #gender_id, #voucher_id, #market_id, #courier_id, #payment_type_id, #customer_address_id').val('').trigger('change');
      $('#transaction_date').datetimepicker('setDate', new Date());
      $('#voucher').val(0);
      $('#cost_handler').val(0);
      $('#transaction').val(0);
      $('#transactiontemp').val("Rp. 0");
      $('#modal-form').modal('show');
  }

  function refresh_table() {
      $('#datatable').DataTable().ajax.reload();
      $('#datatable').DataTable().responsive.recalc();
  }
</script>
@endsection
