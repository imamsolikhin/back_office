@extends('layout.default')

@section('content')
<div class="subheader py-2 {{ Metronic::printClasses('subheader', false) }}" id="kt_subheader">
  <div class="{{ Metronic::printClasses('subheader-container', false) }} d-flex align-items-center justify-content-between flex-wrap flex-sm-nowrap">
    <div class="d-flex align-items-baseline flex-wrap mr-5">
      <h5 class="text-dark font-weight-bold my-1 mr-5">
        {{ $module_alias }}
      </h5>
      <ul class="breadcrumb breadcrumb-transparent breadcrumb-dot font-weight-bold p-0 my-2 font-size-sm">
        <li class="breadcrumb-item">
          <a href="{{ route('dashboard') }}" class="text-muted">Dashboard</a>
        </li>
        <li class="breadcrumb-item">
          <a href="{{ $auth }}" class="text-muted">{{ $module_alias }}</a>
        </li>
        <li class="breadcrumb-item">
          <a href="#view" class="text-muted">View</a>
        </li>
      </ul>
    </div>
  </div>
</div>

@include('inc.error-list')
@include('inc.success-notif')
@include('inc.danger-notif')
<div class="card card-custom body-container">
  <div class="card-header bg-danger flex-wrap border-1 pt-1 pb-0 mb-2" style="min-height: 0;">
    <div class="card-title pt-1 pb-1">
      <h3 class="card-label font-weight-bolder text-white">{{ $module_alias }}
        <div class="text-muted pt-2 font-size-lg">show Datatable from table {{ $module_alias }}</div>
      </h3>
    </div>
    <div class="card-toolbar pt-1 pb-0">
      <div class="col-lg-5">
          <select class="form-control select2" id="company" name="company" style="width: 100%;">
            @php $data = list_model('Master','Company') @endphp
            @isset ($data)
              @foreach($data as $rs)
                @if($rs->id == sess_user('company_id'))
                  <option value="{{ $rs->id }}" selected>{{ $rs->name }}</option>
                @else
                  <option value="{{ $rs->id }}">{{ $rs->name }}</option>
                @endif
              @endforeach
            @endisset
         </select>
      </div>
      <a href="#" onclick="show_data('')" class="btn btn-primary font-weight-bolder" style="background-color: #1e1e2d;border-color: #0c8eff;">
        <span class="svg-icon svg-icon-md">
          <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
            <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
              <rect x="0" y="0" width="24" height="24"></rect>
              <circle fill="#000000" cx="9" cy="15" r="6"></circle>
              <path d="M8.8012943,7.00241953 C9.83837775,5.20768121 11.7781543,4 14,4 C17.3137085,4 20,6.6862915 20,10 C20,12.2218457 18.7923188,14.1616223 16.9975805,15.1987057 C16.9991904,15.1326658 17,15.0664274 17,15 C17,10.581722 13.418278,7 9,7 C8.93357256,7 8.86733422,7.00080962 8.8012943,7.00241953 Z" fill="#000000" opacity="0.3"></path>
            </g>
          </svg>
        </span>Add New
      </a>
      <div class="card-toolbar pt-1 pb-0">
        <div class="dropdown dropdown-inline px-2">
            <button type="button" class="btn btn-tool btn-lg" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <i class="la la-download text-white"></i> Reports
            </button>
            <div class="dropdown-menu dropdown-menu-sm dropdown-menu-right">
                <ul class="navi flex-column navi-hover py-2" id="btn_tools">
                    <li class="navi-header font-weight-bolder text-uppercase font-size-xs text-primary pb-2">
                        Export Tools
                    </li>
                    <li class="navi-item">
                        <a class="navi-link tool-action" onclick="export_delivery_note_g()">
                            <span class="navi-icon"><i class="la icon-lg la-print"></i></span>
                            <span class="navi-text">Delivery Note Global</span>
                        </a>
                    </li>
                    <li class="navi-item">
                        <a class="navi-link tool-action" onclick="export_delivery_note_d()">
                            <span class="navi-icon"><i class="la icon-lg la-print"></i></span>
                            <span class="navi-text">Delivery Note Detail</span>
                        </a>
                    </li>
                    <li class="navi-item">
                        <a class="navi-link tool-action" onclick="export_delivery_outstanding()">
                            <span class="navi-icon"><i class="la icon-lg la-print"></i></span>
                            <span class="navi-text">Delivery Outstanding</span>
                        </a>
                    </li>
                </ul>
            </div>
        </div>
      </div>
    </div>
  </div>
  <div class="card-body pt-1">
    <table class="table table-bordered table-hover w100" cellspacing="0" id="datatable" style="width: 1070px !important;"></table>
  </div>
  <div class="card-body pt-1">
    <table class="table table-bordered table-hover w100 dataTable no-footer dtr-inline" cellspacing="0" id="datatable_so">
      <thead>
        <tr>
            <th class="text-left text-white dt-body-nowrap pt-1 pb-1" style="font-size:1.2rem;">No</th>
            <th class="text-center text-white dt-body-nowrap pt-1 pb-1" style="font-size:1.2rem;">No Order</th>
            <th class="text-center text-white dt-body-nowrap pt-1 pb-1" style="font-size:1.2rem;">No Resi</th>
            <th class="text-center text-white dt-body-nowrap pt-1 pb-1" style="font-size:1.2rem;">No Refno</th>
            <th class="text-center text-white dt-body-nowrap pt-1 pb-1" style="font-size:1.2rem;">Customer</th>
            <th class="text-center text-white dt-body-nowrap pt-1 pb-1" style="font-size:1.2rem;">Jml Packet</th>
            <th class="text-center text-white dt-body-nowrap pt-1 pb-1" style="font-size:1.2rem;">Marketing</th>
            <th class="text-center text-white dt-body-nowrap pt-1 pb-1" style="font-size:1.2rem;">Action</th>
        </tr>
      </thead>
      <tbody></tbody>
    </table>
  </div>
</div>


<!-- Modal-->
<div class="modal fade" id="modal-so" data-backdrop="static" tabindex="-1" role="dialog" aria-labelledby="staticBackdrop" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header bg-danger pt-3 pb-3">
                <h5 class="modal-title text-white bold" id="modal">Sales Order {{ $module_alias }}</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <i aria-hidden="true" class="ki ki-close"></i>
                </button>
            </div>
            <div class="card-body pt-3">
                <div class="mb-1">
                    <div class="form-group row">
                        <label class="col-lg-4 col-form-label">{{ $module_alias }} ID</label>
                        <div class="col-lg-8">
                            <input type="text" class="form-control id" name="id" placeholder="AUTO" value="" readonly/>
                        </div>
                    </div>
                </div>
                <div class="mb-2">
                    <div class="form-group row">
                        <label class="col-lg-4 col-form-label">{{ $module_alias }} Date</label>
                        <div class="col-lg-8">
                            <div class="input-icon">
                                <input type="input" class="form-control datetimepicker-input delivery_date" name="delivery_date" placeholder="dd/mm/yyyy" data-date-format="dd-mm-yyyy hh:ii" value="<?php echo date('d-m-Y H:i'); ?>" readonly/>
                                <span>
                                    <i class="far fa-calendar-alt text-muted"></i>
                                </span>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="mb-2">
                    <div class="form-group row">
                        <label class="col-lg-4 col-form-label">Kurir</label>
                        <div class="col-lg-8">
                            <select class="form-control select2 courier_id" name="courier_id" style="width: 100%;" disabled>
                              <option value="" selected>Chose </option>
                              @php $data = list_model('Master','Courier') @endphp
                              @isset ($data)
                                @foreach($data as $rs)
                                 <option value="{{ $rs->id }}">{{ $rs->name }}</option>
                                @endforeach
                              @endisset
                           </select>
                        </div>
                    </div>
                </div>
                <div class="mb-1">
                    <div class="form-group row">
                        <label class="col-lg-4 col-form-label">{{ $module_alias }} RefNo</label>
                        <div class="col-lg-8">
                            <input type="text" class="form-control delivery_refno" name="delivery_refno" placeholder="Enter Name" value="" readonly/>
                        </div>
                    </div>
                </div>
                <div class="mb-1">
                    <div class="form-group row">
                        <label class="col-lg-4 col-form-label">{{ $module_alias }} RefNo</label>
                        <div class="col-lg-8">
                            <input type="text" class="form-control delivery_refno" name="delivery_refno" placeholder="Enter Name" value="" readonly/>
                        </div>
                    </div>
                </div>
                <div class="mb-1">
                    <div class="form-group row">
                        <label class="col-lg-4 col-form-label">{{ $module_alias }} Remark</label>
                        <div class="col-lg-8">
                            <input type="text" class="form-control delivery_remark" name="delivery_remark" placeholder="Enter Name" value="" readonly/>
                        </div>
                    </div>
                </div>
                <div class="card card-custom pt-3">
                  <div class="card-header bg-danger flex-wrap border-1 pt-1 pb-0 mb-2" style="min-height: 0;">
                    <div class="card-title pt-1 pb-1">
                      <h3 class="card-label font-weight-bolder text-white">SO Import</h3>
                    </div>
                    <div class="card-toolbar pt-1 pb-0 col-lg-10">
                        <input type="text" class="form-control col-lg-7" id="query_string" name="query_string" placeholder="No Resi" value=""/>
                        <div class="col-lg-3">
                            <select class="form-control select2" id="col_searching" name="col_searching" style="width: 100%;">
                              <option value="id" selected>No Order </option>
                              <option value="delivery_no">No Resi </option>
                              <option value="delivery_refno">No Ref </option>
                           </select>
                        </div>
                        <button type="button" id="btn_query_string" class="btn btn-info ml-2">Import SO</button>
                    </div>
                  </div>
                  <div class="card-body pt-1">
                    <table class="table table-bordered table-hover w100 dataTable no-footer dtr-inline" cellspacing="0" id="datatable_result">
                      <thead>
                        <tr>
                            <th class="text-left text-white dt-body-nowrap pt-1 pb-1" style="font-size:1.2rem;">No</th>
                            <th class="text-center text-white dt-body-nowrap pt-1 pb-1" style="font-size:1.2rem;">No Order</th>
                            <th class="text-center text-white dt-body-nowrap pt-1 pb-1" style="font-size:1.2rem;">No Resi</th>
                            <th class="text-center text-white dt-body-nowrap pt-1 pb-1" style="font-size:1.2rem;">No Refno</th>
                            <th class="text-center text-white dt-body-nowrap pt-1 pb-1" style="font-size:1.2rem;">Customer</th>
                            <th class="text-center text-white dt-body-nowrap pt-1 pb-1" style="font-size:1.2rem;">Jml Packet</th>
                            <th class="text-center text-white dt-body-nowrap pt-1 pb-1" style="font-size:1.2rem;">Marketing</th>
                            <th class="text-center text-white dt-body-nowrap pt-1 pb-1" style="font-size:1.2rem;">Confirm</th>
                        </tr>
                      </thead>
                      <tbody></tbody>
                    </table>
                  </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="modal-form" data-backdrop="static" tabindex="-1" role="dialog" aria-labelledby="staticBackdrop" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header bg-danger pt-3 pb-3">
                <h5 class="modal-title text-white bold" id="modal">Form {{ $module_alias }}</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <i aria-hidden="true" class="ki ki-close"></i>
                </button>
            </div>
            <form class="form" id="form-input" action="{{ $path }}/save" method="POST">
              {!! csrf_field() !!}
                <div class="card-body pt-3">
                    <div class="mb-1">
                        <div class="form-group row">
                            <label class="col-lg-4 col-form-label">{{ $module_alias }} ID</label>
                            <div class="col-lg-8">
                                <input type="text" class="form-control" id="id" name="id" placeholder="AUTO" value="" readonly/>
                            </div>
                        </div>
                    </div>
                    <div class="mb-1">
                        <div class="form-group row">
                            <label class="col-lg-4 col-form-label">Company ID</label>
                            <div class="col-lg-8">
                                <input type="text" class="form-control" id="company_id" name="company_id" placeholder="AUTO" value="" readonly/>
                            </div>
                        </div>
                    </div>
                    <div class="mb-2">
                        <div class="form-group row">
                            <label class="col-lg-4 col-form-label">{{ $module_alias }} Date</label>
                            <div class="col-lg-8">
                                <div class="input-icon">
                                    <input type="input" class="form-control datetimepicker-input" id="delivery_date" name="delivery_date" placeholder="dd/mm/yyyy" data-date-format="dd-mm-yyyy hh:ii" value="<?php echo date('d-m-Y H:i'); ?>"/>
                                    <span>
                                        <i class="far fa-calendar-alt text-muted"></i>
                                    </span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="mb-2">
                        <div class="form-group row">
                            <label class="col-lg-4 col-form-label">Kurir</label>
                            <div class="col-lg-8">
                                <select class="form-control select2" id="courier_id" name="courier_id" style="width: 100%;">
                                  <option value="" selected>Chose </option>
                                  @php $data = list_model('Master','Courier') @endphp
                                  @isset ($data)
                                    @foreach($data as $rs)
                                     <option value="{{ $rs->id }}">{{ $rs->name }}</option>
                                    @endforeach
                                  @endisset
                               </select>
                            </div>
                        </div>
                    </div>
                    <div class="mb-1">
                        <div class="form-group row">
                            <label class="col-lg-4 col-form-label">{{ $module_alias }} RefNo</label>
                            <div class="col-lg-8">
                                <input type="text" class="form-control" id="delivery_refno" name="delivery_refno" placeholder="Enter Name" value=""/>
                            </div>
                        </div>
                    </div>
                    <div class="mb-1">
                        <div class="form-group row">
                            <label class="col-lg-4 col-form-label">{{ $module_alias }} Remark</label>
                            <div class="col-lg-8">
                                <input type="text" class="form-control" id="delivery_remark" name="delivery_remark" placeholder="Enter Name" value=""/>
                            </div>
                        </div>
                    </div>
                    <div class="mb-2">
                        <div class="form-group row">
                            <label class="col-lg-4 col-form-label"></label>
                            <div class="col-lg-8">
                                <button type="submit" class="btn btn-success font-weight-bold">Save</button>
                                <button type="button" data-dismiss="modal" class="btn btn-danger">Cancel</button>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
@endsection

{{-- Styles Section --}}
@section('styles')
<link rel="stylesheet" href="{{ config('app.url') }}plugins/custom/datatables/datatables.bundle.css">
<link rel="stylesheet" href="{{ config('app.url') }}css/inject.css">
<style>
#datatable_so th, #datatable_so td {
    padding: 0.2rem;
    vertical-align: top;
    border-top: 1px solid #ECF0F3;
}
</style>
@endsection

{{-- Scripts Section --}}
@section('scripts')
@include ('inc.confirm-delete-modal')
<script src="{{ config('app.url') }}plugins/custom/datatables/datatables.bundle.js"></script>
<script src="{{ config('app.url') }}js/inject.js"></script>
<script type="text/javascript">
  $(document).ready(function() {
    $('.select2').select2({});
    $('.datetimepicker-input').datetimepicker({
        format: 'dd-mm-yyyy hh:ii'
    });

    $("div.datesearchbox").html('<div class="input-group"> <div class="input-group-addon"> <i class="glyphicon glyphicon-calendar"></i> </div><input type="text" class="form-control text-center datetimepicker-input" id="datesearch_start" placeholder="Search by date range" value="{{date('d-m-Y H:i', strtotime($dashboard_startdate))}}">&nbsp;to&nbsp;<input type="text" class="form-control  text-center datetimepicker-input" id="datesearch_end" placeholder="Search by date range" value="{{date('d-m-Y H:i', strtotime($dashboard_enddate))}}"></div>');
    $("#datesearch_start, #datesearch_end").attr("readonly",true);
    $("#datesearch_start, #datesearch_end").change(function(){
      refresh_table();
    });
    $('#datesearch_start, #datesearch_end').datetimepicker({
        format: 'dd-mm-yyyy hh:ii',
        inline: true,
     });
  });

  var table = $('#datatable').dataTable({
    pageLength: 5,
    responsive: true,
    searchDelay: 800,
    processing: true,
    serverSide: true,
    searching: true,
    lengthMenu: [[5, 10, 25, 50, 100, 200], [5, 10, 25, 50, 100, 200]],
    ajax: {
      method: 'POST',
      url : '{{ $path }}/list',
      headers: {
        'X-CSRF-TOKEN': '{{ csrf_token() }}'
      },
      data: function (d) {
        d.from_date = formatdate($("#datesearch_start").val());
        d.to_date = formatdate($(" #datesearch_end").val(),true);
        d.company_id = $("#company").val();
      }
    },
    columns: [
      {title: "No", data: 'DT_RowIndex', orderable: false, searchable: false, autoHide: false},
      {title: "{{ $module_alias }} ID", data: 'id', defaultContent: '-', class: 'text-center dt-body-nowrap', autohide: true},
      {title: "Delivery Date", data: 'delivery_date', defaultContent: '-', class: 'text-center dt-body-nowrap', autohide: true},
      {title: "Courier Name", data: 'courier_name', defaultContent: '-', class: 'text-center dt-body-nowrap', autohide: true},
      {title: "Total Packet", data: 'total_packet', defaultContent: '-', class: 'text-center dt-body-nowrap', autohide: true},
      {title: "Total Quantity", data: 'total_quantity', defaultContent: '-', class: 'text-center dt-body-nowrap', autohide: true},
      {title: "Author", data: 'author', defaultContent: '-', class: 'text-center dt-body-nowrap', autohide: true},
      {title: "Actions", data: 'action', orderable: false, responsivePriority: -1},
    ],
    bStateSave: true,
    dom:  "<'row'<'col-sm-4'l><'col-sm-5' <'datesearchbox'>><'col-sm-3'f>>" +
          "<'row'<'col-sm-12'tr>>" +
          "<'row'<'col-sm-5'i><'col-sm-7'p>>",
    columnDefs: [
      {
        targets: [0,-1],
        className: 'text-center visible dt-body-nowrap'
      },
    ],
    initComplete: function() {
      $('.tl-tip').tooltip();
    }
  });

  $('#datatable tbody').on( 'click', 'tr', function () {
    $("#datatable_so tbody>tr").remove();
    if ($(this).hasClass('selected')) {
      table.$('tr.selected').removeClass('selected');
      $(this).removeClass('selected');
      $(".str_default").text('-');
    } else {
      table.$('tr.selected').removeClass('selected');
      $(this).addClass('selected');
      var id = $(this).find("td:eq(1)").html();
      $(".madule_name").text(" : "+$(this).find("td:eq(1)").html()+"[RESI:"+$(this).find("td:eq(2)").html()+"]");
      // $("#datatable_so").loading("start");
      list_so(id);
    }
  });

  function list_so(id){
    var query = $('#query_string').val();
    var list = query.split(',');

    $("#datatable_so tbody>tr").remove();
    $.ajax({
        method: 'POST',
        url : '{{ $path_detail }}/list?dln='+id+'&company_id='+$("#company").val(),
        headers: {
          'X-CSRF-TOKEN': '{{ csrf_token() }}'
        },
        success: function (response) {
          if(response.data){
              for (i in response.data){
                var rs = response.data[i];
                var table = document.getElementById("datatable_so").getElementsByTagName('tbody')[0];
                var row = table.insertRow(-1);
                var delivery_detail = "'"+rs.delivery_detail+"'";
                var delivery_header = "'"+rs.delivery_header+"'";
                row.insertCell(0).innerHTML = '<label class="col-12 text-left pt-1 pb-1">'+(Number(i)+1)+'</label>';
                row.insertCell(1).innerHTML = '<label class="col-12 text-center pt-1 pb-1">'+rs.id+'</label>';
                row.insertCell(2).innerHTML = '<label class="col-12 text-center pt-1 pb-1">'+rs.delivery_no+'</label>';
                row.insertCell(3).innerHTML = '<label class="col-12 text-center pt-1 pb-1">'+rs.delivery_refno+'</label>';
                row.insertCell(4).innerHTML = '<label class="col-12 text-center pt-1 pb-1">'+rs.gender_name+' '+rs.full_name+'</label>';
                row.insertCell(5).innerHTML = '<label class="col-12 text-center pt-1 pb-1">'+rs.quantity+'</label>';
                row.insertCell(6).innerHTML = '<label class="col-12 text-center pt-1 pb-1">'+rs.author+'</label>';
                if(!rs.delivery_status){
                    row.insertCell(7).innerHTML = '<center><a onclick="del_so(this,'+delivery_header+','+delivery_detail+')" class="btn btn-outline-danger btn-sm"><i class="flaticon-delete"></i>del</a></center>';
                }else{
                  row.insertCell(7).innerHTML = '<center><a class="btn btn-outline-success btn-sm"><i class="flaticon-lock"></i>lock</a></center>';
                }

              }
          }
        },
        error: function (xhr, status, error) {
            showDialog.show(xhr.status + " " + status + " " + error, false);
        }
    });
  }

  function del_so(btn,header,id){
    var query = $('#query_string').val();
    var list = query.split(',');
    $.ajax({
        method: 'DELETE',
        url : '{{ $path_detail }}/'+id,
        headers: {
          'X-CSRF-TOKEN': '{{ csrf_token() }}'
        },
        success: function (response) {
          if(response.data){
            list_so(header);
          }
        },
        error: function (xhr, status, error) {
            showDialog.show(xhr.status + " " + status + " " + error, false);
        }
    });
  }

  function show_so(id = "") {
      if (id !== "") {
          $.ajax({
              url: "{{ $path }}/data/" + id,
              type: "GET",
              headers: {
                'X-CSRF-TOKEN': '{{ csrf_token() }}'
              },
              success: function (response) {
                if(!response){
                  showDialog.show("data "+id+" not found");
                  return;
                }
                $('.id').val(response.data.id);
                $('.courier_id').val(response.data.courier_id).trigger('change');
                $('.delivery_refno').val(response.data.delivery_refno);
                $('.delivery_remark').val(response.data.delivery_remark);
                $('.delivery_date').val(response.data.delivery_date).datetimepicker('update', response.data.delivery_date);
                $('#modal-so').modal('show');
              },
              error: function (xhr, status, error) {
                  showDialog.show(xhr.status + " " + status + " " + error, false);
              }
          });
      }
  }

  function show_data(id = "") {
      if (id !== "") {
          $.ajax({
              url: "{{ $path }}/data/" + id,
              type: "GET",
              headers: {
                'X-CSRF-TOKEN': '{{ csrf_token() }}'
              },
              success: function (response) {
                if(!response){
                  showDialog.show("data "+id+" not found");
                  return;
                }
                  $('#form-input').trigger("reset");
                  $("#form-input").attr("action", "{{ $path }}/update/"+id);

                  $('#id').val(response.data.id);
                  $('#company_id').val(response.data.company_id);
                  $('#courier_id').val(response.data.courier_id).trigger('change');
                  $('#delivery_refno').val(response.data.delivery_refno);
                  $('#delivery_remark').val(response.data.delivery_remark);
                  $('#delivery_date').val(response.data.delivery_date).datetimepicker('update', response.data.delivery_date);
                  $('#modal-form').modal('show');
              },
              error: function (xhr, status, error) {
                  showDialog.show(xhr.status + " " + status + " " + error, false);
              }
          });
      } else {
          $('#form-input').trigger("reset");
          $("#form-input").attr("action", "{{ $path }}/save");
          $('#method').val("POST");
          $('#company_id').val($('#company').val());
          $('#modal-form').modal('show');
      }
  }

  $('#btn_query_string').click(function(){
    var query = $('#query_string').val();
    var list = query.split(',');
    $("#datatable_result tbody>tr").remove();
    $.ajax({
        method: 'POST',
        url : '{{ $path_detail }}/detail',
        headers: {
          'X-CSRF-TOKEN': '{{ csrf_token() }}'
        },
        data: {
          list_so:list,
          col_searching:$('#col_searching').val(),
          courier_id:$(".courier_id").val(),
          company_id:$("#company").val()
        },
        success: function (response) {
          if(response.data){
              for (i in response.data){
                var rs = response.data[i];
                var table = document.getElementById("datatable_result").getElementsByTagName('tbody')[0];
                var row = table.insertRow(-1);
                var order = "'"+$('.id').val()+"'";
                var so_order = "'"+rs.id+"'";
                row.insertCell(0).innerHTML = '<label class="col-12 text-left pt-1 pb-1">'+(Number(i)+1)+'</label>';
                row.insertCell(1).innerHTML = '<label class="col-12 text-center pt-1 pb-1">'+rs.id+'</label>';
                row.insertCell(2).innerHTML = '<label class="col-12 text-center pt-1 pb-1">'+rs.delivery_no+'</label>';
                row.insertCell(3).innerHTML = '<label class="col-12 text-center pt-1 pb-1">'+rs.delivery_refno+'</label>';
                row.insertCell(4).innerHTML = '<label class="col-12 text-center pt-1 pb-1">'+rs.gender_name+' '+rs.full_name+'</label>';
                row.insertCell(5).innerHTML = '<label class="col-12 text-center pt-1 pb-1">'+rs.quantity+'</label>';
                row.insertCell(6).innerHTML = '<label class="col-12 text-center pt-1 pb-1">'+rs.author+'</label>';
                row.insertCell(7).innerHTML = '<a onclick="update_data(this,'+order+','+so_order+')" class="btn btn-outline-success"><i class="flaticon2-poll-symbol"></i>process</a>';
              }
          }

        },
        error: function (xhr, status, error) {
            showDialog.show(xhr.status + " " + status + " " + error, false);
        }
    });
  });

  function update_data(btn,order,so_order){
    $("#modal-query").loading("start");
    $.ajax({
        url: "{{ $path_detail }}/save?dln="+order+"&so_order="+so_order,
        type: "POST",
        headers: {
          'X-CSRF-TOKEN': '{{ csrf_token() }}'
        },
        success: function (response) {
          var row = btn.parentNode.parentNode;
          row.parentNode.removeChild(row);
          refresh_table();
          $("#modal-query").loading("stop");
        },
        error: function (xhr, status, error) {
            $("#modal-query").loading("stop");
            showDialog.show(xhr.status + " " + status + " " + error, false);
        }
    });
  }

  function export_copy(id,format){
    $.ajax({
        url : '{{ $path_detail }}/list?dln='+id+'&company_id='+$("#company").val(),
        type: "POST",
        headers: {
          'X-CSRF-TOKEN': '{{ csrf_token() }}'
        },
        success: function (response) {
          var no = 0;
          var format = '';
          format += "*Laporan Pengiriman Harian: " +format+"*";
          format += "<br> *Nama Admin: {{sess_user('name')}}*";
          format += "<br> *Department: "+$("#company").find("option:selected").text()+"*";
          format += "<br> *========================*";

          var title = [];
          response.data.forEach(function (a) {
              if(title.indexOf(a.courier_name) === -1){
                  title.push(a.courier_name);
              }
          });
          var author = [];
          response.data.forEach(function (a) {
              if(author.indexOf(a.author) === -1){
                  author.push(a.author);
              }
          });

          for (var i in title){
            var titles = title[i]
            if(!title[i]){
              titles = "NOT SET";
            }
            var total_botol = 0;
            var total_packet = 0;
            format += "<br>";
            format += "*"+(Number(i)+1)+".) Kurir: "+titles+"*";
            var closing = "";
            for (var a in author){
              closing += "<br>  *"+(Number(a)+1)+".) Marketing: "+author[a]+"*";
              closing += "<br>";
              var no_closing = 0;
              for (var x in response.data){
                if(response.data[x].courier_name == title[i] && response.data[x].author == author[a]){
                  var voucher = parseFloat(response.data[x].voucher_total);
                  var quantity = parseFloat(response.data[x].quantity);
                  var cost_handler = parseFloat(response.data[x].cost_handler);
                  var price = parseFloat(response.data[x].price);
                  var courier_cost = parseFloat(response.data[x].courier_cost);
                  var insurance = parseFloat(response.data[x].insurance);
                  total_botol = (total_botol+quantity);
                  total_packet = (total_packet+1);

                  var total_product = ((quantity*price)-voucher);
                  var total = ((quantity*price)-voucher)+courier_cost+insurance+cost_handler;
                  var totaltemp = "Rp. "+parseFloat(total).toFixed(2).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
                  var voucher_total = "Rp. "+parseFloat(voucher).toFixed(2).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
                  var courier_total = "Rp. "+parseFloat(courier_cost+insurance).toFixed(2).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");

                  no_closing = no_closing+1;
                  closing += "   "+no_closing+".) Resi   : "+response.data[x].delivery_no+"<br>";
                  closing += "         Cust      : "+response.data[x].full_name+"<br>";
                  closing += "         Telp      : "+response.data[x].phone+" Botol<br>";
                  closing += "         Jumlah    : "+parseInt(response.data[x].quantity)+"<br>";
                  closing += "         Voucher   : "+voucher_total+"<br>";
                  closing += "         Total   : Rp. "+addCommas(total_product)+"<br>";
                  closing += "         ongkir   : "+courier_total+"<br>";
                  closing += "         Grand Total  : "+totaltemp+"<br>";
                }
              }
            }
            format += "<br> *#Total Paket: "+addCommas(total_packet);
            format += "<br> *#Total Botol: "+addCommas(total_botol);
            format += closing;
            closing = "";
          }
          format += "<br>";
          format += "<br>   *(Replace this note!!)*";
          format += "<br>";
          format += "<br>*Terimakasih*";
          format += "<br>*CS:{{sess_user('name')}}*";
          copyToClipboard(format);
          var img_url = '{{ENV('APP_URL')}}';
          var options = {
            title:'Laporan Pengiriman Harian',
            // imgUrl:img_url,
          };
          showDialog.show(format,options);
        },
        error: function (xhr, status, error) {
            showDialog.show(xhr.status + " " + status + " " + error, false);
        }
    });
  }

  $("#company").change(function(){
    $('#courier_id').empty();
    $('#courier_id').append('<option value="">loading...</option>');
    $('.courier_id').empty();
    $('.courier_id').append('<option value="">loading...</option>');
    $.ajax({
        url: "{{ getRoutes('master','courier') }}/list?company_id=" + $("#company").val(),
        type: "GET",
        success: function (response) {
          $('#courier_id').empty();
          $('#courier_id').append('<option value="">Chose Courier</option>');
          $('.courier_id').empty();
          $('.courier_id').append('<option value="">Chose Courier</option>');
          if(response.data){
            for (var i in response.data){
                var data = response.data[i];
                $('#courier_id').append('<option value="'+data.id +'" title="'+data.name+'">'+data.name+'</option>');
                $('.courier_id').append('<option value="'+data.id +'" title="'+data.name+'">'+data.name+'</option>');
            }
          }
        },
        error: function (xhr, status, error) {
        }
    });
    refresh_table();
  });

  function export_delivery_note_g(id) {
    var from_date = formatdate($("#datesearch_start").val());
    var to_date = formatdate($(" #datesearch_end").val());
    if(from_date=="" && to_date==""){
      showDialog.show('<h4 class="text-danger text-bold">Oops, Range date has been set!</h4>');
      return;
    }

    var company_id = $("#company").val();
    var url = "";
    if(company_id != ""){
      url += "&company_id=" + company_id;
    }
    url += "&transaction_status=Delivery Courier";
    if(id){
      url += "&delivery_id="+id;
    }
    window.open("{{ ENV('API_RPT_URL') }}/delivery-note-global.php?from_date="+from_date+"&to_date="+to_date+url);
  }

  function export_delivery_note_d(id) {
    var from_date = formatdate($("#datesearch_start").val());
    var to_date = formatdate($(" #datesearch_end").val());
    if(from_date=="" && to_date==""){
      showDialog.show('<h4 class="text-danger text-bold">Oops, Range date has been set!</h4>');
      return;
    }

    var company_id = $("#company").val();
    var url = "";
    if(company_id != ""){
      url += "&company_id=" + company_id;
    }
    url += "&transaction_status=Delivery Courier";

    if(id){
      url += "&delivery_id="+id;
    }
    window.open("{{ ENV('API_RPT_URL') }}/delivery-note-detail.php?from_date="+from_date+"&to_date="+to_date+url);
  }

  function export_delivery_outstanding(id) {
    var from_date = formatdate($("#datesearch_start").val());
    var to_date = formatdate($(" #datesearch_end").val());
    if(from_date=="" && to_date==""){
      showDialog.show('<h4 class="text-danger text-bold">Oops, Range date has been set!</h4>');
      return;
    }

    var company_id = $("#company").val();
    var url = "";
    if(company_id != ""){
      url += "&company_id=" + company_id;
    }
    url += "&transaction_status=Delivery Courier";

    if(id){
      url += "&delivery_id="+id;
    }
    window.open("{{ ENV('API_RPT_URL') }}/delivery-note-outstanding.php?from_date="+from_date+"&to_date="+to_date+url);
  }

  function refresh_table() {
      $('#datatable').DataTable().ajax.reload();
      $('#datatable').DataTable().responsive.recalc();
  }
</script>
@endsection
