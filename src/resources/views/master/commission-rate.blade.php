@extends('layout.default')

@section('content')
<div class="subheader py-2 {{ Metronic::printClasses('subheader', false) }}" id="kt_subheader">
  <div class="{{ Metronic::printClasses('subheader-container', false) }} d-flex align-items-center justify-content-between flex-wrap flex-sm-nowrap">
    <div class="d-flex align-items-baseline flex-wrap mr-5">
      <h5 class="text-dark font-weight-bold my-1 mr-5">
        {{ $module }}
      </h5>
      <ul class="breadcrumb breadcrumb-transparent breadcrumb-dot font-weight-bold p-0 my-2 font-size-sm">
        <li class="breadcrumb-item">
          <a href="{{ route('dashboard') }}" class="text-muted">Dashboard</a>
        </li>
        <li class="breadcrumb-item">
          <a href="{{ $auth }}" class="text-muted">{{ $module }}</a>
        </li>
        <li class="breadcrumb-item">
          <a href="#view" class="text-muted">View</a>
        </li>
      </ul>
    </div>
  </div>
</div>

@include('inc.error-list')
@include('inc.success-notif')
@include('inc.danger-notif')
<div class="card card-custom body-container">
  <div class="card-header bg-danger flex-wrap border-1 pt-1 pb-0 mb-2" style="min-height: 0;">
    <div class="card-title pt-1 pb-1">
      <h3 class="card-label font-weight-bolder text-white">{{ $module }}
        <div class="text-muted pt-2 font-size-lg">show Datatable from table {{ $module }}</div>
      </h3>
    </div>
    <div class="card-toolbar pt-1 pb-0 col-lg-8 d-flex flex-row">
      <div class="col-lg-8">
        <select class="form-control select2" id="company" name="company" style="width: 100%;">
          @php $dt = list_model('Master','Company') @endphp
          @isset ($dt)
            @foreach($dt as $rs)
              @if($rs->id == sess_user('company_id'))
                <option value="{{ $rs->id }}" selected>{{ $rs->name }}</option>
              @else
                <option value="{{ $rs->id }}">{{ $rs->name }}</option>
              @endif
            @endforeach
          @endisset
       </select>
      </div>
      <a href="#" onclick="show_data('')" class="btn btn-primary font-weight-bolder" style="background-color: #1e1e2d;border-color: #0c8eff;">
        <span class="svg-icon svg-icon-md">
          <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
            <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
              <rect x="0" y="0" width="24" height="24"></rect>
              <circle fill="#000000" cx="9" cy="15" r="6"></circle>
              <path d="M8.8012943,7.00241953 C9.83837775,5.20768121 11.7781543,4 14,4 C17.3137085,4 20,6.6862915 20,10 C20,12.2218457 18.7923188,14.1616223 16.9975805,15.1987057 C16.9991904,15.1326658 17,15.0664274 17,15 C17,10.581722 13.418278,7 9,7 C8.93357256,7 8.86733422,7.00080962 8.8012943,7.00241953 Z" fill="#000000" opacity="0.3"></path>
            </g>
          </svg>
        </span>Add New
      </a>
      <div class="card-toolbar pt-1 pb-0">
        <div class="dropdown dropdown-inline px-2">
            <button type="button" class="btn btn-tool btn-lg" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <i class="la la-download text-white"></i> Tools
            </button>
            <div class="dropdown-menu dropdown-menu-sm dropdown-menu-right">
                <ul class="navi flex-column navi-hover py-2" id="btn_tools">
                    <li class="navi-header font-weight-bolder text-uppercase font-size-xs text-primary pb-2">
                        Export Tools
                    </li>
                    <li class="navi-item">
                        <a class="navi-link tool-action" onclick="export_pdf(1)">
                            <span class="navi-icon"><i class="la la-print"></i></span>
                            <span class="navi-text">Print Comissions</span>
                        </a>
                    </li>
                </ul>
            </div>
        </div>
      </div>
    </div>
  </div>
  <div class="card-body pt-1">
    <table class="table table-bordered table-hover w100" cellspacing="0" id="datatable" style="width: 1070px !important;"></table>
  </div>
     <div class="card-body pt-0 datatable_detail">
       <div class="d-flex col-md-12">
    				<div class="col-md-6">
              <div class="table-responsive">
                <div class="card-header bg-danger p-2 pb-0">
                  <div class="card-title pb-0 mb-0">
                      <h3 class="text-white pb-0 mb-0" style="font-weight:900"><div id="color-picker-1" class="mx-auto">Sales Order Commission Rate <label class="module_name"></label></div></h3>
                  </div>
                </div>
              </div>
              <div class="table-responsive">
                <table id="datatable_sor" class="datatable_detail table table-bordered table-hover w100 dataTable no-footer dtr-inline" cellspacing="0">
                    <thead>
                      <tr>
                          <th class="text-left text-white dt-body-nowrap pt-1 pb-1" style="font-size:1.2rem;">No</th>
                          <th class="text-center text-white dt-body-nowrap pt-1 pb-1" style="font-size:1.2rem;">No Order</th>
                          <th class="text-center text-white dt-body-nowrap pt-1 pb-1" style="font-size:1.2rem;">No Resi</th>
                          <th class="text-center text-white dt-body-nowrap pt-1 pb-1" style="font-size:1.2rem;">Transaction</th>
                          <th class="text-center text-white dt-body-nowrap pt-1 pb-1" style="font-size:1.2rem;">Customer</th>
                          <th class="text-center text-white dt-body-nowrap pt-1 pb-1" style="font-size:1.2rem;">Jml Packet</th>
                          <th class="text-center text-white dt-body-nowrap pt-1 pb-1" style="font-size:1.2rem;">Bank</th>
                          <th class="text-center text-white dt-body-nowrap pt-1 pb-1" style="font-size:1.2rem;">Marketing</th>
                          <th class="text-center text-white dt-body-nowrap pt-1 pb-1" style="font-size:1.2rem;">Action</th>
                      </tr>
                    </thead>
                    <tbody></tbody>
                  </table>
              </div>
    				</div>
    				<div class="col-md-6">
              <div class="table-responsive">
                <div class="card-header bg-danger p-2 pb-0">
                  <div class="card-title pb-0 mb-0">
                      <h3 class="text-white pb-0 mb-0" style="font-weight:900"><div id="color-picker-1" class="mx-auto">Sales Order White List <label class="module_name"></label></div></h3>
                  </div>
                </div>
              </div>
              <div class="table-responsive">
                <table id="datatable_sow" class="datatable_detail table table-bordered table-hover w100 dataTable no-footer dtr-inline" cellspacing="0">
                    <thead>
                      <tr>
                          <th class="text-left text-white dt-body-nowrap pt-1 pb-1" style="font-size:1.2rem;">No</th>
                          <th class="text-center text-white dt-body-nowrap pt-1 pb-1" style="font-size:1.2rem;">No Order</th>
                          <th class="text-center text-white dt-body-nowrap pt-1 pb-1" style="font-size:1.2rem;">No Resi</th>
                          <th class="text-center text-white dt-body-nowrap pt-1 pb-1" style="font-size:1.2rem;">Transaction</th>
                          <th class="text-center text-white dt-body-nowrap pt-1 pb-1" style="font-size:1.2rem;">Customer</th>
                          <th class="text-center text-white dt-body-nowrap pt-1 pb-1" style="font-size:1.2rem;">Jml Packet</th>
                          <th class="text-center text-white dt-body-nowrap pt-1 pb-1" style="font-size:1.2rem;">Courier</th>
                          <th class="text-center text-white dt-body-nowrap pt-1 pb-1" style="font-size:1.2rem;">Marketing</th>
                          <th class="text-center text-white dt-body-nowrap pt-1 pb-1" style="font-size:1.2rem;">Action</th>
                      </tr>
                    </thead>
                    <tbody></tbody>
                  </table>
              </div>
    				</div>
       </div>
     </div>
</div>


<!-- Modal-->
<div class="modal fade" id="modal-form" data-backdrop="static" tabindex="-1" role="dialog" aria-labelledby="staticBackdrop" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header bg-danger pt-3 pb-3">
                <h5 class="modal-title text-white bold" id="modal">Form {{ $module }}</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <i aria-hidden="true" class="ki ki-close"></i>
                </button>
            </div>
            <form class="form" id="form-input" action="{{ $path }}/save" method="POST">
              {!! csrf_field() !!}
              <input type="hidden" class="form-control" id="method" name="_method" placeholder="Enter method" value="POST"/>
                <div class="card-body pt-3">
                    <div class="mb-1">
                        <div class="form-group row">
                            <label class="col-lg-4 col-form-label">{{ $module }} ID</label>
                            <div class="col-lg-8">
                                <input type="text" class="form-control" id="id" name="id" placeholder="AUTO" value="" readonly />
                            </div>
                        </div>
                    </div>
                    <div class="mb-2">
                        <div class="form-group row">
                            <label class="col-lg-4 col-form-label">Company</label>
                            <div class="col-lg-8">
                                <select class="form-control select2" id="company_id" name="company_id" style="width: 100%;">
                                  <option value="" selected>Chose Marketing</option>
                                  @php $dt = list_model('Master','Company') @endphp
                                  @isset ($dt)
                                    @foreach($dt as $rs)
                                      @if($rs->id == sess_user('company_id'))
                                        <option value="{{ $rs->id }}" selected>{{ $rs->name }}</option>
                                      @else
                                        <option value="{{ $rs->id }}">{{ $rs->id }} - {{ $rs->name }}</option>
                                      @endif
                                    @endforeach
                                  @endisset
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="mb-2">
                        <div class="form-group row">
                            <label class="col-lg-4 col-form-label">Marketing</label>
                            <div class="col-lg-8">
                                <select class="form-control select2" id="sales_id" name="sales_id" style="width: 100%;">
                                  <option value="" selected>Chose Marketing</option>
                                   @php $dt = listUser('all') @endphp
                                   @isset ($dt)
                                     @foreach($dt as $rs)
                                      <option value="{{ $rs->id }}">{{ $rs->company_id }}-{{ $rs->name }}</option>
                                     @endforeach
                                   @endisset
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="mb-2">
                        <div class="form-group row">
                            <label class="col-lg-4 col-form-label">Start Date</label>
                            <div class="col-lg-8">
                                <div class="input-icon">
                                    <input type="input" class="form-control datetimepicker-input" id="start_date" name="start_date" placeholder="dd/mm/yyyy" data-date-format="dd-mm-yyyy hh:ii" value="<?php echo date('d-m-Y H:i'); ?>"/>
                                    <span>
                                        <i class="far fa-calendar-alt text-muted"></i>
                                    </span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="mb-2">
                        <div class="form-group row">
                            <label class="col-lg-4 col-form-label">End Date</label>
                            <div class="col-lg-8">
                                <div class="input-icon">
                                    <input type="input" class="form-control datetimepicker-input" id="end_date" name="end_date" placeholder="dd/mm/yyyy" data-date-format="dd-mm-yyyy hh:ii" value="<?php echo date('d-m-Y H:i'); ?>"/>
                                    <span>
                                        <i class="far fa-calendar-alt text-muted"></i>
                                    </span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="mb-1">
                        <div class="form-group row">
                            <label class="col-lg-4 col-form-label">Commission Price</label>
                            <div class="col-lg-8">
                                <input type="number" class="form-control" id="commission_price" name="commission_price" placeholder="Rp. 0" value=""/>
                            </div>
                        </div>
                    </div>
                    <div class="mb-2">
                        <div class="form-group row">
                            <label class="col-lg-4 col-form-label">{{ $module }} Status</label>
                            <div class="col-lg-8">
                                <input id="status" name="status" data-switch="true" type="checkbox" checked="checked" data-on-text="Enabled" data-handle-width="200" data-handle-font="1"  data-off-text="Disabled" data-on-color="warning" />
                            </div>
                        </div>
                    </div>
                    <div class="mb-2">
                        <div class="form-group row">
                            <label class="col-lg-4 col-form-label"></label>
                            <div class="col-lg-8">
                                <button type="submit" class="btn btn-success font-weight-bold">Save</button>
                                <button type="button" data-dismiss="modal" class="btn btn-danger">Cancel</button>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
@endsection

{{-- Styles Section --}}
@section('styles')
<link rel="stylesheet" href="{{ config('app.url') }}plugins/custom/datatables/datatables.bundle.css">
<link rel="stylesheet" href="{{ config('app.url') }}css/inject.css">
@endsection

{{-- Scripts Section --}}
@section('scripts')
@include ('inc.confirm-delete-modal')
<script src="{{ config('app.url') }}plugins/custom/datatables/datatables.bundle.js"></script>
<script src="{{ config('app.url') }}js/inject.js"></script>
<script type="text/javascript">
  $(document).ready(function() {
    $('.select2').select2();
    $('[data-switch=true]').bootstrapSwitch('state', true);
    $('.datetimepicker-input').datetimepicker({
        format: 'dd-mm-yyyy hh:ii'
    });

    $("div.datesearchbox").html('<div class="input-group"> <div class="input-group-addon"> <i class="glyphicon glyphicon-calendar"></i> </div><input type="text" class="form-control text-center datetimepicker-input" id="datesearch_start" placeholder="Search by date range" value="{{date('d-m-Y H:i', strtotime($dashboard_startdate))}}">&nbsp;to&nbsp;<input type="text" class="form-control  text-center datetimepicker-input" id="datesearch_end" placeholder="Search by date range" value="{{date('d-m-Y H:i', strtotime($dashboard_enddate))}}"></div>');
    $("#datesearch_start, #datesearch_end").attr("readonly",true);
    $("#datesearch_start, #datesearch_end, #company").change(function(){
      refresh_table();
    });

    $('#datesearch_start, #datesearch_end').datetimepicker({
        format: 'dd-mm-yyyy hh:ii',
        inline: true,
     });
  });

  $('[data-switch=true]').bootstrapSwitch();
  var table = $('#datatable').dataTable({
    pageLength: 5,
    responsive: true,
    searchDelay: 800,
    processing: true,
    serverSide: true,
    searching: true,
    lengthMenu: [[5, 10, 25, 50, 100, 200, -1], [5, 10, 25, 50, 100, 200, "All"]],
    ajax: {
      method: 'POST',
      url : '{{ $data }}',
      headers: {
        'X-CSRF-TOKEN': '{{ csrf_token() }}'
      },
      data: function (d) {
        d.from_date = formatdate($("#datesearch_start").val());
        d.to_date = formatdate($(" #datesearch_end").val(),true);
        d.company_id = $("#company").val();
      }
    },
    columns: [
      {title: "No", data: 'DT_RowIndex', orderable: false, searchable: false, autoHide: false},
      {title: "{{ $module }} ID", data: 'id', defaultContent: '-', class: 'text-center dt-body-nowrap', autohide: true},
      {title: "Sales ID", data: 'sales_name', name:'users.name', defaultContent: '-', class: 'text-center dt-body-nowrap', autohide: true},
      {title: "Valid Start Date", data: 'start_date', defaultContent: '-', class: 'text-center dt-body-nowrap', autohide: true},
      {title: "Valid End Date", data: 'end_date', defaultContent: '-', class: 'text-center dt-body-nowrap', autohide: true},
      {title: "Commission Price", data: 'commission_price', defaultContent: '-', class: 'text-center dt-body-nowrap', autohide: true},
      {title: "Author", data: 'author', defaultContent: '-', class: 'text-center dt-body-nowrap', autohide: true},
      {title: "Status", data: 'active', defaultContent: '-', class: 'text-center dt-body-nowrap', autohide: true},
      {title: "Actions", data: 'action', orderable: false, responsivePriority: -1},
    ],
    bStateSave: true,
    dom:  "<'row'<'col-sm-4'l><'col-sm-5' <'datesearchbox'>><'col-sm-3'f>>" +
          "<'row'<'col-sm-12'tr>>" +
          "<'row'<'col-sm-5'i><'col-sm-7'p>>",
    columnDefs: [
      {
        targets: [0,-1],
        className: 'text-center visible dt-body-nowrap'
      },
    ],
    initComplete: function() {
      $('.tl-tip').tooltip();
    }
  });

  $('#datatable tbody').on( 'click', 'tr', function () {
    $("#datatable_so tbody>tr").remove();
    if ($(this).hasClass('selected')) {
      table.$('tr.selected').removeClass('selected');
      $(this).removeClass('selected');
      $(".module_name").text('-');
      $("#datatable_sor tbody>tr").remove();
      $("#datatable_sow tbody>tr").remove();
    } else {
      table.$('tr.selected').removeClass('selected');
      $(this).addClass('selected');
      var id = $(this).find("td:eq(1)").html();
      $(".module_name").text(" : "+$(this).find("td:eq(1)").html());
      list_data(id);
    }
  });

  function list_data(id = "") {
      $(".datatable_detail").loading("start");
      $("#datatable_sor tbody>tr").remove();
      $("#datatable_sow tbody>tr").remove();
      if (id !== "") {
        $.ajax({
            url : '{{ $data }}',
            type: "POST",
            data: {
              detail : 1,
              company_id : $("#company").val(),
              id:id
            },
            headers: {
              'X-CSRF-TOKEN': '{{ csrf_token() }}'
            },
            success: function (response) {
              if(response.data){
                for (i in response.data["so_commisionrate"]){
                  var rs = response.data["so_commisionrate"][i];
                  var table = document.getElementById("datatable_sor").getElementsByTagName('tbody')[0];
                  var row = table.insertRow(-1);
                  var closing_header = "'"+rs.id+"'";
                  var voucher = parseFloat(rs.voucher_total);
                  var quantity = parseFloat(rs.quantity);
                  var cost_handler = parseFloat(rs.cost_handler);
                  var price = parseFloat(rs.price);
                  var total_product = parseFloat(rs.balance_price);
                  var status = '<a onclick="process_data(this,'+closing_header+',1)" class="btn btn-outline-success"><i class="flaticon2-poll-symbol"></i>whitelist</a>';
                  if (rs.closing_status){
                    status = '<a class="btn btn-outline-danger"><i class="flaticon2-poll-symbol"></i>Closed</a>';
                  }
                    row.insertCell(0).innerHTML = '<label class="col-12 text-left pt-1 pb-1">'+(Number(i)+1)+'</label>';
                    row.insertCell(1).innerHTML = '<label class="col-12 text-center pt-1 pb-1">'+rs.id+'</label>';
                    row.insertCell(2).innerHTML = '<label class="col-12 text-center pt-1 pb-1">'+rs.delivery_no+'</label>';
                    row.insertCell(3).innerHTML = '<label class="col-12 text-center pt-1 pb-1">'+formatdate(rs.transaction_date)+'</label>';
                    row.insertCell(4).innerHTML = '<label class="col-12 text-center pt-1 pb-1">'+rs.gender_name+' '+rs.full_name+'</label>';
                    row.insertCell(5).innerHTML = '<label class="col-12 text-center pt-1 pb-1">'+rs.quantity+'<br>'+rs.courier_name+'</label>';
                    row.insertCell(6).innerHTML = '<label class="col-12 text-center pt-1 pb-1">'+rs.bank_name+'<br>Rp. '+addCommas(total_product)+'</label>';
                    row.insertCell(7).innerHTML = '<label class="col-12 text-center pt-1 pb-1">'+rs.author+'</label>';
                    row.insertCell(8).innerHTML = '<label class="col-12 text-center pt-1 pb-1">'+rs.confirm_status+'<br>'+status+'</label>';
                }
                for (i in response.data["so_whitelist"]){
                  var rs = response.data["so_whitelist"][i];
                  var table = document.getElementById("datatable_sow").getElementsByTagName('tbody')[0];
                  var row = table.insertRow(-1);
                  var closing_header = "'"+rs.id+"'";
                  var voucher = parseFloat(rs.voucher_total);
                  var quantity = parseFloat(rs.quantity);
                  var cost_handler = parseFloat(rs.cost_handler);
                  var price = parseFloat(rs.price);
                  var total_product = ((quantity*price)-voucher);
                  var status = '<a onclick="process_data(this,'+closing_header+',0)" class="btn btn-outline-danger"><i class="flaticon2-poll-symbol"></i>delete</a>';
                  if (rs.closing_status){
                    status = '<a class="btn btn-outline-danger"><i class="flaticon2-poll-symbol"></i>Closed</a>';
                  }
                    row.insertCell(0).innerHTML = '<label class="col-12 text-warning text-left pt-1 pb-1">'+(Number(i)+1)+'</label>';
                    row.insertCell(1).innerHTML = '<label class="col-12 text-warning text-center pt-1 pb-1">'+rs.id+'</label>';
                    row.insertCell(2).innerHTML = '<label class="col-12 text-warning text-center pt-1 pb-1">'+rs.delivery_no+'</label>';
                    row.insertCell(3).innerHTML = '<label class="col-12 text-warning text-center pt-1 pb-1">'+formatdate(rs.transaction_date)+'</label>';
                    row.insertCell(4).innerHTML = '<label class="col-12 text-warning text-center pt-1 pb-1">'+rs.gender_name+' '+rs.full_name+'</label>';
                    row.insertCell(5).innerHTML = '<label class="col-12 text-warning text-center pt-1 pb-1">'+rs.quantity+'<br>'+rs.courier_name+'</label>';
                    row.insertCell(6).innerHTML = '<label class="col-12 text-warning text-center pt-1 pb-1">'+rs.bank_name+'<br>Rp. '+addCommas(total_product)+'</label>';
                    row.insertCell(7).innerHTML = '<label class="col-12 text-warning text-center pt-1 pb-1">'+rs.author+'</label>';
                    row.insertCell(8).innerHTML = '<label class="col-12 text-warning text-center pt-1 pb-1">'+rs.confirm_status+'<br>'+status+'</label>';
                }
                $(".datatable_detail").loading("stop");
              }
            },
            error: function (xhr, status, error) {
              $(".datatable_detail").loading("stop");
            }
        });
      } else {

      }
  }

  function process_data(btn,id,status){
    $("#datatable_detail").loading("start");
    $.ajax({
        url: "{{ $path_detail }}/update/"+id+"?status="+status,
        type: "POST",
        headers: {
          'X-CSRF-TOKEN': '{{ csrf_token() }}'
        },
        success: function (response) {
          var row = btn.parentNode.parentNode;
          row.parentNode.removeChild(row);
          refresh_table();
          $("#datatable_detail").loading("stop");
        },
        error: function (xhr, status, error) {
            $("#datatable_detail").loading("stop");
            showDialog.show(xhr.status + " " + status + " " + error, false);
        }
    });
  }

  function show_data(id = "") {
      if (id !== "") {
          $.ajax({
              url: "{{ $path }}/data/" + id,
              type: "GET",
              headers: {
                'X-CSRF-TOKEN': '{{ csrf_token() }}'
              },
              success: function (response) {
                if(!response){
                  showDialog.show("data "+id+" not found");
                }
                  $('#form-input').trigger("reset");
                  $("#form-input").attr("action", "{{ $path }}/"+id);
                  $('#method').val("PUT");

                  $('#id').val(response.data.id);
                  $('#company_id').val(response.data.company_id).trigger('change');
                  $('#sales_id').val(response.data.sales_id).trigger('change');
                  $('#commission_price').val(response.data.commission_price);
                  $('#start_date').val(response.data.start_date).datetimepicker('update', response.data.start_date);
                  $('#end_date').val(response.data.end_date).datetimepicker('update', response.data.end_date);
                  $('#status').bootstrapSwitch('state', response.data.status);
                  $('#modal-form').modal('show');
              },
              error: function (xhr, status, error) {
                  showDialog.show(xhr.status + " " + status + " " + error, false);
              }
          });
      } else {
          $('#form-input').trigger("reset");
          $("#form-input").attr("action", "{{ $path }}");
          $('#method').val("POST");
          $('#modal-form').modal('show');
          document.getElementById("id").focus();
      }
  }

  function refresh_table() {
      $('#datatable').DataTable().ajax.reload();
      $('#datatable').DataTable().responsive.recalc();
  }

  function format_date(val) {
      var date_arr = val.split(" ");
      var time = date_arr[1];
      var date_id_arr = date_arr[0].split("-");
      var date_fix = date_id_arr[2] + "-" + date_id_arr[1] + "-" + date_id_arr[0];
      var date_transaction = date_fix + " " + time;
      return date_transaction;
  }

    function export_pdf(exprt) {

      var from_date = formatdate($("#datesearch_start").val());
      var to_date = formatdate($(" #datesearch_end").val());
      if(from_date=="" && to_date==""){
        showDialog.show('<h4 class="text-danger text-bold">Oops, Range date has been set!</h4>');
        return;
      }
      var url = "";
      if($("#company").val() != ""){
        url += "&company_id=" + $("#company").val();
      }

      window.open("{{ ENV('API_RPT_URL') }}/commission-rate-detail.php?from_date="+from_date+"&to_date="+to_date+url);
    }
</script>
@endsection
