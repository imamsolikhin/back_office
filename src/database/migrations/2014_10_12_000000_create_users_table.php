<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->string('id')->primary();
            $table->string('company_id')->nullable();
            $table->string('dashboard_url')->nullable();
            $table->string('name');
            $table->string('phone')->unique();
            $table->string('email')->unique();
            $table->string('username')->unique();
            $table->string('password');
            $table->string('role_id');
            $table->boolean('active')->default('1');
            $table->boolean('billing_recipient')->default(0);
            $table->dateTime('last_login')->nullable();
            $table->string('api_token', 60)->nullable();
            $table->string('forgot_password_token', 20)->nullable();
            $table->rememberToken();
            $table->timestamps();
            $table->softDeletes();
            $table->index('id');
            $table->index('company_id');
            $table->index('role_id');
            $table->index('email');
            $table->index('username');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
