<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRolesTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return  void
     */
    public function up()
    {
        // Create table for storing roles
        Schema::create('roles', function (Blueprint $table) {
            $table->string('id')->primary();
            $table->string('company_id')->nullable();
            $table->string('name');
            $table->string('display_name');
            $table->string('description')->nullable();
            $table->tinyInteger('status')->default(0);
            $table->timestamps();
            $table->index('id');
            $table->index('company_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return  void
     */
    public function down()
    {
        Schema::dropIfExists('roles');
    }
}
