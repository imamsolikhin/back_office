<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSlsSalesOrderClosingTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sls_sales_order_closing', function (Blueprint $table) {
            $table->string('id',50)->primary();
            $table->string('company_id',100)->nullable();
            $table->string('bank_id')->nullable();
            $table->decimal('grand_total', $precision = 16, $scale = 2)->nullable()->default(0);
            $table->string('closing_img')->nullable();
            $table->string('closing_refno')->nullable();
            $table->string('closing_remark')->nullable();
            $table->dateTime('closing_date')->nullable();
            $table->string('author',100)->nullable();
            $table->tinyInteger('status')->default(0);
            $table->string('created_by',100)->nullable();
            $table->string('updated_by',100)->nullable();
            $table->timestamps();
            $table->index('id');
            $table->index('company_id');
            $table->index('bank_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sls_sales_order_closing');
    }
}
