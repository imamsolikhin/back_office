<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      \App\Http\Resources\Master\Ads::create([
        'id'=>'DEV-GLOBAL-001',
        'company_id'=>'DEV',
        'name'=>'Facebook',
        'status'=>1
      ]);
      \App\Http\Resources\Master\Ads::create([
        'id'=>'DEV-GLOBAL-002',
        'company_id'=>'DEV',
        'name'=>'Loops',
        'status'=>1
      ]);
      \App\Http\Resources\Master\Gender::create([
        'id'=>'DEV-GLOBAL-001',
        'company_id'=>'DEV',
        'name'=>'Bapak',
        'status'=>1
      ]);
      \App\Http\Resources\Master\Gender::create([
        'id'=>'DEV-GLOBAL-002',
        'company_id'=>'DEV',
        'name'=>'Ibu',
        'status'=>1
      ]);
      \App\Models\User::create([
        'id'=>'DEV-GLOBAL-001',
        'company_id'=>'DEV',
        'dashboard_url'=>'dashboard',
        'name'=>'Admin DEV',
        'username'=>'admin',
        'phone'=>'081808178118',
        'email'=> 'admin@gmail.com',
        'password'=>'$2y$10$j2k/ohKZUiJ8.VLrSgijdujJiTzY27PJIqUrWEMtYikyFfmVopfiC',
        'role_id'=>"DEV",
        'active'=>1
      ]);
      \App\Models\Role::create([
        'id'=>'DEV',
        'name'=>'DEV',
        'company_id'=>'DEV',
        'display_name'=>'Super Developer',
        'description'=>'Super Developer',
        'status'=>1
      ]);
      \App\Models\Role::create([
        'id'=>'MNG',
        'name'=>'MNG',
        'company_id'=>'DEV',
        'display_name'=>'Management',
        'description'=>'Management',
        'status'=>1
      ]);
      \App\Http\Resources\Master\Company::create([
        'id'=>'DEV',
        'name'=>'Developh',
        'created_by'=>'devjr',
        'updated_by'=>'devjr',
        'status'=>1
      ]);
    }
}
